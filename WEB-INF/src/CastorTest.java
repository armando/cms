import java.io.File;
import java.io.FileDescriptor;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Enumeration;

import org.exolab.castor.xml.schema.Facet;
import org.exolab.castor.xml.schema.Schema;
import org.exolab.castor.xml.schema.SimpleType;
import org.exolab.castor.xml.schema.reader.SchemaReader;

/*
 * Created on 26-ago-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author armando
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CastorTest {
       public static void main(String[] args) {
        try{
            SchemaReader sr= new SchemaReader("file:/eclipse/workspace/modelos/xsd/gastronomia_viajes/servicios.xsd");
            Schema s= sr.read();
            SimpleType st= s.getSimpleType("servicioType");
            for (Enumeration e= st.getFacets("enumeration"); e.hasMoreElements();){
                Facet f= (Facet) e.nextElement();
                System.out.println(f.getValue());
            }
            System.out.println(st.hasFacet("enumeration"));
        } catch (IOException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
