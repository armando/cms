
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.hola.imagen.Image;
import com.hola.imagen.Version;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageEncoder;

/**
 * 
 * @author Armando Ramos <armando@hola.com>
 *  
 */

public class ImageServlet extends HttpServlet {
    private static final String BASE= "/www/data/hola";
    private File imgDir= null;

    private String appName;

    private final SimpleDateFormat formatter= new SimpleDateFormat(
            "EEE, dd MMM yyyy HH:mm:ss z", new Locale("en", "US"));

    static Logger logger= Logger.getLogger(ImageServlet.class.getName());
    // Optimization suggested by Dennis Sosnoski in
    // http://www-106.ibm.com/developerworks/library/jw-performance.html

    private final Date convertDate= new Date();
    private final StringBuffer convertBuffer= new StringBuffer();
    private final FieldPosition convertField= new FieldPosition(0);

    public void init(ServletConfig config) throws ServletException {
        imgDir= new File(BASE);
        try{
            formatter.setTimeZone(new SimpleTimeZone(0, "GMT"));
        } catch (Exception e){
            throw new ServletException(e);
        }
        super.init(config);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        String imagenFile= req.getPathInfo();
        int slash= imagenFile.lastIndexOf('/');
        String file= imagenFile.substring(slash);
        String id= imagenFile.substring(0, slash);

        logger.info("Getting image: " + file + " de " + id);

        int baseidx= id.indexOf(BASE);
        if (baseidx >= 0){
            id= id.substring(baseidx + BASE.length());
        }

        File dataDir= new File(imgDir, id);
        File dataFile= new File(dataDir, "index.xml");

        Image image= null;
        try{
            image= (Image) Image.unmarshal(new FileReader(dataFile));
        } catch (FileNotFoundException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

        Pattern p1= Pattern.compile("/([^.]+)\\.(jpg|gif|png|html)");
        Pattern p2= Pattern.compile("([0-9]+)(x([0-9]+))?");
        String name= null, type= null;

        int w= 0, h= 0;
        int maxw= 0, maxh= 0;
        Matcher m1= p1.matcher(file);
        if (m1.matches()){
            name= m1.group(1);
            type= m1.group(2);

            Matcher m2= p2.matcher(name);
            if (m2.matches()){
                w= Integer.parseInt(m2.group(1));
                if (m2.group(2) != null){
                    h= Integer.parseInt(m2.group(3));
                } else{
                    h= 0;
                }
//                System.out.println(w + ":" + h + ":" + type);
            }
        }

        Version[] versions= image.getVersion();
        Version selected= versions[0];
        int score= Integer.MAX_VALUE;
        h= (h > 0) ? h : Integer.MAX_VALUE;
        w= (w > 0) ? w : Integer.MAX_VALUE;

        for (int i= 0, n= image.getVersionCount(); i < n; i++){
            Version v= versions[i];
            if (v.getData().endsWith(file)){
                selected= v;
                h= v.getHeight();
                maxh= h;
                w= v.getWidth();
                maxw= w;
//                System.out.println("Bingo! w="+w+" h="+h);
                break;
            }
//            System.out.println("Testing " + i);
//            System.out.println(score + "::" + v.getHeight() + "-" + h + "-"
//                    + v.getWidth() + "-" + w);
            maxw= (v.getWidth() > maxw) ? v.getWidth() : maxw;
            maxh= (v.getHeight() > maxh) ? v.getHeight() : maxh;
//            if (v.getWidth() <= w){
                int hdif= (h < Integer.MAX_VALUE) ? h - v.getHeight() : 0;
                int s= w - v.getWidth() + hdif;
//                System.out.println("S= " + s);
                int sabs= Math.abs(s);
                if (sabs < score){
                    score= sabs;
                    selected= v;
//                    System.out.println(selected.getData() + " por ahora");
                }
//            }
        }

//        System.out.println(maxw + "x" + maxh);

        double prop= (double) selected.getHeight() / selected.getWidth();
        w= (w <= maxw) ? w : maxw;
        h= (h <= maxh) ? h : (new Double(prop * w)).intValue();

//        System.out.println("h= " + h + ": w=" + w + " prop:" + prop);

        BufferedOutputStream out= null;
        BufferedInputStream in= null;

        try{
            URL im= new URL(selected.getData());
            InputStream is= im.openStream();
            //            long distance= System.currentTimeMillis() - selected.();
            //            FileInputStream fis= new FileInputStream(f);
            in= new BufferedInputStream(is);

            out= new BufferedOutputStream(res.getOutputStream());
            if (w != selected.getWidth()){
                BufferedImage ibuffer= ImageIO.read(in);
                BufferedImage scaledImg= new BufferedImage(w, h,
                        BufferedImage.TYPE_INT_RGB);
                Graphics2D gScaledImg= scaledImg.createGraphics();
                // Note the use of BILNEAR filtering to enable smooth scaling
                gScaledImg.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                        RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                gScaledImg.drawImage(ibuffer, 0, 0, w, h, null);

                String mime= type.equalsIgnoreCase("jpg") ? "JPEG"
                        : "PNG";

                ImageEncoder encoder= ImageCodec.createImageEncoder(mime, out,
                        null);
                encoder.encode(scaledImg);

                //RenderedOp op = JAI.create("filestore", bi, "test.png",
                // "PNG");

                //                File f= new File("/www/data/test.gif");
                //                ImageIO.write(scaledImg, "gif", f);

            } else{
                out= new BufferedOutputStream(res.getOutputStream());
                int car, n= 0; // File Lenght count
                while ((car= in.read()) != -1){
                    out.write(car);
                    n++;
                }
            }

            String mimeType= selected.getMime();
            res.setContentType(mimeType);

            /*
             * convertDate.setTime(f.lastModified());
             * convertBuffer.setLength(0); res.setHeader("Last-Modified",
             * formatter.format(convertDate, convertBuffer,
             * convertField).toString());
             * 
             * convertDate.setTime(System.currentTimeMillis() + distance / 10L);
             * convertBuffer.setLength(0); res.setHeader("Expires",
             * formatter.format(convertDate, convertBuffer,
             * convertField).toString());
             */
            //            res.setContentLength(n);
            //            fis.close();
            out.flush();
            out.close();
        } catch (Exception e){
            logger.error("Exception showing image: " + imagenFile, e);
        } finally{
            if (in != null){
                in.close();
            } // Always Closed
            if (out != null){
                out.close();
            } // Always Closed
        }
    }

}