/*
 * Created on 07-sep-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hola.ed.misc;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.myfaces.custom.tree.DefaultMutableTreeNode;
import org.apache.myfaces.custom.tree.model.DefaultTreeModel;


/**
 * @author armando
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Tree extends DefaultTreeModel {
    public Tree() {
        System.out.println("Generando tree");
        setPath("imagenes");
    }

    public void setPath(String path) {
        DefaultMutableTreeNode root2= (DefaultMutableTreeNode)this.getRoot();
        for (int i= 0, n= root2.getChildCount(); i < n; i++){
            root2.remove(i);
         }
        System.out.println("Regenerando tree");
        root2.setUserObject("Test");
        String base= "/www/data/hola";
        File f= new File(base, path);
//        DefaultMutableTreeNode child= newChild(root, new Branch("test", "test"));
       addChildren(root2, f);
//        DefaultMutableTreeNode node= new DefaultMutableTreeNode("test");
//        root2.insert(node);
//        System.out.println(root.getChildCount()+" children en root");
    }

    private void addChildren(DefaultMutableTreeNode node, File f) {
        if (f.isDirectory()){
            DefaultMutableTreeNode child= newChild(node, new Branch(f.getName(), f));
            File[] files= f.listFiles();
            for (int i= 0; i < files.length; i++){
//                System.out.println("dir-"+files[i]);
                addChildren(child, files[i]);
            }
        }
    }

    private DefaultMutableTreeNode newChild(DefaultMutableTreeNode node, Branch branch) {
        DefaultMutableTreeNode child= new DefaultMutableTreeNode(branch);
//        System.out.println("Inserting "+branch.toString()+ " en "+node);
        node.insert(child);
//        this.nodeChanged(node);
        return child;
    }
}