package com.hola.ed.misc;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.imageio.ImageIO;

import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import com.hola.imagen.Image;
import com.hola.imagen.Reference;
import com.hola.imagen.Version;

/**
 * @author Manfred Geiler (latest modification by $Author$)
 * @version $Revision$ $Date$
 */
public class FileUploadForm {
    private UploadedFile upFile;
    private String name= "", title= "", text= "", news= "", newIm= "";
    private Integer width= null;
    private Integer height= null;
    private Integer versionNumber= null;
    private Version version= null;
    Image image= null;
    Version[] versiones= null;
    File index= null;

    public FileUploadForm(){
        versionNumber= new Integer(0);
        version= new Version();
        Reference ref= new Reference();
        version.setReference(ref);
    }
    
    /**
     * @return Returns the version.
     */
    public Integer getVersionNumber() {
        return versionNumber;
    }

    /**
     * @param version
     *            The version to set.
     */
    public void setVersionNumber(Integer _versionNumber) {
        this.versionNumber= _versionNumber;
        this.version= versiones[versionNumber.intValue()];
        title= version.getReference().getTitle();
        text= version.getReference().getText();
    }

    /**
     * @return Returns the version.
     */
    public Version getVersion() {
        return version;
    }

    /**
     * @param version
     *            The version to set.
     */
    public void setVersion(Version _version) {
        this.version= _version;
    }
    
    public Boolean getHasVersion(){
        return new Boolean(version.getData() != null);
    }

    public UploadedFile getUpFile() {
        return upFile;
    }

    public void setUpFile(UploadedFile _upFile) {
        upFile= _upFile;
    }

    public String getName() {
//        System.out.println("Entrando en getname");
        return name;
    }

    public void setName(String _name) {
        name= _name;
        upFile= null;
        Map application= FacesContext.getCurrentInstance().getExternalContext()
                .getApplicationMap();
//        application.remove("fileupload_bytes");

        String dirName= "/www/data/hola/" + getName();
        index= new File(dirName, "/index.xml");
//        System.out.println(index.getAbsolutePath());

        try{
            image= (Image) Image.unmarshal(new FileReader(index));
        } catch (FileNotFoundException e){
            // TODO Auto-generated catch block
            //            e.printStackTrace();
            image= new Image();
            image.setId(getName());
        } catch (Exception e){
            e.printStackTrace();
            image= new Image();
            image.setId(getName());
        }

        versiones= image.getVersion();
        if (versiones != null && versiones.length > 0){
//            System.out.println(versiones.length);
            version= versiones[0];
        }
    }

    public String getNew() {
        return newIm;
    }

    public void setNew(String _new) {
        newIm= _new;
    }

    public String getCurrent() {
        if (version == null){
            return "";
        }
        String data= version.getData();
//        System.out.println("Current: " + data);
        return getName() + data.substring(data.lastIndexOf('/'));
    }

    public void newImage() {
        String dirName= "/www/data/hola/" + getName();
        File dir= new File(dirName, newIm);
        if (!dir.exists()){
            dir.mkdirs();
        }
        FacesContext facesContext= FacesContext.getCurrentInstance();
        VariableResolver vr= facesContext.getApplication()
                .getVariableResolver();
        Tree t= (Tree) resolve("treeModel");
        t.setPath("imagenes");
        setName(getName() + "/" + newIm);
    }

    public String upload() throws IOException {
        FacesContext facesContext= FacesContext.getCurrentInstance();
        Map session= facesContext.getExternalContext().getSessionMap();

 /*     
        if (upFile == null){
            System.out.println("Upfile null");
            UploadedFile up= (UploadedFile) session.get("upFile");
            if (up != null){
                upFile= up;
            } else{
                System.out.println("No hay imagen en sesion");
            }
        } else{
            facesContext.getExternalContext().getApplicationMap().put(
                    "fileupload_bytes", upFile.getBytes());
            session.put("upFile", upFile);
        }
*/
        int igual= -1;

        if (upFile != null){
            ByteArrayInputStream ba= new ByteArrayInputStream(upFile.getBytes());
            File f= new File("/www/data/hola" + getName(), URLEncoder.encode(upFile.getName()));
            FileOutputStream fo= new FileOutputStream(f);
            fo.write(upFile.getBytes());
            fo.close();
            System.out.println("Grabando en " + f.getAbsolutePath());
            BufferedImage im= ImageIO.read(ba);

            System.out.println(im.getWidth(null) + "x" + im.getHeight(null));
            version= new Version();
            version.setWidth(im.getWidth(null));
            version.setHeight(im.getHeight(null));
            // To save an base64-encoded version
            //			Base64 encoder= new Base64();
            //	        v.setData(new String(encoder.encode(upFile.getBytes())));
            version.setData(f.toURI().toString());
            version.setMime(upFile.getContentType());
            Reference ref= new Reference();
            ref.setCreated(new Date());
            version.setReference(ref);
            
            facesContext.getExternalContext().getApplicationMap().put(
                        "fileupload_type", upFile.getContentType());

            System.out.println("Grabados: " + version.getHeight() + "x"
                    + version.getWidth());
        }

        version.getReference().setText(text);
        version.getReference().setTitle(title);

        for (int i= 0, n= image.getVersionCount(); i < n; i++){
            Version v1= image.getVersion(i);
            if (version.getData().equals(v1.getData())){
                igual= i;
                break;
            }
        }

        if (igual >= 0){
            image.setVersion(igual, version);
            System.out.println("Sustituyendo " + igual);
            System.out.println(image.getVersion(igual).getReference().getText());
        } else{
            image.addVersion(version);
            System.out.println("Anyadiendo " + version.getData()
                    + " para un total de " + image.getVersionCount());
        }

        if (!version.isValid()){
            System.out.println("Version no valida");
        }

        try{
            //        	if (image.isValid()){
            System.out.println("Grabando en " + index.getAbsolutePath());
            ResourceList rl= (ResourceList) resolve("resources");
            rl.update(image);
            image.marshal(new FileWriter(index));
            //        	}
            //        	else{
            //        		System.out.println("No es valida");
            //        	}
        } catch (MarshalException e1){
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (ValidationException e1){
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1){
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        return "ok";
    }

    /**
     * @return
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @return
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param integer
     */
    public void setHeight(Integer integer) {
        height= integer;
    }

    /**
     * @param integer
     */
    public void setWidth(Integer integer) {
        width= integer;
    }

    /**
     * @return
     */
    public String getNews() {
        return news;
    }

    /**
     * @param string
     */
    public void setNews(String string) {
        news= string;
    }

    /**
     * @return
     */
    public String getText() {
        return text;
    }

    /**
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param string
     */
    public void setText(String string) {
        text= string;
    }

    /**
     * @param string
     */
    public void setTitle(String string) {
        title= string;
    }
    
    private Object resolve(String var){
        FacesContext facesContext= FacesContext.getCurrentInstance();
        VariableResolver vr= facesContext.getApplication()
                .getVariableResolver();
        return vr.resolveVariable(facesContext, var);
    }

    public void validateName(FacesContext context, UIComponent toValidate,
            Object value) {

        String message= "";
        String nom= (String) value;
        System.out.println("Validando ");
        if (!nom.matches("/[a-z_-]+/[0-9]{4}/[0-9]{2}/[0-9]{2}/[0-9a-z_-]+")){
            ((UIInput) toValidate).setValid(false);
            message= "Error en nombre";
            context.addMessage(toValidate.getClientId(context),
                    new FacesMessage(message));
        }
    }
}