
package com.hola.ed.misc;
import java.io.File;
import java.net.MalformedURLException;

public class DTree {
    private int idcounter= 0;
    private String tree= null;
    
    public DTree() {
        System.out.println("Initializing Tree...");
        tree= generateDTree(new File("/www/data/hola/imagenes/"));
    }

    public String getTree(){
        return tree;
    }

    private String generateDTree(File f){
        idcounter= 0;
        StringBuffer sb= new StringBuffer();
        sb.append("a = new dTree('a');\n");
//        sb.append("a.config.closeSameLevel=true;\n");
        sb.append("a.config.useCookies=true;\n");
        recurseFiles(sb, f, id(), -1);
        sb.append("document.write(a);\n");
        return sb.toString();
    }
    
    private void recurseFiles(StringBuffer sb, File f, int id, int parent){
        String name= "";
        if (f.isHidden())
            return;
        if (f.isFile()){
            try{
                name= f.toURL().toExternalForm();
            } catch (MalformedURLException e){
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        addBranch(sb, id, parent, f.getName(), name);
        if (f.isDirectory()){
            File[] files= f.listFiles();
            for (int i= 0; i < files.length; i++){
                recurseFiles(sb, files[i], id(), id);
            }
        }
    }

    private int id(){
        return idcounter++;
    }
    
    
    private static void addBranch(StringBuffer sb, int id, int parent, String name, String action){
        sb.append("a.add(").append(id).append(", ");
        sb.append(parent).append(",'");
        sb.append(name).append("', '");
        sb.append(action).append("');\n");
    }
}