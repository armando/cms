/*
 * Created on 13-sep-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hola.ed.misc;

import java.io.File;

/**
 * @author armando
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Branch {
    private String name= null;
    File file= null;
    public Branch (String n, File f){
        this.name= n;
        this.file= f;
    }
    public static void main(String[] args) {
    }
    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }
    /**
     * @return Returns the value.
     */
    public File getFile() {
        return file;
    }
    
    public String toString(){
        return name;
    }
}
