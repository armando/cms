/*
 * Created on 08-sep-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hola.ed.misc;

import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;

import org.apache.myfaces.custom.tree.DefaultMutableTreeNode;
import org.apache.myfaces.custom.tree.event.TreeSelectionEvent;
import org.apache.myfaces.custom.tree.model.TreePath;


/**
 * @author armando
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TreeSelectionListener implements
        org.apache.myfaces.custom.tree.event.TreeSelectionListener {

    /* (non-Javadoc)
     * @see net.sourceforge.myfaces.custom.tree.event.TreeSelectionListener#valueChanged(net.sourceforge.myfaces.custom.tree.event.TreeSelectionEvent)
     */
    public void valueChanged(TreeSelectionEvent arg0) {
        TreePath tp= arg0.getNewSelectionPath();

        FacesContext fc= FacesContext.getCurrentInstance();
        VariableResolver vr = fc.getApplication().getVariableResolver();
        
        FileUploadForm fu= (FileUploadForm) vr.resolveVariable(fc, "fileUploadForm");
        ResourceList rl= (ResourceList) vr.resolveVariable(fc, "resources");

        String path= treeToPath(tp);
            
        fu.setName(path);
        rl.setLocation(path);
    }

    private String treeToPath(TreePath tp){
        Object[] branches= tp.getPath();
        StringBuffer sb= new StringBuffer();
        for (int i= 0, n= branches.length; i < n; i++){
//            System.out.println(branches[i].getClass().getName());
            DefaultMutableTreeNode node= (DefaultMutableTreeNode) branches[i];
//            System.out.println(node.getUserObject().getClass().getName());
            Object uo= node.getUserObject();
            if (uo instanceof Branch){
                Branch b= (Branch) uo;
//                System.out.println(b.getName()+":"+b.getFile().getAbsolutePath());
                sb.append("/").append(b.getName());
            }
        }
        return sb.toString();
    }
    
    public static void main(String[] args) {
    }

    /* (non-Javadoc)
     * @see org.apache.myfaces.custom.tree.event.TreeSelectionListener#valueChanged(org.apache.myfaces.custom.tree.event.TreeSelectionEvent)
     */

}
