package com.hola.ed.misc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;

import com.hola.imagen.Image;
import com.hola.imagen.Version;

/**
 * @author Armando Ramos <armando@hola.com>
 * @version $Revision$ $Date$
 */
public class ResourceList {
    String location= null;
    DataModel list= null;

    /**
     * @return Returns the location.
     */
    public String getLocation() {
        return location;
    }

    public void setLocation(String _location) {
        location= _location;
        load();
    }

    public DataModel getList() {
        return list;
    }

    public void load() {
        String dirName= "/www/data/hola/" + getLocation();
        File dir= new File(dirName);

        File file= new File(dir, "/index.xml");
        Image image= null;
        try{
            image= (Image) Image.unmarshal(new FileReader(file));
        } catch (FileNotFoundException e){
            System.out.println(" No existe " + file);
        } catch (Exception e){
            e.printStackTrace();
        }

        update(image);
    }

    public void update(Image image) {
        list= new ArrayDataModel();

        if (image != null){
            Version[] versiones= image.getVersion();
            list.setWrappedData(versiones);
        }

        /*
         * for (int i= 0, n= versiones.length; i < n; i++){ Version v=
         * versiones[i];
         * 
         * Base64 encoder= new Base64(); String data= v.getData();
         * 
         * ByteArrayInputStream ba= new
         * ByteArrayInputStream(encoder.decode(data.getBytes())); BufferedImage
         * im = ImageIO.read(ba); }
         */
    }
}