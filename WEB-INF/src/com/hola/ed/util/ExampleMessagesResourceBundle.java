
package com.hola.ed.util;

import javax.faces.context.FacesContext;
import java.util.Enumeration;
import java.util.ResourceBundle;

/**
 * DOCUMENT ME!
 * @author Manfred Geiler (latest modification by $Author$)
 * @version $Revision$ $Date$
 */
public class ExampleMessagesResourceBundle
    extends ResourceBundle
{
    private static String BUNDLE_NAME = "com.hola.ed.resources.ed_messages";

    protected ResourceBundle getMyBundle()
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return ResourceBundle.getBundle(BUNDLE_NAME, facesContext.getViewRoot().getLocale());
    }

    protected Object handleGetObject(String key)
    {
        return getMyBundle().getObject(key);
    }

    public Enumeration getKeys()
    {
        return getMyBundle().getKeys();
    }
}
