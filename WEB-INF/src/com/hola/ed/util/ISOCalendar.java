/* ISO 8601 date manipulation in Java 1.2.
   This was sent to me by Simon Brooke <simon@jasmine.org.uk>
   2000-11-22T15:20:52Z for inclusion into my material on ISO 8601.
   I don't understand much of it, but I include it in the hope
   that some people might find it useful. 
     Jukka K. Korpela, jkorpela@cs.tut.fi 
*/

package com.hola.ed.util;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class ISOCalendar extends GregorianCalendar {
    /** Return an ISO8601 string representing the date/time
     *  represented by this Calendar */
    public String toString() {
        String timef= "'T'HH:mm:ss";
        String datef= "yyyy-MM-dd";
        String bothf= "yyyy-MM-dd'T'HH:mm:ss";
        boolean dotimezone= true;

        String format= bothf; // initially assume this is a date/time

        /*
               if ( ( isSet( DAY_OF_MONTH) == false || get( DAY_OF_MONTH) == 1) &&
                    ( isSet( MONTH) == false || get( MONTH) == 0) &&
                    ( isSet( YEAR) == false || get( YEAR) == 1970))
                           // it's highly probable that we're
                           // looking at a time-of-day.
                   format = timef;
               else
                   if ( ( isSet( HOUR) == false || get( HOUR) == 0) &&
                    ( isSet( MINUTE) == false || get( MINUTE) == 0) &&
                    ( isSet( SECOND) == false || get( SECOND) == 0))
                           // it's highly probable that we're
                           // looking at a date.
                   {
                       format = datef;
                       dotimezone = false;
                   }
        */
        DateFormat df= new SimpleDateFormat(format);
        StringBuffer result= new StringBuffer(df.format(getTime()));

        if (dotimezone) // we don't need to worry about timezone in
            { // date only strings but otherwise we do...

            int offset=
                get(java.util.Calendar.ZONE_OFFSET)
                    + get(java.util.Calendar.DST_OFFSET);

            if (offset == 0)
                result.append("Z");
            // zero offset -- excellent, easy.
            else { // horrible, horrible, oh most horrible.
                NumberFormat nf= new DecimalFormat("00");

                offset= offset / 60000;
                int om= Math.abs(offset % 60), oh= Math.abs(offset / 60);
                String os= "+";
                if (offset < 0)
                    os= "-";

                result.append(os).append(nf.format(oh)).append(":").append(
                    nf.format(om));
            }
        }

        return result.toString();
    }

    public static void main(String[] args) {
        ISOCalendar i= new ISOCalendar();
        System.out.println(i.toString());
    }
}
