/*
 * Created on 20-abr-2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hola.ed.util;

import java.io.IOException;
import java.util.Vector;

import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

/**
 * @author armando
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Ping {

    /**
     * 
     */
    public Ping() {
        super();
        // TODO Auto-generated constructor stub
    }

    public static void pingomatic(String name, String url){
        ping("http://rpc.pingomatic.com/", name, url);
    }

    public static void yahoo(String name, String url){
        ping("http://api.my.yahoo.com/RPC2/", name, url);
    }

    public static void moreover(String name, String url){
        ping("http://api.moreover.com/RPC2/", name, url);
    }
    
    public static void technorati(String name, String url){
        ping("http://rpc.technorati.com", name, url);
    }

    public static void sindic8(String name, String url){
        ping("http://www.syndic8.com/xmlrpc.php", name, url);
    }
   
    public static void ping(String ping, String name, String url){
        try{
            XmlRpcClient xc = new XmlRpcClient(ping);
    		Vector	params	= new Vector();
    		params.add(name);
    		params.add(url);
    		
                Object result= xc.execute( "weblogUpdates.ping", params );
                
            } catch (XmlRpcException e1){
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (IOException e1){
                // TODO Auto-generated catch block
                e1.printStackTrace();
        }
        
    }    
    
    public static void main(String[] args) {
    }
}
