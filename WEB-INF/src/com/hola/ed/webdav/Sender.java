/*
 * Created on 19-abr-2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hola.ed.webdav;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Vector;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpURL;
import org.apache.webdav.lib.WebdavResource;
import org.apache.xmlrpc.XmlRpcClient;

/**
 * @author armando
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Sender {

    /**
     *  
     */
    public Sender() {
        super();
        // TODO Auto-generated constructor stub
    }

    public static boolean send(String base, String ticker) {
        boolean rslt= false;
        String filename= ticker+".xml";
        try{
            HttpURL hrl= new HttpURL("http://pu2.hola.com:4000/webdav/");
            hrl.setUserinfo("tomcat", "tomcat");
            WebdavResource wdr= new WebdavResource(hrl);

            File fn= new File(base, filename);
            if (fn.exists()){
                System.out.println(fn.getAbsolutePath());
            }
            wdr.setPath(wdr.getPath()+"newsticker");
            String path= wdr.getPath() +"/"+ filename;
            rslt= wdr.putMethod(path, fn);

            wdr.close();
        } catch (MalformedURLException mue){
            System.out.println(mue.getMessage());
        } catch (HttpException he){
            System.out.println(he.getMessage());
        } catch (IOException ioe){
            System.out.println(ioe.getMessage());
        }
        
        return rslt;
    }

    public static boolean receive(String base, String ticker) {
        boolean rslt= false;
        try{
            HttpURL hrl= new HttpURL("http://localhost:4000/webdav/");
            hrl.setUserinfo("tomcat", "tomcat");
            WebdavResource wdr= new WebdavResource(hrl);
      
            File fn = new File("tomcat.gif"); 
            wdr.getMethod(fn);
            wdr.close();
        } catch (MalformedURLException mue){
            System.out.println(mue.getMessage());
        } catch (HttpException he){
            System.out.println(he.getMessage());
        } catch (IOException ioe){
            System.out.println(ioe.getMessage());
        }
        return rslt;
    }

    public static void main(String[] args) {
        boolean rslt= false;
        try{
            HttpURL hrl= new HttpURL("http://localhost:4000/webdav/");
            hrl.setUserinfo("tomcat", "tomcat");
            WebdavResource wdr= new WebdavResource(hrl);

            rslt= wdr.mkcolMethod(wdr.getPath() + "newsticker");
            System.out.println(rslt);

            wdr.close();
        } catch (MalformedURLException mue){
            System.out.println(mue.getMessage());
        } catch (HttpException he){
            System.out.println(he.getMessage());
        } catch (IOException ioe){
            System.out.println(ioe.getMessage());
        }
    }

}