/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.imagen;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Image.
 * 
 * @version $Revision$ $Date$
 */
public class Image extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _id
     */
    private java.lang.String _id;

    /**
     * Field _versionList
     */
    private java.util.ArrayList _versionList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Image() {
        super();
        _versionList = new ArrayList();
    } //-- com.hola.imagen.Image()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addVersion
     * 
     * @param vVersion
     */
    public void addVersion(com.hola.imagen.Version vVersion)
        throws java.lang.IndexOutOfBoundsException
    {
        _versionList.add(vVersion);
    } //-- void addVersion(com.hola.imagen.Version) 

    /**
     * Method addVersion
     * 
     * @param index
     * @param vVersion
     */
    public void addVersion(int index, com.hola.imagen.Version vVersion)
        throws java.lang.IndexOutOfBoundsException
    {
        _versionList.add(index, vVersion);
    } //-- void addVersion(int, com.hola.imagen.Version) 

    /**
     * Method clearVersion
     */
    public void clearVersion()
    {
        _versionList.clear();
    } //-- void clearVersion() 

    /**
     * Method enumerateVersion
     */
    public java.util.Enumeration enumerateVersion()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_versionList.iterator());
    } //-- java.util.Enumeration enumerateVersion() 

    /**
     * Returns the value of field 'id'.
     * 
     * @return the value of field 'id'.
     */
    public java.lang.String getId()
    {
        return this._id;
    } //-- java.lang.String getId() 

    /**
     * Method getVersion
     * 
     * @param index
     */
    public com.hola.imagen.Version getVersion(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _versionList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hola.imagen.Version) _versionList.get(index);
    } //-- com.hola.imagen.Version getVersion(int) 

    /**
     * Method getVersion
     */
    public com.hola.imagen.Version[] getVersion()
    {
        int size = _versionList.size();
        com.hola.imagen.Version[] mArray = new com.hola.imagen.Version[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hola.imagen.Version) _versionList.get(index);
        }
        return mArray;
    } //-- com.hola.imagen.Version[] getVersion() 

    /**
     * Method getVersionCount
     */
    public int getVersionCount()
    {
        return _versionList.size();
    } //-- int getVersionCount() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeVersion
     * 
     * @param vVersion
     */
    public boolean removeVersion(com.hola.imagen.Version vVersion)
    {
        boolean removed = _versionList.remove(vVersion);
        return removed;
    } //-- boolean removeVersion(com.hola.imagen.Version) 

    /**
     * Sets the value of field 'id'.
     * 
     * @param id the value of field 'id'.
     */
    public void setId(java.lang.String id)
    {
        this._id = id;
    } //-- void setId(java.lang.String) 

    /**
     * Method setVersion
     * 
     * @param index
     * @param vVersion
     */
    public void setVersion(int index, com.hola.imagen.Version vVersion)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _versionList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _versionList.set(index, vVersion);
    } //-- void setVersion(int, com.hola.imagen.Version) 

    /**
     * Method setVersion
     * 
     * @param versionArray
     */
    public void setVersion(com.hola.imagen.Version[] versionArray)
    {
        //-- copy array
        _versionList.clear();
        for (int i = 0; i < versionArray.length; i++) {
            _versionList.add(versionArray[i]);
        }
    } //-- void setVersion(com.hola.imagen.Version) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hola.imagen.Image) Unmarshaller.unmarshal(com.hola.imagen.Image.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
