/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.imagen;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class RefType.
 * 
 * @version $Revision$ $Date$
 */
public class RefType extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _title
     */
    private java.lang.String _title;

    /**
     * Field _text
     */
    private java.lang.String _text;

    /**
     * Field _keywords
     */
    private java.lang.String _keywords;

    /**
     * Field _author
     */
    private java.lang.String _author;

    /**
     * Field _created
     */
    private java.util.Date _created;

    /**
     * Field _editor
     */
    private java.lang.String _editor;

    /**
     * Field _modified
     */
    private java.util.Date _modified;

    /**
     * Field _newsList
     */
    private java.util.ArrayList _newsList;


      //----------------/
     //- Constructors -/
    //----------------/

    public RefType() {
        super();
        _newsList = new ArrayList();
    } //-- com.hola.imagen.RefType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addNews
     * 
     * @param vNews
     */
    public void addNews(java.lang.String vNews)
        throws java.lang.IndexOutOfBoundsException
    {
        _newsList.add(vNews);
    } //-- void addNews(java.lang.String) 

    /**
     * Method addNews
     * 
     * @param index
     * @param vNews
     */
    public void addNews(int index, java.lang.String vNews)
        throws java.lang.IndexOutOfBoundsException
    {
        _newsList.add(index, vNews);
    } //-- void addNews(int, java.lang.String) 

    /**
     * Method clearNews
     */
    public void clearNews()
    {
        _newsList.clear();
    } //-- void clearNews() 

    /**
     * Method enumerateNews
     */
    public java.util.Enumeration enumerateNews()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_newsList.iterator());
    } //-- java.util.Enumeration enumerateNews() 

    /**
     * Returns the value of field 'author'.
     * 
     * @return the value of field 'author'.
     */
    public java.lang.String getAuthor()
    {
        return this._author;
    } //-- java.lang.String getAuthor() 

    /**
     * Returns the value of field 'created'.
     * 
     * @return the value of field 'created'.
     */
    public java.util.Date getCreated()
    {
        return this._created;
    } //-- java.util.Date getCreated() 

    /**
     * Returns the value of field 'editor'.
     * 
     * @return the value of field 'editor'.
     */
    public java.lang.String getEditor()
    {
        return this._editor;
    } //-- java.lang.String getEditor() 

    /**
     * Returns the value of field 'keywords'.
     * 
     * @return the value of field 'keywords'.
     */
    public java.lang.String getKeywords()
    {
        return this._keywords;
    } //-- java.lang.String getKeywords() 

    /**
     * Returns the value of field 'modified'.
     * 
     * @return the value of field 'modified'.
     */
    public java.util.Date getModified()
    {
        return this._modified;
    } //-- java.util.Date getModified() 

    /**
     * Method getNews
     * 
     * @param index
     */
    public java.lang.String getNews(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _newsList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_newsList.get(index);
    } //-- java.lang.String getNews(int) 

    /**
     * Method getNews
     */
    public java.lang.String[] getNews()
    {
        int size = _newsList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_newsList.get(index);
        }
        return mArray;
    } //-- java.lang.String[] getNews() 

    /**
     * Method getNewsCount
     */
    public int getNewsCount()
    {
        return _newsList.size();
    } //-- int getNewsCount() 

    /**
     * Returns the value of field 'text'.
     * 
     * @return the value of field 'text'.
     */
    public java.lang.String getText()
    {
        return this._text;
    } //-- java.lang.String getText() 

    /**
     * Returns the value of field 'title'.
     * 
     * @return the value of field 'title'.
     */
    public java.lang.String getTitle()
    {
        return this._title;
    } //-- java.lang.String getTitle() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeNews
     * 
     * @param vNews
     */
    public boolean removeNews(java.lang.String vNews)
    {
        boolean removed = _newsList.remove(vNews);
        return removed;
    } //-- boolean removeNews(java.lang.String) 

    /**
     * Sets the value of field 'author'.
     * 
     * @param author the value of field 'author'.
     */
    public void setAuthor(java.lang.String author)
    {
        this._author = author;
    } //-- void setAuthor(java.lang.String) 

    /**
     * Sets the value of field 'created'.
     * 
     * @param created the value of field 'created'.
     */
    public void setCreated(java.util.Date created)
    {
        this._created = created;
    } //-- void setCreated(java.util.Date) 

    /**
     * Sets the value of field 'editor'.
     * 
     * @param editor the value of field 'editor'.
     */
    public void setEditor(java.lang.String editor)
    {
        this._editor = editor;
    } //-- void setEditor(java.lang.String) 

    /**
     * Sets the value of field 'keywords'.
     * 
     * @param keywords the value of field 'keywords'.
     */
    public void setKeywords(java.lang.String keywords)
    {
        this._keywords = keywords;
    } //-- void setKeywords(java.lang.String) 

    /**
     * Sets the value of field 'modified'.
     * 
     * @param modified the value of field 'modified'.
     */
    public void setModified(java.util.Date modified)
    {
        this._modified = modified;
    } //-- void setModified(java.util.Date) 

    /**
     * Method setNews
     * 
     * @param index
     * @param vNews
     */
    public void setNews(int index, java.lang.String vNews)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _newsList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _newsList.set(index, vNews);
    } //-- void setNews(int, java.lang.String) 

    /**
     * Method setNews
     * 
     * @param newsArray
     */
    public void setNews(java.lang.String[] newsArray)
    {
        //-- copy array
        _newsList.clear();
        for (int i = 0; i < newsArray.length; i++) {
            _newsList.add(newsArray[i]);
        }
    } //-- void setNews(java.lang.String) 

    /**
     * Sets the value of field 'text'.
     * 
     * @param text the value of field 'text'.
     */
    public void setText(java.lang.String text)
    {
        this._text = text;
    } //-- void setText(java.lang.String) 

    /**
     * Sets the value of field 'title'.
     * 
     * @param title the value of field 'title'.
     */
    public void setTitle(java.lang.String title)
    {
        this._title = title;
    } //-- void setTitle(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hola.imagen.RefType) Unmarshaller.unmarshal(com.hola.imagen.RefType.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
