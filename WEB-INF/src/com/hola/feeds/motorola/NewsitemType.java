/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.motorola;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/


/**
 * Class NewsitemType.
 * 
 * @version $Revision$ $Date$
 */
public abstract class NewsitemType extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _publish
     */
    private java.lang.String _publish;

    /**
     * Field _expires
     */
    private java.lang.String _expires;

    /**
     * Field _headline
     */
    private java.lang.String _headline;

    /**
     * Field _dateline
     */
    private java.lang.String _dateline;

    /**
     * Field _text
     */
    private java.lang.String _text;

    /**
     * Field _image
     */
    private com.hola.feeds.motorola.Image _image;

    /**
     * Field _icon
     */
    private java.lang.String _icon;


      //----------------/
     //- Constructors -/
    //----------------/

    public NewsitemType() {
        super();
    } //-- com.hola.feeds.motorola.NewsitemType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method getDatelineReturns the value of field 'dateline'.
     * 
     * @return the value of field 'dateline'.
     */
    public java.lang.String getDateline()
    {
        return this._dateline;
    } //-- java.lang.String getDateline() 

    /**
     * Method getExpiresReturns the value of field 'expires'.
     * 
     * @return the value of field 'expires'.
     */
    public java.lang.String getExpires()
    {
        return this._expires;
    } //-- java.lang.String getExpires() 

    /**
     * Method getHeadlineReturns the value of field 'headline'.
     * 
     * @return the value of field 'headline'.
     */
    public java.lang.String getHeadline()
    {
        return this._headline;
    } //-- java.lang.String getHeadline() 

    /**
     * Method getIconReturns the value of field 'icon'.
     * 
     * @return the value of field 'icon'.
     */
    public java.lang.String getIcon()
    {
        return this._icon;
    } //-- java.lang.String getIcon() 

    /**
     * Method getImageReturns the value of field 'image'.
     * 
     * @return the value of field 'image'.
     */
    public com.hola.feeds.motorola.Image getImage()
    {
        return this._image;
    } //-- com.hola.feeds.motorola.Image getImage() 

    /**
     * Method getPublishReturns the value of field 'publish'.
     * 
     * @return the value of field 'publish'.
     */
    public java.lang.String getPublish()
    {
        return this._publish;
    } //-- java.lang.String getPublish() 

    /**
     * Method getTextReturns the value of field 'text'.
     * 
     * @return the value of field 'text'.
     */
    public java.lang.String getText()
    {
        return this._text;
    } //-- java.lang.String getText() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method setDatelineSets the value of field 'dateline'.
     * 
     * @param dateline the value of field 'dateline'.
     */
    public void setDateline(java.lang.String dateline)
    {
        this._dateline = dateline;
    } //-- void setDateline(java.lang.String) 

    /**
     * Method setExpiresSets the value of field 'expires'.
     * 
     * @param expires the value of field 'expires'.
     */
    public void setExpires(java.lang.String expires)
    {
        this._expires = expires;
    } //-- void setExpires(java.lang.String) 

    /**
     * Method setHeadlineSets the value of field 'headline'.
     * 
     * @param headline the value of field 'headline'.
     */
    public void setHeadline(java.lang.String headline)
    {
        this._headline = headline;
    } //-- void setHeadline(java.lang.String) 

    /**
     * Method setIconSets the value of field 'icon'.
     * 
     * @param icon the value of field 'icon'.
     */
    public void setIcon(java.lang.String icon)
    {
        this._icon = icon;
    } //-- void setIcon(java.lang.String) 

    /**
     * Method setImageSets the value of field 'image'.
     * 
     * @param image the value of field 'image'.
     */
    public void setImage(com.hola.feeds.motorola.Image image)
    {
        this._image = image;
    } //-- void setImage(com.hola.feeds.motorola.Image) 

    /**
     * Method setPublishSets the value of field 'publish'.
     * 
     * @param publish the value of field 'publish'.
     */
    public void setPublish(java.lang.String publish)
    {
        this._publish = publish;
    } //-- void setPublish(java.lang.String) 

    /**
     * Method setTextSets the value of field 'text'.
     * 
     * @param text the value of field 'text'.
     */
    public void setText(java.lang.String text)
    {
        this._text = text;
    } //-- void setText(java.lang.String) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
