/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * Class TGuid.
 * 
 * @version $Revision$ $Date$
 */
public abstract class TGuid extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * internal content storage
     */
    private java.lang.String _content = "";

    /**
     * Field _isPermaLink
     */
    private boolean _isPermaLink = true;

    /**
     * keeps track of state for field: _isPermaLink
     */
    private boolean _has_isPermaLink;


      //----------------/
     //- Constructors -/
    //----------------/

    public TGuid() {
        super();
        setContent("");
    } //-- com.hola.feeds.rss.TGuid()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteIsPermaLink
     */
    public void deleteIsPermaLink()
    {
        this._has_isPermaLink= false;
    } //-- void deleteIsPermaLink() 

    /**
     * Method getContentReturns the value of field 'content'. The
     * field 'content' has the following description: internal
     * content storage
     * 
     * @return the value of field 'content'.
     */
    public java.lang.String getContent()
    {
        return this._content;
    } //-- java.lang.String getContent() 

    /**
     * Method getIsPermaLinkReturns the value of field
     * 'isPermaLink'.
     * 
     * @return the value of field 'isPermaLink'.
     */
    public boolean getIsPermaLink()
    {
        return this._isPermaLink;
    } //-- boolean getIsPermaLink() 

    /**
     * Method hasIsPermaLink
     */
    public boolean hasIsPermaLink()
    {
        return this._has_isPermaLink;
    } //-- boolean hasIsPermaLink() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method setContentSets the value of field 'content'. The
     * field 'content' has the following description: internal
     * content storage
     * 
     * @param content the value of field 'content'.
     */
    public void setContent(java.lang.String content)
    {
        this._content = content;
    } //-- void setContent(java.lang.String) 

    /**
     * Method setIsPermaLinkSets the value of field 'isPermaLink'.
     * 
     * @param isPermaLink the value of field 'isPermaLink'.
     */
    public void setIsPermaLink(boolean isPermaLink)
    {
        this._isPermaLink = isPermaLink;
        this._has_isPermaLink = true;
    } //-- void setIsPermaLink(boolean) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
