/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss.types;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class TCloudProtocol.
 * 
 * @version $Revision$ $Date$
 */
public class TCloudProtocol implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * The xml-rpc type
     */
    public static final int VALUE_0_TYPE = 0;

    /**
     * The instance of the xml-rpc type
     */
    public static final TCloudProtocol VALUE_0 = new TCloudProtocol(VALUE_0_TYPE, "xml-rpc");

    /**
     * The http-post type
     */
    public static final int VALUE_1_TYPE = 1;

    /**
     * The instance of the http-post type
     */
    public static final TCloudProtocol VALUE_1 = new TCloudProtocol(VALUE_1_TYPE, "http-post");

    /**
     * The soap type
     */
    public static final int VALUE_2_TYPE = 2;

    /**
     * The instance of the soap type
     */
    public static final TCloudProtocol VALUE_2 = new TCloudProtocol(VALUE_2_TYPE, "soap");

    /**
     * Field _memberTable
     */
    private static java.util.Hashtable _memberTable = init();

    /**
     * Field type
     */
    private int type = -1;

    /**
     * Field stringValue
     */
    private java.lang.String stringValue = null;


      //----------------/
     //- Constructors -/
    //----------------/

    private TCloudProtocol(int type, java.lang.String value) {
        super();
        this.type = type;
        this.stringValue = value;
    } //-- com.hola.feeds.rss.types.TCloudProtocol(int, java.lang.String)


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method enumerateReturns an enumeration of all possible
     * instances of TCloudProtocol
     */
    public static java.util.Enumeration enumerate()
    {
        return _memberTable.elements();
    } //-- java.util.Enumeration enumerate() 

    /**
     * Method getTypeReturns the type of this TCloudProtocol
     */
    public int getType()
    {
        return this.type;
    } //-- int getType() 

    /**
     * Method init
     */
    private static java.util.Hashtable init()
    {
        Hashtable members = new Hashtable();
        members.put("xml-rpc", VALUE_0);
        members.put("http-post", VALUE_1);
        members.put("soap", VALUE_2);
        return members;
    } //-- java.util.Hashtable init() 

    /**
     * Method toStringReturns the String representation of this
     * TCloudProtocol
     */
    public java.lang.String toString()
    {
        return this.stringValue;
    } //-- java.lang.String toString() 

    /**
     * Method valueOfReturns a new TCloudProtocol based on the
     * given String value.
     * 
     * @param string
     */
    public static com.hola.feeds.rss.types.TCloudProtocol valueOf(java.lang.String string)
    {
        java.lang.Object obj = null;
        if (string != null) obj = _memberTable.get(string);
        if (obj == null) {
            String err = "'" + string + "' is not a valid TCloudProtocol";
            throw new IllegalArgumentException(err);
        }
        return (TCloudProtocol) obj;
    } //-- com.hola.feeds.rss.types.TCloudProtocol valueOf(java.lang.String) 

}
