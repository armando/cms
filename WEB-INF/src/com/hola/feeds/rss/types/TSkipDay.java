/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss.types;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * A day when aggregators should not request the channel data.
 * 
 * @version $Revision$ $Date$
 */
public class TSkipDay implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * The Monday type
     */
    public static final int MONDAY_TYPE = 0;

    /**
     * The instance of the Monday type
     */
    public static final TSkipDay MONDAY = new TSkipDay(MONDAY_TYPE, "Monday");

    /**
     * The Tuesday type
     */
    public static final int TUESDAY_TYPE = 1;

    /**
     * The instance of the Tuesday type
     */
    public static final TSkipDay TUESDAY = new TSkipDay(TUESDAY_TYPE, "Tuesday");

    /**
     * The Wednesday type
     */
    public static final int WEDNESDAY_TYPE = 2;

    /**
     * The instance of the Wednesday type
     */
    public static final TSkipDay WEDNESDAY = new TSkipDay(WEDNESDAY_TYPE, "Wednesday");

    /**
     * The Thursday type
     */
    public static final int THURSDAY_TYPE = 3;

    /**
     * The instance of the Thursday type
     */
    public static final TSkipDay THURSDAY = new TSkipDay(THURSDAY_TYPE, "Thursday");

    /**
     * The Friday type
     */
    public static final int FRIDAY_TYPE = 4;

    /**
     * The instance of the Friday type
     */
    public static final TSkipDay FRIDAY = new TSkipDay(FRIDAY_TYPE, "Friday");

    /**
     * The Saturday type
     */
    public static final int SATURDAY_TYPE = 5;

    /**
     * The instance of the Saturday type
     */
    public static final TSkipDay SATURDAY = new TSkipDay(SATURDAY_TYPE, "Saturday");

    /**
     * The Sunday type
     */
    public static final int SUNDAY_TYPE = 6;

    /**
     * The instance of the Sunday type
     */
    public static final TSkipDay SUNDAY = new TSkipDay(SUNDAY_TYPE, "Sunday");

    /**
     * Field _memberTable
     */
    private static java.util.Hashtable _memberTable = init();

    /**
     * Field type
     */
    private int type = -1;

    /**
     * Field stringValue
     */
    private java.lang.String stringValue = null;


      //----------------/
     //- Constructors -/
    //----------------/

    private TSkipDay(int type, java.lang.String value) {
        super();
        this.type = type;
        this.stringValue = value;
    } //-- com.hola.feeds.rss.types.TSkipDay(int, java.lang.String)


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method enumerateReturns an enumeration of all possible
     * instances of TSkipDay
     */
    public static java.util.Enumeration enumerate()
    {
        return _memberTable.elements();
    } //-- java.util.Enumeration enumerate() 

    /**
     * Method getTypeReturns the type of this TSkipDay
     */
    public int getType()
    {
        return this.type;
    } //-- int getType() 

    /**
     * Method init
     */
    private static java.util.Hashtable init()
    {
        Hashtable members = new Hashtable();
        members.put("Monday", MONDAY);
        members.put("Tuesday", TUESDAY);
        members.put("Wednesday", WEDNESDAY);
        members.put("Thursday", THURSDAY);
        members.put("Friday", FRIDAY);
        members.put("Saturday", SATURDAY);
        members.put("Sunday", SUNDAY);
        return members;
    } //-- java.util.Hashtable init() 

    /**
     * Method toStringReturns the String representation of this
     * TSkipDay
     */
    public java.lang.String toString()
    {
        return this.stringValue;
    } //-- java.lang.String toString() 

    /**
     * Method valueOfReturns a new TSkipDay based on the given
     * String value.
     * 
     * @param string
     */
    public static com.hola.feeds.rss.types.TSkipDay valueOf(java.lang.String string)
    {
        java.lang.Object obj = null;
        if (string != null) obj = _memberTable.get(string);
        if (obj == null) {
            String err = "'" + string + "' is not a valid TSkipDay";
            throw new IllegalArgumentException(err);
        }
        return (TSkipDay) obj;
    } //-- com.hola.feeds.rss.types.TSkipDay valueOf(java.lang.String) 

}
