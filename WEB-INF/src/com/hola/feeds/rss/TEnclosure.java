/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * Class TEnclosure.
 * 
 * @version $Revision$ $Date$
 */
public abstract class TEnclosure extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * internal content storage
     */
    private java.lang.String _content = "";

    /**
     * URL where the enclosure is located
     */
    private java.lang.String _url;

    /**
     * Size in bytes
     */
    private int _length;

    /**
     * keeps track of state for field: _length
     */
    private boolean _has_length;

    /**
     * MIME media-type of the enclosure
     */
    private java.lang.String _type;


      //----------------/
     //- Constructors -/
    //----------------/

    public TEnclosure() {
        super();
        setContent("");
    } //-- com.hola.feeds.rss.TEnclosure()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method getContentReturns the value of field 'content'. The
     * field 'content' has the following description: internal
     * content storage
     * 
     * @return the value of field 'content'.
     */
    public java.lang.String getContent()
    {
        return this._content;
    } //-- java.lang.String getContent() 

    /**
     * Method getLengthReturns the value of field 'length'. The
     * field 'length' has the following description: Size in bytes
     * 
     * @return the value of field 'length'.
     */
    public int getLength()
    {
        return this._length;
    } //-- int getLength() 

    /**
     * Method getTypeReturns the value of field 'type'. The field
     * 'type' has the following description: MIME media-type of the
     * enclosure
     * 
     * @return the value of field 'type'.
     */
    public java.lang.String getType()
    {
        return this._type;
    } //-- java.lang.String getType() 

    /**
     * Method getUrlReturns the value of field 'url'. The field
     * 'url' has the following description: URL where the enclosure
     * is located
     * 
     * @return the value of field 'url'.
     */
    public java.lang.String getUrl()
    {
        return this._url;
    } //-- java.lang.String getUrl() 

    /**
     * Method hasLength
     */
    public boolean hasLength()
    {
        return this._has_length;
    } //-- boolean hasLength() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method setContentSets the value of field 'content'. The
     * field 'content' has the following description: internal
     * content storage
     * 
     * @param content the value of field 'content'.
     */
    public void setContent(java.lang.String content)
    {
        this._content = content;
    } //-- void setContent(java.lang.String) 

    /**
     * Method setLengthSets the value of field 'length'. The field
     * 'length' has the following description: Size in bytes
     * 
     * @param length the value of field 'length'.
     */
    public void setLength(int length)
    {
        this._length = length;
        this._has_length = true;
    } //-- void setLength(int) 

    /**
     * Method setTypeSets the value of field 'type'. The field
     * 'type' has the following description: MIME media-type of the
     * enclosure
     * 
     * @param type the value of field 'type'.
     */
    public void setType(java.lang.String type)
    {
        this._type = type;
    } //-- void setType(java.lang.String) 

    /**
     * Method setUrlSets the value of field 'url'. The field 'url'
     * has the following description: URL where the enclosure is
     * located
     * 
     * @param url the value of field 'url'.
     */
    public void setUrl(java.lang.String url)
    {
        this._url = url;
    } //-- void setUrl(java.lang.String) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
