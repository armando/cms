/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * Class TRss.
 * 
 * @version $Revision$ $Date$
 */
public abstract class TRss extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _version
     */
    private java.math.BigDecimal _version = new java.math.BigDecimal("2.0");

    /**
     * Field _channel
     */
    private com.hola.feeds.rss.Channel _channel;


      //----------------/
     //- Constructors -/
    //----------------/

    public TRss() {
        super();
        setVersion(new java.math.BigDecimal("2.0"));
    } //-- com.hola.feeds.rss.TRss()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method getChannelReturns the value of field 'channel'.
     * 
     * @return the value of field 'channel'.
     */
    public com.hola.feeds.rss.Channel getChannel()
    {
        return this._channel;
    } //-- com.hola.feeds.rss.Channel getChannel() 

    /**
     * Method getVersionReturns the value of field 'version'.
     * 
     * @return the value of field 'version'.
     */
    public java.math.BigDecimal getVersion()
    {
        return this._version;
    } //-- java.math.BigDecimal getVersion() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method setChannelSets the value of field 'channel'.
     * 
     * @param channel the value of field 'channel'.
     */
    public void setChannel(com.hola.feeds.rss.Channel channel)
    {
        this._channel = channel;
    } //-- void setChannel(com.hola.feeds.rss.Channel) 

    /**
     * Method setVersionSets the value of field 'version'.
     * 
     * @param version the value of field 'version'.
     */
    public void setVersion(java.math.BigDecimal version)
    {
        this._version = version;
    } //-- void setVersion(java.math.BigDecimal) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
