/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * Class TImage.
 * 
 * @version $Revision$ $Date$
 */
public abstract class TImage extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * The URL of the image file.
     */
    private java.lang.String _url;

    /**
     * Describes the image, it's used in the ALT attribute of the
     * HTML <img> tag when the channel is rendered in HTML.
     */
    private java.lang.String _title;

    /**
     * The URL of the site, when the channel is rendered, the image
     * is a link to the site. (Note, in practice the image <title>
     * and <link> should have the same value as the channel's
     * <title> and <link>. 
     */
    private java.lang.String _link;

    /**
     * The width of the image in pixels.
     */
    private int _width = 88;

    /**
     * keeps track of state for field: _width
     */
    private boolean _has_width;

    /**
     * The height of the image in pixels.
     */
    private int _height = 31;

    /**
     * keeps track of state for field: _height
     */
    private boolean _has_height;

    /**
     * Text that is included in the TITLE attribute of the link
     * formed around the image in the HTML rendering.
     */
    private java.lang.String _description;


      //----------------/
     //- Constructors -/
    //----------------/

    public TImage() {
        super();
    } //-- com.hola.feeds.rss.TImage()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteHeight
     */
    public void deleteHeight()
    {
        this._has_height= false;
    } //-- void deleteHeight() 

    /**
     * Method deleteWidth
     */
    public void deleteWidth()
    {
        this._has_width= false;
    } //-- void deleteWidth() 

    /**
     * Method getDescriptionReturns the value of field
     * 'description'. The field 'description' has the following
     * description: Text that is included in the TITLE attribute of
     * the link formed around the image in the HTML rendering.
     * 
     * @return the value of field 'description'.
     */
    public java.lang.String getDescription()
    {
        return this._description;
    } //-- java.lang.String getDescription() 

    /**
     * Method getHeightReturns the value of field 'height'. The
     * field 'height' has the following description: The height of
     * the image in pixels.
     * 
     * @return the value of field 'height'.
     */
    public int getHeight()
    {
        return this._height;
    } //-- int getHeight() 

    /**
     * Method getLinkReturns the value of field 'link'. The field
     * 'link' has the following description: The URL of the site,
     * when the channel is rendered, the image is a link to the
     * site. (Note, in practice the image <title> and <link> should
     * have the same value as the channel's <title> and <link>. 
     * 
     * @return the value of field 'link'.
     */
    public java.lang.String getLink()
    {
        return this._link;
    } //-- java.lang.String getLink() 

    /**
     * Method getTitleReturns the value of field 'title'. The field
     * 'title' has the following description: Describes the image,
     * it's used in the ALT attribute of the HTML <img> tag when
     * the channel is rendered in HTML.
     * 
     * @return the value of field 'title'.
     */
    public java.lang.String getTitle()
    {
        return this._title;
    } //-- java.lang.String getTitle() 

    /**
     * Method getUrlReturns the value of field 'url'. The field
     * 'url' has the following description: The URL of the image
     * file.
     * 
     * @return the value of field 'url'.
     */
    public java.lang.String getUrl()
    {
        return this._url;
    } //-- java.lang.String getUrl() 

    /**
     * Method getWidthReturns the value of field 'width'. The field
     * 'width' has the following description: The width of the
     * image in pixels.
     * 
     * @return the value of field 'width'.
     */
    public int getWidth()
    {
        return this._width;
    } //-- int getWidth() 

    /**
     * Method hasHeight
     */
    public boolean hasHeight()
    {
        return this._has_height;
    } //-- boolean hasHeight() 

    /**
     * Method hasWidth
     */
    public boolean hasWidth()
    {
        return this._has_width;
    } //-- boolean hasWidth() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method setDescriptionSets the value of field 'description'.
     * The field 'description' has the following description: Text
     * that is included in the TITLE attribute of the link formed
     * around the image in the HTML rendering.
     * 
     * @param description the value of field 'description'.
     */
    public void setDescription(java.lang.String description)
    {
        this._description = description;
    } //-- void setDescription(java.lang.String) 

    /**
     * Method setHeightSets the value of field 'height'. The field
     * 'height' has the following description: The height of the
     * image in pixels.
     * 
     * @param height the value of field 'height'.
     */
    public void setHeight(int height)
    {
        this._height = height;
        this._has_height = true;
    } //-- void setHeight(int) 

    /**
     * Method setLinkSets the value of field 'link'. The field
     * 'link' has the following description: The URL of the site,
     * when the channel is rendered, the image is a link to the
     * site. (Note, in practice the image <title> and <link> should
     * have the same value as the channel's <title> and <link>. 
     * 
     * @param link the value of field 'link'.
     */
    public void setLink(java.lang.String link)
    {
        this._link = link;
    } //-- void setLink(java.lang.String) 

    /**
     * Method setTitleSets the value of field 'title'. The field
     * 'title' has the following description: Describes the image,
     * it's used in the ALT attribute of the HTML <img> tag when
     * the channel is rendered in HTML.
     * 
     * @param title the value of field 'title'.
     */
    public void setTitle(java.lang.String title)
    {
        this._title = title;
    } //-- void setTitle(java.lang.String) 

    /**
     * Method setUrlSets the value of field 'url'. The field 'url'
     * has the following description: The URL of the image file.
     * 
     * @param url the value of field 'url'.
     */
    public void setUrl(java.lang.String url)
    {
        this._url = url;
    } //-- void setUrl(java.lang.String) 

    /**
     * Method setWidthSets the value of field 'width'. The field
     * 'width' has the following description: The width of the
     * image in pixels.
     * 
     * @param width the value of field 'width'.
     */
    public void setWidth(int width)
    {
        this._width = width;
        this._has_width = true;
    } //-- void setWidth(int) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
