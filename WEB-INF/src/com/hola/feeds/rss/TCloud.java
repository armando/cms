/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import com.hola.feeds.rss.types.TCloudProtocol;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * Specifies a web service that supports the rssCloud interface
 * which can be implemented in HTTP-POST, XML-RPC or SOAP 1.1. 
 * Its purpose is to allow processes to register with a cloud to be
 * notified of updates to the channel, implementing a lightweight
 * publish-subscribe protocol for RSS feeds.
 * 
 * @version $Revision$ $Date$
 */
public abstract class TCloud extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _domain
     */
    private java.lang.String _domain;

    /**
     * Field _port
     */
    private int _port;

    /**
     * keeps track of state for field: _port
     */
    private boolean _has_port;

    /**
     * Field _path
     */
    private java.lang.String _path;

    /**
     * Field _registerProcedure
     */
    private java.lang.String _registerProcedure;

    /**
     * Field _protocol
     */
    private com.hola.feeds.rss.types.TCloudProtocol _protocol;


      //----------------/
     //- Constructors -/
    //----------------/

    public TCloud() {
        super();
    } //-- com.hola.feeds.rss.TCloud()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method getDomainReturns the value of field 'domain'.
     * 
     * @return the value of field 'domain'.
     */
    public java.lang.String getDomain()
    {
        return this._domain;
    } //-- java.lang.String getDomain() 

    /**
     * Method getPathReturns the value of field 'path'.
     * 
     * @return the value of field 'path'.
     */
    public java.lang.String getPath()
    {
        return this._path;
    } //-- java.lang.String getPath() 

    /**
     * Method getPortReturns the value of field 'port'.
     * 
     * @return the value of field 'port'.
     */
    public int getPort()
    {
        return this._port;
    } //-- int getPort() 

    /**
     * Method getProtocolReturns the value of field 'protocol'.
     * 
     * @return the value of field 'protocol'.
     */
    public com.hola.feeds.rss.types.TCloudProtocol getProtocol()
    {
        return this._protocol;
    } //-- com.hola.feeds.rss.types.TCloudProtocol getProtocol() 

    /**
     * Method getRegisterProcedureReturns the value of field
     * 'registerProcedure'.
     * 
     * @return the value of field 'registerProcedure'.
     */
    public java.lang.String getRegisterProcedure()
    {
        return this._registerProcedure;
    } //-- java.lang.String getRegisterProcedure() 

    /**
     * Method hasPort
     */
    public boolean hasPort()
    {
        return this._has_port;
    } //-- boolean hasPort() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method setDomainSets the value of field 'domain'.
     * 
     * @param domain the value of field 'domain'.
     */
    public void setDomain(java.lang.String domain)
    {
        this._domain = domain;
    } //-- void setDomain(java.lang.String) 

    /**
     * Method setPathSets the value of field 'path'.
     * 
     * @param path the value of field 'path'.
     */
    public void setPath(java.lang.String path)
    {
        this._path = path;
    } //-- void setPath(java.lang.String) 

    /**
     * Method setPortSets the value of field 'port'.
     * 
     * @param port the value of field 'port'.
     */
    public void setPort(int port)
    {
        this._port = port;
        this._has_port = true;
    } //-- void setPort(int) 

    /**
     * Method setProtocolSets the value of field 'protocol'.
     * 
     * @param protocol the value of field 'protocol'.
     */
    public void setProtocol(com.hola.feeds.rss.types.TCloudProtocol protocol)
    {
        this._protocol = protocol;
    } //-- void setProtocol(com.hola.feeds.rss.types.TCloudProtocol) 

    /**
     * Method setRegisterProcedureSets the value of field
     * 'registerProcedure'.
     * 
     * @param registerProcedure the value of field
     * 'registerProcedure'.
     */
    public void setRegisterProcedure(java.lang.String registerProcedure)
    {
        this._registerProcedure = registerProcedure;
    } //-- void setRegisterProcedure(java.lang.String) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
