/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * Class TRssChannel.
 * 
 * @version $Revision$ $Date$
 */
public abstract class TRssChannel extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * The name of the channel. It's how people refer to your
     * service. If you have an HTML website that contains the same
     * information as your RSS file, the title of your channel
     * should be the same as the title of your website.
     */
    private java.lang.String _title;

    /**
     * The URL to the HTML website corresponding to the channel.
     */
    private java.lang.String _link;

    /**
     * Phrase or sentence describing the channel.
     */
    private java.lang.String _description;

    /**
     * The language the channel is written in. This allows
     * aggregators to group all Italian language sites, for
     * example, on a single page. A list of allowable values for
     * this element, as provided by Netscape, is here. You may also
     * use values defined by the W3C.
     */
    private java.lang.String _language;

    /**
     * Copyright notice for content in the channel.
     */
    private java.lang.String _copyright;

    /**
     * Email address for person responsible for editorial content.
     */
    private java.lang.String _managingEditor;

    /**
     * Email address for person responsible for technical issues
     * relating to channel.
     */
    private java.lang.String _webMaster;

    /**
     * The publication date for the content in the channel. All
     * date-times in RSS conform to the Date and Time Specification
     * of RFC 822, with the exception that the year may be
     * expressed with two characters or four characters (four
     * preferred).
     */
    private java.lang.String _pubDate;

    /**
     * The last time the content of the channel changed.
     */
    private java.lang.String _lastBuildDate;

    /**
     * Specify one or more categories that the channel belongs to.
     */
    private com.hola.feeds.rss.Category _category;

    /**
     * A string indicating the program used to generate the channel.
     */
    private java.lang.String _generator;

    /**
     * A URL that points to the documentation for the format used
     * in the RSS file. It's probably a pointer to this page. It's
     * for people who might stumble across an RSS file on a Web
     * server 25 years from now and wonder what it is.
     */
    private java.lang.String _docs;

    /**
     * Allows processes to register with a cloud to be notified of
     * updates to the channel, implementing a lightweight
     * publish-subscribe protocol for RSS feeds.
     */
    private com.hola.feeds.rss.Cloud _cloud;

    /**
     * ttl stands for time to live. It's a number of minutes that
     * indicates how long a channel can be cached before refreshing
     * from the source.
     */
    private int _ttl;

    /**
     * keeps track of state for field: _ttl
     */
    private boolean _has_ttl;

    /**
     * Specifies a GIF, JPEG or PNG image that can be displayed
     * with the channel.
     */
    private com.hola.feeds.rss.Image _image;

    /**
     * Specifies a text input box that can be displayed with the
     * channel.
     */
    private com.hola.feeds.rss.TextInput _textInput;

    /**
     * A hint for aggregators telling them which hours they can skip
     */
    private com.hola.feeds.rss.SkipHours _skipHours;

    /**
     * A hint for aggregators telling them which days they can skip.
     */
    private com.hola.feeds.rss.SkipDays _skipDays;

    /**
     * Field _itemList
     */
    private java.util.ArrayList _itemList;


      //----------------/
     //- Constructors -/
    //----------------/

    public TRssChannel() {
        super();
        _itemList = new ArrayList();
    } //-- com.hola.feeds.rss.TRssChannel()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addItem
     * 
     * @param vItem
     */
    public void addItem(com.hola.feeds.rss.Item vItem)
        throws java.lang.IndexOutOfBoundsException
    {
        _itemList.add(vItem);
    } //-- void addItem(com.hola.feeds.rss.Item) 

    /**
     * Method addItem
     * 
     * @param index
     * @param vItem
     */
    public void addItem(int index, com.hola.feeds.rss.Item vItem)
        throws java.lang.IndexOutOfBoundsException
    {
        _itemList.add(index, vItem);
    } //-- void addItem(int, com.hola.feeds.rss.Item) 

    /**
     * Method clearItem
     */
    public void clearItem()
    {
        _itemList.clear();
    } //-- void clearItem() 

    /**
     * Method deleteTtl
     */
    public void deleteTtl()
    {
        this._has_ttl= false;
    } //-- void deleteTtl() 

    /**
     * Method enumerateItem
     */
    public java.util.Enumeration enumerateItem()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_itemList.iterator());
    } //-- java.util.Enumeration enumerateItem() 

    /**
     * Method getCategoryReturns the value of field 'category'. The
     * field 'category' has the following description: Specify one
     * or more categories that the channel belongs to.
     * 
     * @return the value of field 'category'.
     */
    public com.hola.feeds.rss.Category getCategory()
    {
        return this._category;
    } //-- com.hola.feeds.rss.Category getCategory() 

    /**
     * Method getCloudReturns the value of field 'cloud'. The field
     * 'cloud' has the following description: Allows processes to
     * register with a cloud to be notified of updates to the
     * channel, implementing a lightweight publish-subscribe
     * protocol for RSS feeds.
     * 
     * @return the value of field 'cloud'.
     */
    public com.hola.feeds.rss.Cloud getCloud()
    {
        return this._cloud;
    } //-- com.hola.feeds.rss.Cloud getCloud() 

    /**
     * Method getCopyrightReturns the value of field 'copyright'.
     * The field 'copyright' has the following description:
     * Copyright notice for content in the channel.
     * 
     * @return the value of field 'copyright'.
     */
    public java.lang.String getCopyright()
    {
        return this._copyright;
    } //-- java.lang.String getCopyright() 

    /**
     * Method getDescriptionReturns the value of field
     * 'description'. The field 'description' has the following
     * description: Phrase or sentence describing the channel.
     * 
     * @return the value of field 'description'.
     */
    public java.lang.String getDescription()
    {
        return this._description;
    } //-- java.lang.String getDescription() 

    /**
     * Method getDocsReturns the value of field 'docs'. The field
     * 'docs' has the following description: A URL that points to
     * the documentation for the format used in the RSS file. It's
     * probably a pointer to this page. It's for people who might
     * stumble across an RSS file on a Web server 25 years from now
     * and wonder what it is.
     * 
     * @return the value of field 'docs'.
     */
    public java.lang.String getDocs()
    {
        return this._docs;
    } //-- java.lang.String getDocs() 

    /**
     * Method getGeneratorReturns the value of field 'generator'.
     * The field 'generator' has the following description: A
     * string indicating the program used to generate the channel.
     * 
     * @return the value of field 'generator'.
     */
    public java.lang.String getGenerator()
    {
        return this._generator;
    } //-- java.lang.String getGenerator() 

    /**
     * Method getImageReturns the value of field 'image'. The field
     * 'image' has the following description: Specifies a GIF, JPEG
     * or PNG image that can be displayed with the channel.
     * 
     * @return the value of field 'image'.
     */
    public com.hola.feeds.rss.Image getImage()
    {
        return this._image;
    } //-- com.hola.feeds.rss.Image getImage() 

    /**
     * Method getItem
     * 
     * @param index
     */
    public com.hola.feeds.rss.Item getItem(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _itemList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hola.feeds.rss.Item) _itemList.get(index);
    } //-- com.hola.feeds.rss.Item getItem(int) 

    /**
     * Method getItem
     */
    public com.hola.feeds.rss.Item[] getItem()
    {
        int size = _itemList.size();
        com.hola.feeds.rss.Item[] mArray = new com.hola.feeds.rss.Item[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hola.feeds.rss.Item) _itemList.get(index);
        }
        return mArray;
    } //-- com.hola.feeds.rss.Item[] getItem() 

    /**
     * Method getItemCount
     */
    public int getItemCount()
    {
        return _itemList.size();
    } //-- int getItemCount() 

    /**
     * Method getLanguageReturns the value of field 'language'. The
     * field 'language' has the following description: The language
     * the channel is written in. This allows aggregators to group
     * all Italian language sites, for example, on a single page. A
     * list of allowable values for this element, as provided by
     * Netscape, is here. You may also use values defined by the
     * W3C.
     * 
     * @return the value of field 'language'.
     */
    public java.lang.String getLanguage()
    {
        return this._language;
    } //-- java.lang.String getLanguage() 

    /**
     * Method getLastBuildDateReturns the value of field
     * 'lastBuildDate'. The field 'lastBuildDate' has the following
     * description: The last time the content of the channel
     * changed.
     * 
     * @return the value of field 'lastBuildDate'.
     */
    public java.lang.String getLastBuildDate()
    {
        return this._lastBuildDate;
    } //-- java.lang.String getLastBuildDate() 

    /**
     * Method getLinkReturns the value of field 'link'. The field
     * 'link' has the following description: The URL to the HTML
     * website corresponding to the channel.
     * 
     * @return the value of field 'link'.
     */
    public java.lang.String getLink()
    {
        return this._link;
    } //-- java.lang.String getLink() 

    /**
     * Method getManagingEditorReturns the value of field
     * 'managingEditor'. The field 'managingEditor' has the
     * following description: Email address for person responsible
     * for editorial content.
     * 
     * @return the value of field 'managingEditor'.
     */
    public java.lang.String getManagingEditor()
    {
        return this._managingEditor;
    } //-- java.lang.String getManagingEditor() 

    /**
     * Method getPubDateReturns the value of field 'pubDate'. The
     * field 'pubDate' has the following description: The
     * publication date for the content in the channel. All
     * date-times in RSS conform to the Date and Time Specification
     * of RFC 822, with the exception that the year may be
     * expressed with two characters or four characters (four
     * preferred).
     * 
     * @return the value of field 'pubDate'.
     */
    public java.lang.String getPubDate()
    {
        return this._pubDate;
    } //-- java.lang.String getPubDate() 

    /**
     * Method getSkipDaysReturns the value of field 'skipDays'. The
     * field 'skipDays' has the following description: A hint for
     * aggregators telling them which days they can skip.
     * 
     * @return the value of field 'skipDays'.
     */
    public com.hola.feeds.rss.SkipDays getSkipDays()
    {
        return this._skipDays;
    } //-- com.hola.feeds.rss.SkipDays getSkipDays() 

    /**
     * Method getSkipHoursReturns the value of field 'skipHours'.
     * The field 'skipHours' has the following description: A hint
     * for aggregators telling them which hours they can skip.
     * 
     * @return the value of field 'skipHours'.
     */
    public com.hola.feeds.rss.SkipHours getSkipHours()
    {
        return this._skipHours;
    } //-- com.hola.feeds.rss.SkipHours getSkipHours() 

    /**
     * Method getTextInputReturns the value of field 'textInput'.
     * The field 'textInput' has the following description:
     * Specifies a text input box that can be displayed with the
     * channel.
     * 
     * @return the value of field 'textInput'.
     */
    public com.hola.feeds.rss.TextInput getTextInput()
    {
        return this._textInput;
    } //-- com.hola.feeds.rss.TextInput getTextInput() 

    /**
     * Method getTitleReturns the value of field 'title'. The field
     * 'title' has the following description: The name of the
     * channel. It's how people refer to your service. If you have
     * an HTML website that contains the same information as your
     * RSS file, the title of your channel should be the same as
     * the title of your website.
     * 
     * @return the value of field 'title'.
     */
    public java.lang.String getTitle()
    {
        return this._title;
    } //-- java.lang.String getTitle() 

    /**
     * Method getTtlReturns the value of field 'ttl'. The field
     * 'ttl' has the following description: ttl stands for time to
     * live. It's a number of minutes that indicates how long a
     * channel can be cached before refreshing from the source.
     * 
     * @return the value of field 'ttl'.
     */
    public int getTtl()
    {
        return this._ttl;
    } //-- int getTtl() 

    /**
     * Method getWebMasterReturns the value of field 'webMaster'.
     * The field 'webMaster' has the following description: Email
     * address for person responsible for technical issues relating
     * to channel.
     * 
     * @return the value of field 'webMaster'.
     */
    public java.lang.String getWebMaster()
    {
        return this._webMaster;
    } //-- java.lang.String getWebMaster() 

    /**
     * Method hasTtl
     */
    public boolean hasTtl()
    {
        return this._has_ttl;
    } //-- boolean hasTtl() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method removeItem
     * 
     * @param vItem
     */
    public boolean removeItem(com.hola.feeds.rss.Item vItem)
    {
        boolean removed = _itemList.remove(vItem);
        return removed;
    } //-- boolean removeItem(com.hola.feeds.rss.Item) 

    /**
     * Method setCategorySets the value of field 'category'. The
     * field 'category' has the following description: Specify one
     * or more categories that the channel belongs to.
     * 
     * @param category the value of field 'category'.
     */
    public void setCategory(com.hola.feeds.rss.Category category)
    {
        this._category = category;
    } //-- void setCategory(com.hola.feeds.rss.Category) 

    /**
     * Method setCloudSets the value of field 'cloud'. The field
     * 'cloud' has the following description: Allows processes to
     * register with a cloud to be notified of updates to the
     * channel, implementing a lightweight publish-subscribe
     * protocol for RSS feeds.
     * 
     * @param cloud the value of field 'cloud'.
     */
    public void setCloud(com.hola.feeds.rss.Cloud cloud)
    {
        this._cloud = cloud;
    } //-- void setCloud(com.hola.feeds.rss.Cloud) 

    /**
     * Method setCopyrightSets the value of field 'copyright'. The
     * field 'copyright' has the following description: Copyright
     * notice for content in the channel.
     * 
     * @param copyright the value of field 'copyright'.
     */
    public void setCopyright(java.lang.String copyright)
    {
        this._copyright = copyright;
    } //-- void setCopyright(java.lang.String) 

    /**
     * Method setDescriptionSets the value of field 'description'.
     * The field 'description' has the following description:
     * Phrase or sentence describing the channel.
     * 
     * @param description the value of field 'description'.
     */
    public void setDescription(java.lang.String description)
    {
        this._description = description;
    } //-- void setDescription(java.lang.String) 

    /**
     * Method setDocsSets the value of field 'docs'. The field
     * 'docs' has the following description: A URL that points to
     * the documentation for the format used in the RSS file. It's
     * probably a pointer to this page. It's for people who might
     * stumble across an RSS file on a Web server 25 years from now
     * and wonder what it is.
     * 
     * @param docs the value of field 'docs'.
     */
    public void setDocs(java.lang.String docs)
    {
        this._docs = docs;
    } //-- void setDocs(java.lang.String) 

    /**
     * Method setGeneratorSets the value of field 'generator'. The
     * field 'generator' has the following description: A string
     * indicating the program used to generate the channel.
     * 
     * @param generator the value of field 'generator'.
     */
    public void setGenerator(java.lang.String generator)
    {
        this._generator = generator;
    } //-- void setGenerator(java.lang.String) 

    /**
     * Method setImageSets the value of field 'image'. The field
     * 'image' has the following description: Specifies a GIF, JPEG
     * or PNG image that can be displayed with the channel.
     * 
     * @param image the value of field 'image'.
     */
    public void setImage(com.hola.feeds.rss.Image image)
    {
        this._image = image;
    } //-- void setImage(com.hola.feeds.rss.Image) 

    /**
     * Method setItem
     * 
     * @param index
     * @param vItem
     */
    public void setItem(int index, com.hola.feeds.rss.Item vItem)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _itemList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _itemList.set(index, vItem);
    } //-- void setItem(int, com.hola.feeds.rss.Item) 

    /**
     * Method setItem
     * 
     * @param itemArray
     */
    public void setItem(com.hola.feeds.rss.Item[] itemArray)
    {
        //-- copy array
        _itemList.clear();
        for (int i = 0; i < itemArray.length; i++) {
            _itemList.add(itemArray[i]);
        }
    } //-- void setItem(com.hola.feeds.rss.Item) 

    /**
     * Method setLanguageSets the value of field 'language'. The
     * field 'language' has the following description: The language
     * the channel is written in. This allows aggregators to group
     * all Italian language sites, for example, on a single page. A
     * list of allowable values for this element, as provided by
     * Netscape, is here. You may also use values defined by the
     * W3C.
     * 
     * @param language the value of field 'language'.
     */
    public void setLanguage(java.lang.String language)
    {
        this._language = language;
    } //-- void setLanguage(java.lang.String) 

    /**
     * Method setLastBuildDateSets the value of field
     * 'lastBuildDate'. The field 'lastBuildDate' has the following
     * description: The last time the content of the channel
     * changed.
     * 
     * @param lastBuildDate the value of field 'lastBuildDate'.
     */
    public void setLastBuildDate(java.lang.String lastBuildDate)
    {
        this._lastBuildDate = lastBuildDate;
    } //-- void setLastBuildDate(java.lang.String) 

    /**
     * Method setLinkSets the value of field 'link'. The field
     * 'link' has the following description: The URL to the HTML
     * website corresponding to the channel.
     * 
     * @param link the value of field 'link'.
     */
    public void setLink(java.lang.String link)
    {
        this._link = link;
    } //-- void setLink(java.lang.String) 

    /**
     * Method setManagingEditorSets the value of field
     * 'managingEditor'. The field 'managingEditor' has the
     * following description: Email address for person responsible
     * for editorial content.
     * 
     * @param managingEditor the value of field 'managingEditor'.
     */
    public void setManagingEditor(java.lang.String managingEditor)
    {
        this._managingEditor = managingEditor;
    } //-- void setManagingEditor(java.lang.String) 

    /**
     * Method setPubDateSets the value of field 'pubDate'. The
     * field 'pubDate' has the following description: The
     * publication date for the content in the channel. All
     * date-times in RSS conform to the Date and Time Specification
     * of RFC 822, with the exception that the year may be
     * expressed with two characters or four characters (four
     * preferred).
     * 
     * @param pubDate the value of field 'pubDate'.
     */
    public void setPubDate(java.lang.String pubDate)
    {
        this._pubDate = pubDate;
    } //-- void setPubDate(java.lang.String) 

    /**
     * Method setSkipDaysSets the value of field 'skipDays'. The
     * field 'skipDays' has the following description: A hint for
     * aggregators telling them which days they can skip.
     * 
     * @param skipDays the value of field 'skipDays'.
     */
    public void setSkipDays(com.hola.feeds.rss.SkipDays skipDays)
    {
        this._skipDays = skipDays;
    } //-- void setSkipDays(com.hola.feeds.rss.SkipDays) 

    /**
     * Method setSkipHoursSets the value of field 'skipHours'. The
     * field 'skipHours' has the following description: A hint for
     * aggregators telling them which hours they can skip.
     * 
     * @param skipHours the value of field 'skipHours'.
     */
    public void setSkipHours(com.hola.feeds.rss.SkipHours skipHours)
    {
        this._skipHours = skipHours;
    } //-- void setSkipHours(com.hola.feeds.rss.SkipHours) 

    /**
     * Method setTextInputSets the value of field 'textInput'. The
     * field 'textInput' has the following description: Specifies a
     * text input box that can be displayed with the channel.
     * 
     * @param textInput the value of field 'textInput'.
     */
    public void setTextInput(com.hola.feeds.rss.TextInput textInput)
    {
        this._textInput = textInput;
    } //-- void setTextInput(com.hola.feeds.rss.TextInput) 

    /**
     * Method setTitleSets the value of field 'title'. The field
     * 'title' has the following description: The name of the
     * channel. It's how people refer to your service. If you have
     * an HTML website that contains the same information as your
     * RSS file, the title of your channel should be the same as
     * the title of your website.
     * 
     * @param title the value of field 'title'.
     */
    public void setTitle(java.lang.String title)
    {
        this._title = title;
    } //-- void setTitle(java.lang.String) 

    /**
     * Method setTtlSets the value of field 'ttl'. The field 'ttl'
     * has the following description: ttl stands for time to live.
     * It's a number of minutes that indicates how long a channel
     * can be cached before refreshing from the source.
     * 
     * @param ttl the value of field 'ttl'.
     */
    public void setTtl(int ttl)
    {
        this._ttl = ttl;
        this._has_ttl = true;
    } //-- void setTtl(int) 

    /**
     * Method setWebMasterSets the value of field 'webMaster'. The
     * field 'webMaster' has the following description: Email
     * address for person responsible for technical issues relating
     * to channel.
     * 
     * @param webMaster the value of field 'webMaster'.
     */
    public void setWebMaster(java.lang.String webMaster)
    {
        this._webMaster = webMaster;
    } //-- void setWebMaster(java.lang.String) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
