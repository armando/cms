/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * An item may represent a "story" -- much like a story in a
 * newspaper or magazine; if so its description is a synopsis of
 * the story, and the link points to the full story. An item may
 * also be complete in itself, if so, the description contains the
 * text (entity-encoded HTML is allowed), and the link and title
 * may be omitted.
 * 
 * @version $Revision$ $Date$
 */
public abstract class TRssItem extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * The title of the item.
     */
    private java.lang.String _title;

    /**
     * The item synopsis.
     */
    private java.lang.String _description;

    /**
     * The URL of the item.
     */
    private java.lang.String _link;

    /**
     * Email address of the author of the item.
     */
    private java.lang.String _author;

    /**
     * Includes the item in one or more categories. 
     */
    private com.hola.feeds.rss.Category _category;

    /**
     * URL of a page for comments relating to the item.
     */
    private java.lang.String _comments;

    /**
     * Describes a media object that is attached to the item.
     */
    private com.hola.feeds.rss.Enclosure _enclosure;

    /**
     * guid or permalink URL for this entry
     */
    private com.hola.feeds.rss.Guid _guid;

    /**
     * Indicates when the item was published.
     */
    private java.lang.String _pubDate;

    /**
     * The RSS channel that the item came from.
     */
    private com.hola.feeds.rss.Source _source;


      //----------------/
     //- Constructors -/
    //----------------/

    public TRssItem() {
        super();
    } //-- com.hola.feeds.rss.TRssItem()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method getAuthorReturns the value of field 'author'. The
     * field 'author' has the following description: Email address
     * of the author of the item.
     * 
     * @return the value of field 'author'.
     */
    public java.lang.String getAuthor()
    {
        return this._author;
    } //-- java.lang.String getAuthor() 

    /**
     * Method getCategoryReturns the value of field 'category'. The
     * field 'category' has the following description: Includes the
     * item in one or more categories. 
     * 
     * @return the value of field 'category'.
     */
    public com.hola.feeds.rss.Category getCategory()
    {
        return this._category;
    } //-- com.hola.feeds.rss.Category getCategory() 

    /**
     * Method getCommentsReturns the value of field 'comments'. The
     * field 'comments' has the following description: URL of a
     * page for comments relating to the item.
     * 
     * @return the value of field 'comments'.
     */
    public java.lang.String getComments()
    {
        return this._comments;
    } //-- java.lang.String getComments() 

    /**
     * Method getDescriptionReturns the value of field
     * 'description'. The field 'description' has the following
     * description: The item synopsis.
     * 
     * @return the value of field 'description'.
     */
    public java.lang.String getDescription()
    {
        return this._description;
    } //-- java.lang.String getDescription() 

    /**
     * Method getEnclosureReturns the value of field 'enclosure'.
     * The field 'enclosure' has the following description:
     * Describes a media object that is attached to the item.
     * 
     * @return the value of field 'enclosure'.
     */
    public com.hola.feeds.rss.Enclosure getEnclosure()
    {
        return this._enclosure;
    } //-- com.hola.feeds.rss.Enclosure getEnclosure() 

    /**
     * Method getGuidReturns the value of field 'guid'. The field
     * 'guid' has the following description: guid or permalink URL
     * for this entry
     * 
     * @return the value of field 'guid'.
     */
    public com.hola.feeds.rss.Guid getGuid()
    {
        return this._guid;
    } //-- com.hola.feeds.rss.Guid getGuid() 

    /**
     * Method getLinkReturns the value of field 'link'. The field
     * 'link' has the following description: The URL of the item.
     * 
     * @return the value of field 'link'.
     */
    public java.lang.String getLink()
    {
        return this._link;
    } //-- java.lang.String getLink() 

    /**
     * Method getPubDateReturns the value of field 'pubDate'. The
     * field 'pubDate' has the following description: Indicates
     * when the item was published.
     * 
     * @return the value of field 'pubDate'.
     */
    public java.lang.String getPubDate()
    {
        return this._pubDate;
    } //-- java.lang.String getPubDate() 

    /**
     * Method getSourceReturns the value of field 'source'. The
     * field 'source' has the following description: The RSS
     * channel that the item came from.
     * 
     * @return the value of field 'source'.
     */
    public com.hola.feeds.rss.Source getSource()
    {
        return this._source;
    } //-- com.hola.feeds.rss.Source getSource() 

    /**
     * Method getTitleReturns the value of field 'title'. The field
     * 'title' has the following description: The title of the
     * item.
     * 
     * @return the value of field 'title'.
     */
    public java.lang.String getTitle()
    {
        return this._title;
    } //-- java.lang.String getTitle() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method setAuthorSets the value of field 'author'. The field
     * 'author' has the following description: Email address of the
     * author of the item.
     * 
     * @param author the value of field 'author'.
     */
    public void setAuthor(java.lang.String author)
    {
        this._author = author;
    } //-- void setAuthor(java.lang.String) 

    /**
     * Method setCategorySets the value of field 'category'. The
     * field 'category' has the following description: Includes the
     * item in one or more categories. 
     * 
     * @param category the value of field 'category'.
     */
    public void setCategory(com.hola.feeds.rss.Category category)
    {
        this._category = category;
    } //-- void setCategory(com.hola.feeds.rss.Category) 

    /**
     * Method setCommentsSets the value of field 'comments'. The
     * field 'comments' has the following description: URL of a
     * page for comments relating to the item.
     * 
     * @param comments the value of field 'comments'.
     */
    public void setComments(java.lang.String comments)
    {
        this._comments = comments;
    } //-- void setComments(java.lang.String) 

    /**
     * Method setDescriptionSets the value of field 'description'.
     * The field 'description' has the following description: The
     * item synopsis.
     * 
     * @param description the value of field 'description'.
     */
    public void setDescription(java.lang.String description)
    {
        this._description = description;
    } //-- void setDescription(java.lang.String) 

    /**
     * Method setEnclosureSets the value of field 'enclosure'. The
     * field 'enclosure' has the following description: Describes a
     * media object that is attached to the item.
     * 
     * @param enclosure the value of field 'enclosure'.
     */
    public void setEnclosure(com.hola.feeds.rss.Enclosure enclosure)
    {
        this._enclosure = enclosure;
    } //-- void setEnclosure(com.hola.feeds.rss.Enclosure) 

    /**
     * Method setGuidSets the value of field 'guid'. The field
     * 'guid' has the following description: guid or permalink URL
     * for this entry
     * 
     * @param guid the value of field 'guid'.
     */
    public void setGuid(com.hola.feeds.rss.Guid guid)
    {
        this._guid = guid;
    } //-- void setGuid(com.hola.feeds.rss.Guid) 

    /**
     * Method setLinkSets the value of field 'link'. The field
     * 'link' has the following description: The URL of the item.
     * 
     * @param link the value of field 'link'.
     */
    public void setLink(java.lang.String link)
    {
        this._link = link;
    } //-- void setLink(java.lang.String) 

    /**
     * Method setPubDateSets the value of field 'pubDate'. The
     * field 'pubDate' has the following description: Indicates
     * when the item was published.
     * 
     * @param pubDate the value of field 'pubDate'.
     */
    public void setPubDate(java.lang.String pubDate)
    {
        this._pubDate = pubDate;
    } //-- void setPubDate(java.lang.String) 

    /**
     * Method setSourceSets the value of field 'source'. The field
     * 'source' has the following description: The RSS channel that
     * the item came from.
     * 
     * @param source the value of field 'source'.
     */
    public void setSource(com.hola.feeds.rss.Source source)
    {
        this._source = source;
    } //-- void setSource(com.hola.feeds.rss.Source) 

    /**
     * Method setTitleSets the value of field 'title'. The field
     * 'title' has the following description: The title of the
     * item.
     * 
     * @param title the value of field 'title'.
     */
    public void setTitle(java.lang.String title)
    {
        this._title = title;
    } //-- void setTitle(java.lang.String) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
