/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss091;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import com.hola.feeds.rss091.types.DayType;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * Class Days.
 * 
 * @version $Revision$ $Date$
 */
public abstract class Days extends com.hellomagazine.Document 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dayList
     */
    private java.util.ArrayList _dayList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Days() {
        super();
        _dayList = new ArrayList();
    } //-- com.hola.feeds.rss091.Days()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addDay
     * 
     * @param vDay
     */
    public void addDay(com.hola.feeds.rss091.types.DayType vDay)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_dayList.size() < 7)) {
            throw new IndexOutOfBoundsException();
        }
        _dayList.add(vDay);
    } //-- void addDay(com.hola.feeds.rss091.types.DayType) 

    /**
     * Method addDay
     * 
     * @param index
     * @param vDay
     */
    public void addDay(int index, com.hola.feeds.rss091.types.DayType vDay)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_dayList.size() < 7)) {
            throw new IndexOutOfBoundsException();
        }
        _dayList.add(index, vDay);
    } //-- void addDay(int, com.hola.feeds.rss091.types.DayType) 

    /**
     * Method clearDay
     */
    public void clearDay()
    {
        _dayList.clear();
    } //-- void clearDay() 

    /**
     * Method enumerateDay
     */
    public java.util.Enumeration enumerateDay()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_dayList.iterator());
    } //-- java.util.Enumeration enumerateDay() 

    /**
     * Method getDay
     * 
     * @param index
     */
    public com.hola.feeds.rss091.types.DayType getDay(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _dayList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hola.feeds.rss091.types.DayType) _dayList.get(index);
    } //-- com.hola.feeds.rss091.types.DayType getDay(int) 

    /**
     * Method getDay
     */
    public com.hola.feeds.rss091.types.DayType[] getDay()
    {
        int size = _dayList.size();
        com.hola.feeds.rss091.types.DayType[] mArray = new com.hola.feeds.rss091.types.DayType[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hola.feeds.rss091.types.DayType) _dayList.get(index);
        }
        return mArray;
    } //-- com.hola.feeds.rss091.types.DayType[] getDay() 

    /**
     * Method getDayCount
     */
    public int getDayCount()
    {
        return _dayList.size();
    } //-- int getDayCount() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method removeDay
     * 
     * @param vDay
     */
    public boolean removeDay(com.hola.feeds.rss091.types.DayType vDay)
    {
        boolean removed = _dayList.remove(vDay);
        return removed;
    } //-- boolean removeDay(com.hola.feeds.rss091.types.DayType) 

    /**
     * Method setDay
     * 
     * @param index
     * @param vDay
     */
    public void setDay(int index, com.hola.feeds.rss091.types.DayType vDay)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _dayList.size())) {
            throw new IndexOutOfBoundsException();
        }
        if (!(index < 7)) {
            throw new IndexOutOfBoundsException();
        }
        _dayList.set(index, vDay);
    } //-- void setDay(int, com.hola.feeds.rss091.types.DayType) 

    /**
     * Method setDay
     * 
     * @param dayArray
     */
    public void setDay(com.hola.feeds.rss091.types.DayType[] dayArray)
    {
        //-- copy array
        _dayList.clear();
        for (int i = 0; i < dayArray.length; i++) {
            _dayList.add(dayArray[i]);
        }
    } //-- void setDay(com.hola.feeds.rss091.types.DayType) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
