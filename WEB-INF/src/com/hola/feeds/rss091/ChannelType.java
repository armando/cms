/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss091;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * Class ChannelType.
 * 
 * @version $Revision$ $Date$
 */
public abstract class ChannelType extends com.hellomagazine.Document 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _title
     */
    private java.lang.String _title;

    /**
     * Field _link
     */
    private java.lang.String _link;

    /**
     * Field _description
     */
    private java.lang.String _description;

    /**
     * Field _language
     */
    private java.lang.String _language;

    /**
     * Field _rating
     */
    private java.lang.String _rating;

    /**
     * Field _image
     */
    private com.hola.feeds.rss091.Image _image;

    /**
     * Field _textInput
     */
    private com.hola.feeds.rss091.TextInput _textInput;

    /**
     * Field _copyright
     */
    private java.lang.String _copyright;

    /**
     * Field _docs
     */
    private java.lang.String _docs;

    /**
     * Field _managingEditor
     */
    private java.lang.String _managingEditor;

    /**
     * Field _webMaster
     */
    private java.lang.String _webMaster;

    /**
     * Field _pubDate
     */
    private java.lang.String _pubDate;

    /**
     * Field _lastBuildDate
     */
    private java.lang.String _lastBuildDate;

    /**
     * Field _skipHours
     */
    private com.hola.feeds.rss091.SkipHours _skipHours;

    /**
     * Field _skipDays
     */
    private com.hola.feeds.rss091.SkipDays _skipDays;

    /**
     * Field _itemList
     */
    private java.util.ArrayList _itemList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ChannelType() {
        super();
        _itemList = new ArrayList();
    } //-- com.hola.feeds.rss091.ChannelType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addItem
     * 
     * @param vItem
     */
    public void addItem(com.hola.feeds.rss091.Item vItem)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_itemList.size() < 15)) {
            throw new IndexOutOfBoundsException();
        }
        _itemList.add(vItem);
    } //-- void addItem(com.hola.feeds.rss091.Item) 

    /**
     * Method addItem
     * 
     * @param index
     * @param vItem
     */
    public void addItem(int index, com.hola.feeds.rss091.Item vItem)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_itemList.size() < 15)) {
            throw new IndexOutOfBoundsException();
        }
        _itemList.add(index, vItem);
    } //-- void addItem(int, com.hola.feeds.rss091.Item) 

    /**
     * Method clearItem
     */
    public void clearItem()
    {
        _itemList.clear();
    } //-- void clearItem() 

    /**
     * Method enumerateItem
     */
    public java.util.Enumeration enumerateItem()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_itemList.iterator());
    } //-- java.util.Enumeration enumerateItem() 

    /**
     * Method getCopyrightReturns the value of field 'copyright'.
     * 
     * @return the value of field 'copyright'.
     */
    public java.lang.String getCopyright()
    {
        return this._copyright;
    } //-- java.lang.String getCopyright() 

    /**
     * Method getDescriptionReturns the value of field
     * 'description'.
     * 
     * @return the value of field 'description'.
     */
    public java.lang.String getDescription()
    {
        return this._description;
    } //-- java.lang.String getDescription() 

    /**
     * Method getDocsReturns the value of field 'docs'.
     * 
     * @return the value of field 'docs'.
     */
    public java.lang.String getDocs()
    {
        return this._docs;
    } //-- java.lang.String getDocs() 

    /**
     * Method getImageReturns the value of field 'image'.
     * 
     * @return the value of field 'image'.
     */
    public com.hola.feeds.rss091.Image getImage()
    {
        return this._image;
    } //-- com.hola.feeds.rss091.Image getImage() 

    /**
     * Method getItem
     * 
     * @param index
     */
    public com.hola.feeds.rss091.Item getItem(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _itemList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hola.feeds.rss091.Item) _itemList.get(index);
    } //-- com.hola.feeds.rss091.Item getItem(int) 

    /**
     * Method getItem
     */
    public com.hola.feeds.rss091.Item[] getItem()
    {
        int size = _itemList.size();
        com.hola.feeds.rss091.Item[] mArray = new com.hola.feeds.rss091.Item[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hola.feeds.rss091.Item) _itemList.get(index);
        }
        return mArray;
    } //-- com.hola.feeds.rss091.Item[] getItem() 

    /**
     * Method getItemCount
     */
    public int getItemCount()
    {
        return _itemList.size();
    } //-- int getItemCount() 

    /**
     * Method getLanguageReturns the value of field 'language'.
     * 
     * @return the value of field 'language'.
     */
    public java.lang.String getLanguage()
    {
        return this._language;
    } //-- java.lang.String getLanguage() 

    /**
     * Method getLastBuildDateReturns the value of field
     * 'lastBuildDate'.
     * 
     * @return the value of field 'lastBuildDate'.
     */
    public java.lang.String getLastBuildDate()
    {
        return this._lastBuildDate;
    } //-- java.lang.String getLastBuildDate() 

    /**
     * Method getLinkReturns the value of field 'link'.
     * 
     * @return the value of field 'link'.
     */
    public java.lang.String getLink()
    {
        return this._link;
    } //-- java.lang.String getLink() 

    /**
     * Method getManagingEditorReturns the value of field
     * 'managingEditor'.
     * 
     * @return the value of field 'managingEditor'.
     */
    public java.lang.String getManagingEditor()
    {
        return this._managingEditor;
    } //-- java.lang.String getManagingEditor() 

    /**
     * Method getPubDateReturns the value of field 'pubDate'.
     * 
     * @return the value of field 'pubDate'.
     */
    public java.lang.String getPubDate()
    {
        return this._pubDate;
    } //-- java.lang.String getPubDate() 

    /**
     * Method getRatingReturns the value of field 'rating'.
     * 
     * @return the value of field 'rating'.
     */
    public java.lang.String getRating()
    {
        return this._rating;
    } //-- java.lang.String getRating() 

    /**
     * Method getSkipDaysReturns the value of field 'skipDays'.
     * 
     * @return the value of field 'skipDays'.
     */
    public com.hola.feeds.rss091.SkipDays getSkipDays()
    {
        return this._skipDays;
    } //-- com.hola.feeds.rss091.SkipDays getSkipDays() 

    /**
     * Method getSkipHoursReturns the value of field 'skipHours'.
     * 
     * @return the value of field 'skipHours'.
     */
    public com.hola.feeds.rss091.SkipHours getSkipHours()
    {
        return this._skipHours;
    } //-- com.hola.feeds.rss091.SkipHours getSkipHours() 

    /**
     * Method getTextInputReturns the value of field 'textInput'.
     * 
     * @return the value of field 'textInput'.
     */
    public com.hola.feeds.rss091.TextInput getTextInput()
    {
        return this._textInput;
    } //-- com.hola.feeds.rss091.TextInput getTextInput() 

    /**
     * Method getTitleReturns the value of field 'title'.
     * 
     * @return the value of field 'title'.
     */
    public java.lang.String getTitle()
    {
        return this._title;
    } //-- java.lang.String getTitle() 

    /**
     * Method getWebMasterReturns the value of field 'webMaster'.
     * 
     * @return the value of field 'webMaster'.
     */
    public java.lang.String getWebMaster()
    {
        return this._webMaster;
    } //-- java.lang.String getWebMaster() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method removeItem
     * 
     * @param vItem
     */
    public boolean removeItem(com.hola.feeds.rss091.Item vItem)
    {
        boolean removed = _itemList.remove(vItem);
        return removed;
    } //-- boolean removeItem(com.hola.feeds.rss091.Item) 

    /**
     * Method setCopyrightSets the value of field 'copyright'.
     * 
     * @param copyright the value of field 'copyright'.
     */
    public void setCopyright(java.lang.String copyright)
    {
        this._copyright = copyright;
    } //-- void setCopyright(java.lang.String) 

    /**
     * Method setDescriptionSets the value of field 'description'.
     * 
     * @param description the value of field 'description'.
     */
    public void setDescription(java.lang.String description)
    {
        this._description = description;
    } //-- void setDescription(java.lang.String) 

    /**
     * Method setDocsSets the value of field 'docs'.
     * 
     * @param docs the value of field 'docs'.
     */
    public void setDocs(java.lang.String docs)
    {
        this._docs = docs;
    } //-- void setDocs(java.lang.String) 

    /**
     * Method setImageSets the value of field 'image'.
     * 
     * @param image the value of field 'image'.
     */
    public void setImage(com.hola.feeds.rss091.Image image)
    {
        this._image = image;
    } //-- void setImage(com.hola.feeds.rss091.Image) 

    /**
     * Method setItem
     * 
     * @param index
     * @param vItem
     */
    public void setItem(int index, com.hola.feeds.rss091.Item vItem)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _itemList.size())) {
            throw new IndexOutOfBoundsException();
        }
        if (!(index < 15)) {
            throw new IndexOutOfBoundsException();
        }
        _itemList.set(index, vItem);
    } //-- void setItem(int, com.hola.feeds.rss091.Item) 

    /**
     * Method setItem
     * 
     * @param itemArray
     */
    public void setItem(com.hola.feeds.rss091.Item[] itemArray)
    {
        //-- copy array
        _itemList.clear();
        for (int i = 0; i < itemArray.length; i++) {
            _itemList.add(itemArray[i]);
        }
    } //-- void setItem(com.hola.feeds.rss091.Item) 

    /**
     * Method setLanguageSets the value of field 'language'.
     * 
     * @param language the value of field 'language'.
     */
    public void setLanguage(java.lang.String language)
    {
        this._language = language;
    } //-- void setLanguage(java.lang.String) 

    /**
     * Method setLastBuildDateSets the value of field
     * 'lastBuildDate'.
     * 
     * @param lastBuildDate the value of field 'lastBuildDate'.
     */
    public void setLastBuildDate(java.lang.String lastBuildDate)
    {
        this._lastBuildDate = lastBuildDate;
    } //-- void setLastBuildDate(java.lang.String) 

    /**
     * Method setLinkSets the value of field 'link'.
     * 
     * @param link the value of field 'link'.
     */
    public void setLink(java.lang.String link)
    {
        this._link = link;
    } //-- void setLink(java.lang.String) 

    /**
     * Method setManagingEditorSets the value of field
     * 'managingEditor'.
     * 
     * @param managingEditor the value of field 'managingEditor'.
     */
    public void setManagingEditor(java.lang.String managingEditor)
    {
        this._managingEditor = managingEditor;
    } //-- void setManagingEditor(java.lang.String) 

    /**
     * Method setPubDateSets the value of field 'pubDate'.
     * 
     * @param pubDate the value of field 'pubDate'.
     */
    public void setPubDate(java.lang.String pubDate)
    {
        this._pubDate = pubDate;
    } //-- void setPubDate(java.lang.String) 

    /**
     * Method setRatingSets the value of field 'rating'.
     * 
     * @param rating the value of field 'rating'.
     */
    public void setRating(java.lang.String rating)
    {
        this._rating = rating;
    } //-- void setRating(java.lang.String) 

    /**
     * Method setSkipDaysSets the value of field 'skipDays'.
     * 
     * @param skipDays the value of field 'skipDays'.
     */
    public void setSkipDays(com.hola.feeds.rss091.SkipDays skipDays)
    {
        this._skipDays = skipDays;
    } //-- void setSkipDays(com.hola.feeds.rss091.SkipDays) 

    /**
     * Method setSkipHoursSets the value of field 'skipHours'.
     * 
     * @param skipHours the value of field 'skipHours'.
     */
    public void setSkipHours(com.hola.feeds.rss091.SkipHours skipHours)
    {
        this._skipHours = skipHours;
    } //-- void setSkipHours(com.hola.feeds.rss091.SkipHours) 

    /**
     * Method setTextInputSets the value of field 'textInput'.
     * 
     * @param textInput the value of field 'textInput'.
     */
    public void setTextInput(com.hola.feeds.rss091.TextInput textInput)
    {
        this._textInput = textInput;
    } //-- void setTextInput(com.hola.feeds.rss091.TextInput) 

    /**
     * Method setTitleSets the value of field 'title'.
     * 
     * @param title the value of field 'title'.
     */
    public void setTitle(java.lang.String title)
    {
        this._title = title;
    } //-- void setTitle(java.lang.String) 

    /**
     * Method setWebMasterSets the value of field 'webMaster'.
     * 
     * @param webMaster the value of field 'webMaster'.
     */
    public void setWebMaster(java.lang.String webMaster)
    {
        this._webMaster = webMaster;
    } //-- void setWebMaster(java.lang.String) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
