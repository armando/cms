/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss091;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * Class RichSiteSummary.
 * 
 * @version $Revision$ $Date$
 */
public abstract class RichSiteSummary extends com.hellomagazine.Document 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _version
     */
    private java.lang.String _version;

    /**
     * Field _channel
     */
    private com.hola.feeds.rss091.Channel _channel;


      //----------------/
     //- Constructors -/
    //----------------/

    public RichSiteSummary() {
        super();
    } //-- com.hola.feeds.rss091.RichSiteSummary()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method getChannelReturns the value of field 'channel'.
     * 
     * @return the value of field 'channel'.
     */
    public com.hola.feeds.rss091.Channel getChannel()
    {
        return this._channel;
    } //-- com.hola.feeds.rss091.Channel getChannel() 

    /**
     * Method getVersionReturns the value of field 'version'.
     * 
     * @return the value of field 'version'.
     */
    public java.lang.String getVersion()
    {
        return this._version;
    } //-- java.lang.String getVersion() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method setChannelSets the value of field 'channel'.
     * 
     * @param channel the value of field 'channel'.
     */
    public void setChannel(com.hola.feeds.rss091.Channel channel)
    {
        this._channel = channel;
    } //-- void setChannel(com.hola.feeds.rss091.Channel) 

    /**
     * Method setVersionSets the value of field 'version'.
     * 
     * @param version the value of field 'version'.
     */
    public void setVersion(java.lang.String version)
    {
        this._version = version;
    } //-- void setVersion(java.lang.String) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
