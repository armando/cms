/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss091;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * Class Hours.
 * 
 * @version $Revision$ $Date$
 */
public abstract class Hours extends com.hellomagazine.Document 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _hourList
     */
    private java.util.ArrayList _hourList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Hours() {
        super();
        _hourList = new ArrayList();
    } //-- com.hola.feeds.rss091.Hours()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addHour
     * 
     * @param vHour
     */
    public void addHour(int vHour)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_hourList.size() < 24)) {
            throw new IndexOutOfBoundsException();
        }
        _hourList.add(new Integer(vHour));
    } //-- void addHour(int) 

    /**
     * Method addHour
     * 
     * @param index
     * @param vHour
     */
    public void addHour(int index, int vHour)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_hourList.size() < 24)) {
            throw new IndexOutOfBoundsException();
        }
        _hourList.add(index, new Integer(vHour));
    } //-- void addHour(int, int) 

    /**
     * Method clearHour
     */
    public void clearHour()
    {
        _hourList.clear();
    } //-- void clearHour() 

    /**
     * Method enumerateHour
     */
    public java.util.Enumeration enumerateHour()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_hourList.iterator());
    } //-- java.util.Enumeration enumerateHour() 

    /**
     * Method getHour
     * 
     * @param index
     */
    public int getHour(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _hourList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return ((Integer)_hourList.get(index)).intValue();
    } //-- int getHour(int) 

    /**
     * Method getHour
     */
    public int[] getHour()
    {
        int size = _hourList.size();
        int[] mArray = new int[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = ((Integer)_hourList.get(index)).intValue();
        }
        return mArray;
    } //-- int[] getHour() 

    /**
     * Method getHourCount
     */
    public int getHourCount()
    {
        return _hourList.size();
    } //-- int getHourCount() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method removeHour
     * 
     * @param vHour
     */
    public boolean removeHour(int vHour)
    {
        boolean removed = _hourList.remove(new Integer(vHour));
        return removed;
    } //-- boolean removeHour(int) 

    /**
     * Method setHour
     * 
     * @param index
     * @param vHour
     */
    public void setHour(int index, int vHour)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _hourList.size())) {
            throw new IndexOutOfBoundsException();
        }
        if (!(index < 24)) {
            throw new IndexOutOfBoundsException();
        }
        _hourList.set(index, new Integer(vHour));
    } //-- void setHour(int, int) 

    /**
     * Method setHour
     * 
     * @param hourArray
     */
    public void setHour(int[] hourArray)
    {
        //-- copy array
        _hourList.clear();
        for (int i = 0; i < hourArray.length; i++) {
            _hourList.add(new Integer(hourArray[i]));
        }
    } //-- void setHour(int) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
