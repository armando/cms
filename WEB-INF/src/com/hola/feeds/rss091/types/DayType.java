/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.rss091.types;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class DayType.
 * 
 * @version $Revision$ $Date$
 */
public class DayType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * The Monday type
     */
    public static final int MONDAY_TYPE = 0;

    /**
     * The instance of the Monday type
     */
    public static final DayType MONDAY = new DayType(MONDAY_TYPE, "Monday");

    /**
     * The Tuesday type
     */
    public static final int TUESDAY_TYPE = 1;

    /**
     * The instance of the Tuesday type
     */
    public static final DayType TUESDAY = new DayType(TUESDAY_TYPE, "Tuesday");

    /**
     * The Wednesday type
     */
    public static final int WEDNESDAY_TYPE = 2;

    /**
     * The instance of the Wednesday type
     */
    public static final DayType WEDNESDAY = new DayType(WEDNESDAY_TYPE, "Wednesday");

    /**
     * The Thursday type
     */
    public static final int THURSDAY_TYPE = 3;

    /**
     * The instance of the Thursday type
     */
    public static final DayType THURSDAY = new DayType(THURSDAY_TYPE, "Thursday");

    /**
     * The Friday type
     */
    public static final int FRIDAY_TYPE = 4;

    /**
     * The instance of the Friday type
     */
    public static final DayType FRIDAY = new DayType(FRIDAY_TYPE, "Friday");

    /**
     * The Saturday type
     */
    public static final int SATURDAY_TYPE = 5;

    /**
     * The instance of the Saturday type
     */
    public static final DayType SATURDAY = new DayType(SATURDAY_TYPE, "Saturday");

    /**
     * The Sunday type
     */
    public static final int SUNDAY_TYPE = 6;

    /**
     * The instance of the Sunday type
     */
    public static final DayType SUNDAY = new DayType(SUNDAY_TYPE, "Sunday");

    /**
     * Field _memberTable
     */
    private static java.util.Hashtable _memberTable = init();

    /**
     * Field type
     */
    private int type = -1;

    /**
     * Field stringValue
     */
    private java.lang.String stringValue = null;


      //----------------/
     //- Constructors -/
    //----------------/

    private DayType(int type, java.lang.String value) {
        super();
        this.type = type;
        this.stringValue = value;
    } //-- com.hola.feeds.rss091.types.DayType(int, java.lang.String)


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method enumerateReturns an enumeration of all possible
     * instances of DayType
     */
    public static java.util.Enumeration enumerate()
    {
        return _memberTable.elements();
    } //-- java.util.Enumeration enumerate() 

    /**
     * Method getTypeReturns the type of this DayType
     */
    public int getType()
    {
        return this.type;
    } //-- int getType() 

    /**
     * Method init
     */
    private static java.util.Hashtable init()
    {
        Hashtable members = new Hashtable();
        members.put("Monday", MONDAY);
        members.put("Tuesday", TUESDAY);
        members.put("Wednesday", WEDNESDAY);
        members.put("Thursday", THURSDAY);
        members.put("Friday", FRIDAY);
        members.put("Saturday", SATURDAY);
        members.put("Sunday", SUNDAY);
        return members;
    } //-- java.util.Hashtable init() 

    /**
     * Method toStringReturns the String representation of this
     * DayType
     */
    public java.lang.String toString()
    {
        return this.stringValue;
    } //-- java.lang.String toString() 

    /**
     * Method valueOfReturns a new DayType based on the given
     * String value.
     * 
     * @param string
     */
    public static com.hola.feeds.rss091.types.DayType valueOf(java.lang.String string)
    {
        java.lang.Object obj = null;
        if (string != null) obj = _memberTable.get(string);
        if (obj == null) {
            String err = "'" + string + "' is not a valid DayType";
            throw new IllegalArgumentException(err);
        }
        return (DayType) obj;
    } //-- com.hola.feeds.rss091.types.DayType valueOf(java.lang.String) 

}
