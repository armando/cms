/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.atom;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class FeedType.
 * 
 * @version $Revision$ $Date$
 */
public class FeedType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _version
     */
    private java.lang.String _version;

    /**
     * Field _lang
     */
    private java.lang.Object _lang;

    /**
     * Field _title
     */
    private java.lang.String _title;

    /**
     * Field _linkList
     */
    private java.util.ArrayList _linkList;

    /**
     * Field _modified
     */
    private java.lang.String _modified;

    /**
     * Field _author
     */
    private com.hola.feeds.atom.Author _author;

    /**
     * Field _contributorList
     */
    private java.util.ArrayList _contributorList;

    /**
     * Field _tagline
     */
    private com.hola.feeds.atom.Tagline _tagline;

    /**
     * Field _id
     */
    private java.lang.String _id;

    /**
     * Field _generator
     */
    private com.hola.feeds.atom.Generator _generator;

    /**
     * Field _copyright
     */
    private java.lang.String _copyright;

    /**
     * Field _info
     */
    private com.hola.feeds.atom.Info _info;

    /**
     * Field _entryList
     */
    private java.util.ArrayList _entryList;

    /**
     * Field _anyObject
     */
    private java.util.ArrayList _anyObject;


      //----------------/
     //- Constructors -/
    //----------------/

    public FeedType() {
        super();
        _linkList = new ArrayList();
        _contributorList = new ArrayList();
        _entryList = new ArrayList();
        _anyObject = new ArrayList();
    } //-- com.hola.feeds.atom.FeedType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addAnyObject
     * 
     * @param vAnyObject
     */
    public void addAnyObject(java.lang.Object vAnyObject)
        throws java.lang.IndexOutOfBoundsException
    {
        _anyObject.add(vAnyObject);
    } //-- void addAnyObject(java.lang.Object) 

    /**
     * Method addAnyObject
     * 
     * @param index
     * @param vAnyObject
     */
    public void addAnyObject(int index, java.lang.Object vAnyObject)
        throws java.lang.IndexOutOfBoundsException
    {
        _anyObject.add(index, vAnyObject);
    } //-- void addAnyObject(int, java.lang.Object) 

    /**
     * Method addContributor
     * 
     * @param vContributor
     */
    public void addContributor(java.lang.Object vContributor)
        throws java.lang.IndexOutOfBoundsException
    {
        _contributorList.add(vContributor);
    } //-- void addContributor(java.lang.Object) 

    /**
     * Method addContributor
     * 
     * @param index
     * @param vContributor
     */
    public void addContributor(int index, java.lang.Object vContributor)
        throws java.lang.IndexOutOfBoundsException
    {
        _contributorList.add(index, vContributor);
    } //-- void addContributor(int, java.lang.Object) 

    /**
     * Method addEntry
     * 
     * @param vEntry
     */
    public void addEntry(com.hola.feeds.atom.Entry vEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        _entryList.add(vEntry);
    } //-- void addEntry(com.hola.feeds.atom.Entry) 

    /**
     * Method addEntry
     * 
     * @param index
     * @param vEntry
     */
    public void addEntry(int index, com.hola.feeds.atom.Entry vEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        _entryList.add(index, vEntry);
    } //-- void addEntry(int, com.hola.feeds.atom.Entry) 

    /**
     * Method addLink
     * 
     * @param vLink
     */
    public void addLink(com.hola.feeds.atom.Link vLink)
        throws java.lang.IndexOutOfBoundsException
    {
        _linkList.add(vLink);
    } //-- void addLink(com.hola.feeds.atom.Link) 

    /**
     * Method addLink
     * 
     * @param index
     * @param vLink
     */
    public void addLink(int index, com.hola.feeds.atom.Link vLink)
        throws java.lang.IndexOutOfBoundsException
    {
        _linkList.add(index, vLink);
    } //-- void addLink(int, com.hola.feeds.atom.Link) 

    /**
     * Method clearAnyObject
     */
    public void clearAnyObject()
    {
        _anyObject.clear();
    } //-- void clearAnyObject() 

    /**
     * Method clearContributor
     */
    public void clearContributor()
    {
        _contributorList.clear();
    } //-- void clearContributor() 

    /**
     * Method clearEntry
     */
    public void clearEntry()
    {
        _entryList.clear();
    } //-- void clearEntry() 

    /**
     * Method clearLink
     */
    public void clearLink()
    {
        _linkList.clear();
    } //-- void clearLink() 

    /**
     * Method enumerateAnyObject
     */
    public java.util.Enumeration enumerateAnyObject()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_anyObject.iterator());
    } //-- java.util.Enumeration enumerateAnyObject() 

    /**
     * Method enumerateContributor
     */
    public java.util.Enumeration enumerateContributor()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_contributorList.iterator());
    } //-- java.util.Enumeration enumerateContributor() 

    /**
     * Method enumerateEntry
     */
    public java.util.Enumeration enumerateEntry()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_entryList.iterator());
    } //-- java.util.Enumeration enumerateEntry() 

    /**
     * Method enumerateLink
     */
    public java.util.Enumeration enumerateLink()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_linkList.iterator());
    } //-- java.util.Enumeration enumerateLink() 

    /**
     * Method getAnyObject
     * 
     * @param index
     */
    public java.lang.Object getAnyObject(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _anyObject.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (java.lang.Object) _anyObject.get(index);
    } //-- java.lang.Object getAnyObject(int) 

    /**
     * Method getAnyObject
     */
    public java.lang.Object[] getAnyObject()
    {
        int size = _anyObject.size();
        java.lang.Object[] mArray = new java.lang.Object[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (java.lang.Object) _anyObject.get(index);
        }
        return mArray;
    } //-- java.lang.Object[] getAnyObject() 

    /**
     * Method getAnyObjectCount
     */
    public int getAnyObjectCount()
    {
        return _anyObject.size();
    } //-- int getAnyObjectCount() 

    /**
     * Returns the value of field 'author'.
     * 
     * @return the value of field 'author'.
     */
    public com.hola.feeds.atom.Author getAuthor()
    {
        return this._author;
    } //-- com.hola.feeds.atom.Author getAuthor() 

    /**
     * Method getContributor
     * 
     * @param index
     */
    public java.lang.Object getContributor(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _contributorList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (java.lang.Object) _contributorList.get(index);
    } //-- java.lang.Object getContributor(int) 

    /**
     * Method getContributor
     */
    public java.lang.Object[] getContributor()
    {
        int size = _contributorList.size();
        java.lang.Object[] mArray = new java.lang.Object[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (java.lang.Object) _contributorList.get(index);
        }
        return mArray;
    } //-- java.lang.Object[] getContributor() 

    /**
     * Method getContributorCount
     */
    public int getContributorCount()
    {
        return _contributorList.size();
    } //-- int getContributorCount() 

    /**
     * Returns the value of field 'copyright'.
     * 
     * @return the value of field 'copyright'.
     */
    public java.lang.String getCopyright()
    {
        return this._copyright;
    } //-- java.lang.String getCopyright() 

    /**
     * Method getEntry
     * 
     * @param index
     */
    public com.hola.feeds.atom.Entry getEntry(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _entryList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hola.feeds.atom.Entry) _entryList.get(index);
    } //-- com.hola.feeds.atom.Entry getEntry(int) 

    /**
     * Method getEntry
     */
    public com.hola.feeds.atom.Entry[] getEntry()
    {
        int size = _entryList.size();
        com.hola.feeds.atom.Entry[] mArray = new com.hola.feeds.atom.Entry[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hola.feeds.atom.Entry) _entryList.get(index);
        }
        return mArray;
    } //-- com.hola.feeds.atom.Entry[] getEntry() 

    /**
     * Method getEntryCount
     */
    public int getEntryCount()
    {
        return _entryList.size();
    } //-- int getEntryCount() 

    /**
     * Returns the value of field 'generator'.
     * 
     * @return the value of field 'generator'.
     */
    public com.hola.feeds.atom.Generator getGenerator()
    {
        return this._generator;
    } //-- com.hola.feeds.atom.Generator getGenerator() 

    /**
     * Returns the value of field 'id'.
     * 
     * @return the value of field 'id'.
     */
    public java.lang.String getId()
    {
        return this._id;
    } //-- java.lang.String getId() 

    /**
     * Returns the value of field 'info'.
     * 
     * @return the value of field 'info'.
     */
    public com.hola.feeds.atom.Info getInfo()
    {
        return this._info;
    } //-- com.hola.feeds.atom.Info getInfo() 

    /**
     * Returns the value of field 'lang'.
     * 
     * @return the value of field 'lang'.
     */
    public java.lang.Object getLang()
    {
        return this._lang;
    } //-- java.lang.Object getLang() 

    /**
     * Method getLink
     * 
     * @param index
     */
    public com.hola.feeds.atom.Link getLink(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _linkList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hola.feeds.atom.Link) _linkList.get(index);
    } //-- com.hola.feeds.atom.Link getLink(int) 

    /**
     * Method getLink
     */
    public com.hola.feeds.atom.Link[] getLink()
    {
        int size = _linkList.size();
        com.hola.feeds.atom.Link[] mArray = new com.hola.feeds.atom.Link[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hola.feeds.atom.Link) _linkList.get(index);
        }
        return mArray;
    } //-- com.hola.feeds.atom.Link[] getLink() 

    /**
     * Method getLinkCount
     */
    public int getLinkCount()
    {
        return _linkList.size();
    } //-- int getLinkCount() 

    /**
     * Returns the value of field 'modified'.
     * 
     * @return the value of field 'modified'.
     */
    public java.lang.String getModified()
    {
        return this._modified;
    } //-- java.lang.String getModified() 

    /**
     * Returns the value of field 'tagline'.
     * 
     * @return the value of field 'tagline'.
     */
    public com.hola.feeds.atom.Tagline getTagline()
    {
        return this._tagline;
    } //-- com.hola.feeds.atom.Tagline getTagline() 

    /**
     * Returns the value of field 'title'.
     * 
     * @return the value of field 'title'.
     */
    public java.lang.String getTitle()
    {
        return this._title;
    } //-- java.lang.String getTitle() 

    /**
     * Returns the value of field 'version'.
     * 
     * @return the value of field 'version'.
     */
    public java.lang.String getVersion()
    {
        return this._version;
    } //-- java.lang.String getVersion() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAnyObject
     * 
     * @param vAnyObject
     */
    public boolean removeAnyObject(java.lang.Object vAnyObject)
    {
        boolean removed = _anyObject.remove(vAnyObject);
        return removed;
    } //-- boolean removeAnyObject(java.lang.Object) 

    /**
     * Method removeContributor
     * 
     * @param vContributor
     */
    public boolean removeContributor(java.lang.Object vContributor)
    {
        boolean removed = _contributorList.remove(vContributor);
        return removed;
    } //-- boolean removeContributor(java.lang.Object) 

    /**
     * Method removeEntry
     * 
     * @param vEntry
     */
    public boolean removeEntry(com.hola.feeds.atom.Entry vEntry)
    {
        boolean removed = _entryList.remove(vEntry);
        return removed;
    } //-- boolean removeEntry(com.hola.feeds.atom.Entry) 

    /**
     * Method removeLink
     * 
     * @param vLink
     */
    public boolean removeLink(com.hola.feeds.atom.Link vLink)
    {
        boolean removed = _linkList.remove(vLink);
        return removed;
    } //-- boolean removeLink(com.hola.feeds.atom.Link) 

    /**
     * Method setAnyObject
     * 
     * @param index
     * @param vAnyObject
     */
    public void setAnyObject(int index, java.lang.Object vAnyObject)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _anyObject.size())) {
            throw new IndexOutOfBoundsException();
        }
        _anyObject.set(index, vAnyObject);
    } //-- void setAnyObject(int, java.lang.Object) 

    /**
     * Method setAnyObject
     * 
     * @param anyObjectArray
     */
    public void setAnyObject(java.lang.Object[] anyObjectArray)
    {
        //-- copy array
        _anyObject.clear();
        for (int i = 0; i < anyObjectArray.length; i++) {
            _anyObject.add(anyObjectArray[i]);
        }
    } //-- void setAnyObject(java.lang.Object) 

    /**
     * Sets the value of field 'author'.
     * 
     * @param author the value of field 'author'.
     */
    public void setAuthor(com.hola.feeds.atom.Author author)
    {
        this._author = author;
    } //-- void setAuthor(com.hola.feeds.atom.Author) 

    /**
     * Method setContributor
     * 
     * @param index
     * @param vContributor
     */
    public void setContributor(int index, java.lang.Object vContributor)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _contributorList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _contributorList.set(index, vContributor);
    } //-- void setContributor(int, java.lang.Object) 

    /**
     * Method setContributor
     * 
     * @param contributorArray
     */
    public void setContributor(java.lang.Object[] contributorArray)
    {
        //-- copy array
        _contributorList.clear();
        for (int i = 0; i < contributorArray.length; i++) {
            _contributorList.add(contributorArray[i]);
        }
    } //-- void setContributor(java.lang.Object) 

    /**
     * Sets the value of field 'copyright'.
     * 
     * @param copyright the value of field 'copyright'.
     */
    public void setCopyright(java.lang.String copyright)
    {
        this._copyright = copyright;
    } //-- void setCopyright(java.lang.String) 

    /**
     * Method setEntry
     * 
     * @param index
     * @param vEntry
     */
    public void setEntry(int index, com.hola.feeds.atom.Entry vEntry)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _entryList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _entryList.set(index, vEntry);
    } //-- void setEntry(int, com.hola.feeds.atom.Entry) 

    /**
     * Method setEntry
     * 
     * @param entryArray
     */
    public void setEntry(com.hola.feeds.atom.Entry[] entryArray)
    {
        //-- copy array
        _entryList.clear();
        for (int i = 0; i < entryArray.length; i++) {
            _entryList.add(entryArray[i]);
        }
    } //-- void setEntry(com.hola.feeds.atom.Entry) 

    /**
     * Sets the value of field 'generator'.
     * 
     * @param generator the value of field 'generator'.
     */
    public void setGenerator(com.hola.feeds.atom.Generator generator)
    {
        this._generator = generator;
    } //-- void setGenerator(com.hola.feeds.atom.Generator) 

    /**
     * Sets the value of field 'id'.
     * 
     * @param id the value of field 'id'.
     */
    public void setId(java.lang.String id)
    {
        this._id = id;
    } //-- void setId(java.lang.String) 

    /**
     * Sets the value of field 'info'.
     * 
     * @param info the value of field 'info'.
     */
    public void setInfo(com.hola.feeds.atom.Info info)
    {
        this._info = info;
    } //-- void setInfo(com.hola.feeds.atom.Info) 

    /**
     * Sets the value of field 'lang'.
     * 
     * @param lang the value of field 'lang'.
     */
    public void setLang(java.lang.Object lang)
    {
        this._lang = lang;
    } //-- void setLang(java.lang.Object) 

    /**
     * Method setLink
     * 
     * @param index
     * @param vLink
     */
    public void setLink(int index, com.hola.feeds.atom.Link vLink)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _linkList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _linkList.set(index, vLink);
    } //-- void setLink(int, com.hola.feeds.atom.Link) 

    /**
     * Method setLink
     * 
     * @param linkArray
     */
    public void setLink(com.hola.feeds.atom.Link[] linkArray)
    {
        //-- copy array
        _linkList.clear();
        for (int i = 0; i < linkArray.length; i++) {
            _linkList.add(linkArray[i]);
        }
    } //-- void setLink(com.hola.feeds.atom.Link) 

    /**
     * Sets the value of field 'modified'.
     * 
     * @param modified the value of field 'modified'.
     */
    public void setModified(java.lang.String modified)
    {
        this._modified = modified;
    } //-- void setModified(java.lang.String) 

    /**
     * Sets the value of field 'tagline'.
     * 
     * @param tagline the value of field 'tagline'.
     */
    public void setTagline(com.hola.feeds.atom.Tagline tagline)
    {
        this._tagline = tagline;
    } //-- void setTagline(com.hola.feeds.atom.Tagline) 

    /**
     * Sets the value of field 'title'.
     * 
     * @param title the value of field 'title'.
     */
    public void setTitle(java.lang.String title)
    {
        this._title = title;
    } //-- void setTitle(java.lang.String) 

    /**
     * Sets the value of field 'version'.
     * 
     * @param version the value of field 'version'.
     */
    public void setVersion(java.lang.String version)
    {
        this._version = version;
    } //-- void setVersion(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hola.feeds.atom.FeedType) Unmarshaller.unmarshal(com.hola.feeds.atom.FeedType.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
