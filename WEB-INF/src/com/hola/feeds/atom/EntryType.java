/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.atom;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class EntryType.
 * 
 * @version $Revision$ $Date$
 */
public class EntryType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _lang
     */
    private java.lang.Object _lang;

    /**
     * Field _title
     */
    private java.lang.String _title;

    /**
     * Field _linkList
     */
    private java.util.ArrayList _linkList;

    /**
     * Field _author
     */
    private com.hola.feeds.atom.Author _author;

    /**
     * Field _contributorList
     */
    private java.util.ArrayList _contributorList;

    /**
     * Field _id
     */
    private java.lang.String _id;

    /**
     * Field _issued
     */
    private java.lang.String _issued;

    /**
     * Field _modified
     */
    private java.lang.String _modified;

    /**
     * Field _created
     */
    private java.lang.String _created;

    /**
     * Field _summary
     */
    private com.hola.feeds.atom.Summary _summary;

    /**
     * Field _contentList
     */
    private java.util.ArrayList _contentList;

    /**
     * Field _anyObject
     */
    private java.util.ArrayList _anyObject;


      //----------------/
     //- Constructors -/
    //----------------/

    public EntryType() {
        super();
        _linkList = new ArrayList();
        _contributorList = new ArrayList();
        _contentList = new ArrayList();
        _anyObject = new ArrayList();
    } //-- com.hola.feeds.atom.EntryType()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addAnyObject
     * 
     * @param vAnyObject
     */
    public void addAnyObject(java.lang.Object vAnyObject)
        throws java.lang.IndexOutOfBoundsException
    {
        _anyObject.add(vAnyObject);
    } //-- void addAnyObject(java.lang.Object) 

    /**
     * Method addAnyObject
     * 
     * @param index
     * @param vAnyObject
     */
    public void addAnyObject(int index, java.lang.Object vAnyObject)
        throws java.lang.IndexOutOfBoundsException
    {
        _anyObject.add(index, vAnyObject);
    } //-- void addAnyObject(int, java.lang.Object) 

    /**
     * Method addContent
     * 
     * @param vContent
     */
    public void addContent(com.hola.feeds.atom.Content vContent)
        throws java.lang.IndexOutOfBoundsException
    {
        _contentList.add(vContent);
    } //-- void addContent(com.hola.feeds.atom.Content) 

    /**
     * Method addContent
     * 
     * @param index
     * @param vContent
     */
    public void addContent(int index, com.hola.feeds.atom.Content vContent)
        throws java.lang.IndexOutOfBoundsException
    {
        _contentList.add(index, vContent);
    } //-- void addContent(int, com.hola.feeds.atom.Content) 

    /**
     * Method addContributor
     * 
     * @param vContributor
     */
    public void addContributor(com.hola.feeds.atom.Contributor vContributor)
        throws java.lang.IndexOutOfBoundsException
    {
        _contributorList.add(vContributor);
    } //-- void addContributor(com.hola.feeds.atom.Contributor) 

    /**
     * Method addContributor
     * 
     * @param index
     * @param vContributor
     */
    public void addContributor(int index, com.hola.feeds.atom.Contributor vContributor)
        throws java.lang.IndexOutOfBoundsException
    {
        _contributorList.add(index, vContributor);
    } //-- void addContributor(int, com.hola.feeds.atom.Contributor) 

    /**
     * Method addLink
     * 
     * @param vLink
     */
    public void addLink(com.hola.feeds.atom.Link vLink)
        throws java.lang.IndexOutOfBoundsException
    {
        _linkList.add(vLink);
    } //-- void addLink(com.hola.feeds.atom.Link) 

    /**
     * Method addLink
     * 
     * @param index
     * @param vLink
     */
    public void addLink(int index, com.hola.feeds.atom.Link vLink)
        throws java.lang.IndexOutOfBoundsException
    {
        _linkList.add(index, vLink);
    } //-- void addLink(int, com.hola.feeds.atom.Link) 

    /**
     * Method clearAnyObject
     */
    public void clearAnyObject()
    {
        _anyObject.clear();
    } //-- void clearAnyObject() 

    /**
     * Method clearContent
     */
    public void clearContent()
    {
        _contentList.clear();
    } //-- void clearContent() 

    /**
     * Method clearContributor
     */
    public void clearContributor()
    {
        _contributorList.clear();
    } //-- void clearContributor() 

    /**
     * Method clearLink
     */
    public void clearLink()
    {
        _linkList.clear();
    } //-- void clearLink() 

    /**
     * Method enumerateAnyObject
     */
    public java.util.Enumeration enumerateAnyObject()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_anyObject.iterator());
    } //-- java.util.Enumeration enumerateAnyObject() 

    /**
     * Method enumerateContent
     */
    public java.util.Enumeration enumerateContent()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_contentList.iterator());
    } //-- java.util.Enumeration enumerateContent() 

    /**
     * Method enumerateContributor
     */
    public java.util.Enumeration enumerateContributor()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_contributorList.iterator());
    } //-- java.util.Enumeration enumerateContributor() 

    /**
     * Method enumerateLink
     */
    public java.util.Enumeration enumerateLink()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_linkList.iterator());
    } //-- java.util.Enumeration enumerateLink() 

    /**
     * Method getAnyObject
     * 
     * @param index
     */
    public java.lang.Object getAnyObject(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _anyObject.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (java.lang.Object) _anyObject.get(index);
    } //-- java.lang.Object getAnyObject(int) 

    /**
     * Method getAnyObject
     */
    public java.lang.Object[] getAnyObject()
    {
        int size = _anyObject.size();
        java.lang.Object[] mArray = new java.lang.Object[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (java.lang.Object) _anyObject.get(index);
        }
        return mArray;
    } //-- java.lang.Object[] getAnyObject() 

    /**
     * Method getAnyObjectCount
     */
    public int getAnyObjectCount()
    {
        return _anyObject.size();
    } //-- int getAnyObjectCount() 

    /**
     * Returns the value of field 'author'.
     * 
     * @return the value of field 'author'.
     */
    public com.hola.feeds.atom.Author getAuthor()
    {
        return this._author;
    } //-- com.hola.feeds.atom.Author getAuthor() 

    /**
     * Method getContent
     * 
     * @param index
     */
    public com.hola.feeds.atom.Content getContent(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _contentList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hola.feeds.atom.Content) _contentList.get(index);
    } //-- com.hola.feeds.atom.Content getContent(int) 

    /**
     * Method getContent
     */
    public com.hola.feeds.atom.Content[] getContent()
    {
        int size = _contentList.size();
        com.hola.feeds.atom.Content[] mArray = new com.hola.feeds.atom.Content[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hola.feeds.atom.Content) _contentList.get(index);
        }
        return mArray;
    } //-- com.hola.feeds.atom.Content[] getContent() 

    /**
     * Method getContentCount
     */
    public int getContentCount()
    {
        return _contentList.size();
    } //-- int getContentCount() 

    /**
     * Method getContributor
     * 
     * @param index
     */
    public com.hola.feeds.atom.Contributor getContributor(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _contributorList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hola.feeds.atom.Contributor) _contributorList.get(index);
    } //-- com.hola.feeds.atom.Contributor getContributor(int) 

    /**
     * Method getContributor
     */
    public com.hola.feeds.atom.Contributor[] getContributor()
    {
        int size = _contributorList.size();
        com.hola.feeds.atom.Contributor[] mArray = new com.hola.feeds.atom.Contributor[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hola.feeds.atom.Contributor) _contributorList.get(index);
        }
        return mArray;
    } //-- com.hola.feeds.atom.Contributor[] getContributor() 

    /**
     * Method getContributorCount
     */
    public int getContributorCount()
    {
        return _contributorList.size();
    } //-- int getContributorCount() 

    /**
     * Returns the value of field 'created'.
     * 
     * @return the value of field 'created'.
     */
    public java.lang.String getCreated()
    {
        return this._created;
    } //-- java.lang.String getCreated() 

    /**
     * Returns the value of field 'id'.
     * 
     * @return the value of field 'id'.
     */
    public java.lang.String getId()
    {
        return this._id;
    } //-- java.lang.String getId() 

    /**
     * Returns the value of field 'issued'.
     * 
     * @return the value of field 'issued'.
     */
    public java.lang.String getIssued()
    {
        return this._issued;
    } //-- java.lang.String getIssued() 

    /**
     * Returns the value of field 'lang'.
     * 
     * @return the value of field 'lang'.
     */
    public java.lang.Object getLang()
    {
        return this._lang;
    } //-- java.lang.Object getLang() 

    /**
     * Method getLink
     * 
     * @param index
     */
    public com.hola.feeds.atom.Link getLink(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _linkList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hola.feeds.atom.Link) _linkList.get(index);
    } //-- com.hola.feeds.atom.Link getLink(int) 

    /**
     * Method getLink
     */
    public com.hola.feeds.atom.Link[] getLink()
    {
        int size = _linkList.size();
        com.hola.feeds.atom.Link[] mArray = new com.hola.feeds.atom.Link[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hola.feeds.atom.Link) _linkList.get(index);
        }
        return mArray;
    } //-- com.hola.feeds.atom.Link[] getLink() 

    /**
     * Method getLinkCount
     */
    public int getLinkCount()
    {
        return _linkList.size();
    } //-- int getLinkCount() 

    /**
     * Returns the value of field 'modified'.
     * 
     * @return the value of field 'modified'.
     */
    public java.lang.String getModified()
    {
        return this._modified;
    } //-- java.lang.String getModified() 

    /**
     * Returns the value of field 'summary'.
     * 
     * @return the value of field 'summary'.
     */
    public com.hola.feeds.atom.Summary getSummary()
    {
        return this._summary;
    } //-- com.hola.feeds.atom.Summary getSummary() 

    /**
     * Returns the value of field 'title'.
     * 
     * @return the value of field 'title'.
     */
    public java.lang.String getTitle()
    {
        return this._title;
    } //-- java.lang.String getTitle() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAnyObject
     * 
     * @param vAnyObject
     */
    public boolean removeAnyObject(java.lang.Object vAnyObject)
    {
        boolean removed = _anyObject.remove(vAnyObject);
        return removed;
    } //-- boolean removeAnyObject(java.lang.Object) 

    /**
     * Method removeContent
     * 
     * @param vContent
     */
    public boolean removeContent(com.hola.feeds.atom.Content vContent)
    {
        boolean removed = _contentList.remove(vContent);
        return removed;
    } //-- boolean removeContent(com.hola.feeds.atom.Content) 

    /**
     * Method removeContributor
     * 
     * @param vContributor
     */
    public boolean removeContributor(com.hola.feeds.atom.Contributor vContributor)
    {
        boolean removed = _contributorList.remove(vContributor);
        return removed;
    } //-- boolean removeContributor(com.hola.feeds.atom.Contributor) 

    /**
     * Method removeLink
     * 
     * @param vLink
     */
    public boolean removeLink(com.hola.feeds.atom.Link vLink)
    {
        boolean removed = _linkList.remove(vLink);
        return removed;
    } //-- boolean removeLink(com.hola.feeds.atom.Link) 

    /**
     * Method setAnyObject
     * 
     * @param index
     * @param vAnyObject
     */
    public void setAnyObject(int index, java.lang.Object vAnyObject)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _anyObject.size())) {
            throw new IndexOutOfBoundsException();
        }
        _anyObject.set(index, vAnyObject);
    } //-- void setAnyObject(int, java.lang.Object) 

    /**
     * Method setAnyObject
     * 
     * @param anyObjectArray
     */
    public void setAnyObject(java.lang.Object[] anyObjectArray)
    {
        //-- copy array
        _anyObject.clear();
        for (int i = 0; i < anyObjectArray.length; i++) {
            _anyObject.add(anyObjectArray[i]);
        }
    } //-- void setAnyObject(java.lang.Object) 

    /**
     * Sets the value of field 'author'.
     * 
     * @param author the value of field 'author'.
     */
    public void setAuthor(com.hola.feeds.atom.Author author)
    {
        this._author = author;
    } //-- void setAuthor(com.hola.feeds.atom.Author) 

    /**
     * Method setContent
     * 
     * @param index
     * @param vContent
     */
    public void setContent(int index, com.hola.feeds.atom.Content vContent)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _contentList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _contentList.set(index, vContent);
    } //-- void setContent(int, com.hola.feeds.atom.Content) 

    /**
     * Method setContent
     * 
     * @param contentArray
     */
    public void setContent(com.hola.feeds.atom.Content[] contentArray)
    {
        //-- copy array
        _contentList.clear();
        for (int i = 0; i < contentArray.length; i++) {
            _contentList.add(contentArray[i]);
        }
    } //-- void setContent(com.hola.feeds.atom.Content) 

    /**
     * Method setContributor
     * 
     * @param index
     * @param vContributor
     */
    public void setContributor(int index, com.hola.feeds.atom.Contributor vContributor)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _contributorList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _contributorList.set(index, vContributor);
    } //-- void setContributor(int, com.hola.feeds.atom.Contributor) 

    /**
     * Method setContributor
     * 
     * @param contributorArray
     */
    public void setContributor(com.hola.feeds.atom.Contributor[] contributorArray)
    {
        //-- copy array
        _contributorList.clear();
        for (int i = 0; i < contributorArray.length; i++) {
            _contributorList.add(contributorArray[i]);
        }
    } //-- void setContributor(com.hola.feeds.atom.Contributor) 

    /**
     * Sets the value of field 'created'.
     * 
     * @param created the value of field 'created'.
     */
    public void setCreated(java.lang.String created)
    {
        this._created = created;
    } //-- void setCreated(java.lang.String) 

    /**
     * Sets the value of field 'id'.
     * 
     * @param id the value of field 'id'.
     */
    public void setId(java.lang.String id)
    {
        this._id = id;
    } //-- void setId(java.lang.String) 

    /**
     * Sets the value of field 'issued'.
     * 
     * @param issued the value of field 'issued'.
     */
    public void setIssued(java.lang.String issued)
    {
        this._issued = issued;
    } //-- void setIssued(java.lang.String) 

    /**
     * Sets the value of field 'lang'.
     * 
     * @param lang the value of field 'lang'.
     */
    public void setLang(java.lang.Object lang)
    {
        this._lang = lang;
    } //-- void setLang(java.lang.Object) 

    /**
     * Method setLink
     * 
     * @param index
     * @param vLink
     */
    public void setLink(int index, com.hola.feeds.atom.Link vLink)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _linkList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _linkList.set(index, vLink);
    } //-- void setLink(int, com.hola.feeds.atom.Link) 

    /**
     * Method setLink
     * 
     * @param linkArray
     */
    public void setLink(com.hola.feeds.atom.Link[] linkArray)
    {
        //-- copy array
        _linkList.clear();
        for (int i = 0; i < linkArray.length; i++) {
            _linkList.add(linkArray[i]);
        }
    } //-- void setLink(com.hola.feeds.atom.Link) 

    /**
     * Sets the value of field 'modified'.
     * 
     * @param modified the value of field 'modified'.
     */
    public void setModified(java.lang.String modified)
    {
        this._modified = modified;
    } //-- void setModified(java.lang.String) 

    /**
     * Sets the value of field 'summary'.
     * 
     * @param summary the value of field 'summary'.
     */
    public void setSummary(com.hola.feeds.atom.Summary summary)
    {
        this._summary = summary;
    } //-- void setSummary(com.hola.feeds.atom.Summary) 

    /**
     * Sets the value of field 'title'.
     * 
     * @param title the value of field 'title'.
     */
    public void setTitle(java.lang.String title)
    {
        this._title = title;
    } //-- void setTitle(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hola.feeds.atom.EntryType) Unmarshaller.unmarshal(com.hola.feeds.atom.EntryType.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
