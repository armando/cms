/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.atom.types;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class LinkTypeRelType.
 * 
 * @version $Revision$ $Date$
 */
public class LinkTypeRelType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * The alternate type
     */
    public static final int ALTERNATE_TYPE = 0;

    /**
     * The instance of the alternate type
     */
    public static final LinkTypeRelType ALTERNATE = new LinkTypeRelType(ALTERNATE_TYPE, "alternate");

    /**
     * The start type
     */
    public static final int START_TYPE = 1;

    /**
     * The instance of the start type
     */
    public static final LinkTypeRelType START = new LinkTypeRelType(START_TYPE, "start");

    /**
     * The next type
     */
    public static final int NEXT_TYPE = 2;

    /**
     * The instance of the next type
     */
    public static final LinkTypeRelType NEXT = new LinkTypeRelType(NEXT_TYPE, "next");

    /**
     * The prev type
     */
    public static final int PREV_TYPE = 3;

    /**
     * The instance of the prev type
     */
    public static final LinkTypeRelType PREV = new LinkTypeRelType(PREV_TYPE, "prev");

    /**
     * The service.edit type
     */
    public static final int SERVICE_EDIT_TYPE = 4;

    /**
     * The instance of the service.edit type
     */
    public static final LinkTypeRelType SERVICE_EDIT = new LinkTypeRelType(SERVICE_EDIT_TYPE, "service.edit");

    /**
     * The service.post type
     */
    public static final int SERVICE_POST_TYPE = 5;

    /**
     * The instance of the service.post type
     */
    public static final LinkTypeRelType SERVICE_POST = new LinkTypeRelType(SERVICE_POST_TYPE, "service.post");

    /**
     * The service.feed type
     */
    public static final int SERVICE_FEED_TYPE = 6;

    /**
     * The instance of the service.feed type
     */
    public static final LinkTypeRelType SERVICE_FEED = new LinkTypeRelType(SERVICE_FEED_TYPE, "service.feed");

    /**
     * Field _memberTable
     */
    private static java.util.Hashtable _memberTable = init();

    /**
     * Field type
     */
    private int type = -1;

    /**
     * Field stringValue
     */
    private java.lang.String stringValue = null;


      //----------------/
     //- Constructors -/
    //----------------/

    private LinkTypeRelType(int type, java.lang.String value) {
        super();
        this.type = type;
        this.stringValue = value;
    } //-- com.hola.feeds.atom.types.LinkTypeRelType(int, java.lang.String)


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method enumerateReturns an enumeration of all possible
     * instances of LinkTypeRelType
     */
    public static java.util.Enumeration enumerate()
    {
        return _memberTable.elements();
    } //-- java.util.Enumeration enumerate() 

    /**
     * Method getTypeReturns the type of this LinkTypeRelType
     */
    public int getType()
    {
        return this.type;
    } //-- int getType() 

    /**
     * Method init
     */
    private static java.util.Hashtable init()
    {
        Hashtable members = new Hashtable();
        members.put("alternate", ALTERNATE);
        members.put("start", START);
        members.put("next", NEXT);
        members.put("prev", PREV);
        members.put("service.edit", SERVICE_EDIT);
        members.put("service.post", SERVICE_POST);
        members.put("service.feed", SERVICE_FEED);
        return members;
    } //-- java.util.Hashtable init() 

    /**
     * Method readResolve will be called during deserialization to
     * replace the deserialized object with the correct constant
     * instance. <br/>
     */
    private java.lang.Object readResolve()
    {
        return valueOf(this.stringValue);
    } //-- java.lang.Object readResolve() 

    /**
     * Method toStringReturns the String representation of this
     * LinkTypeRelType
     */
    public java.lang.String toString()
    {
        return this.stringValue;
    } //-- java.lang.String toString() 

    /**
     * Method valueOfReturns a new LinkTypeRelType based on the
     * given String value.
     * 
     * @param string
     */
    public static com.hola.feeds.atom.types.LinkTypeRelType valueOf(java.lang.String string)
    {
        java.lang.Object obj = null;
        if (string != null) obj = _memberTable.get(string);
        if (obj == null) {
            String err = "'" + string + "' is not a valid LinkTypeRelType";
            throw new IllegalArgumentException(err);
        }
        return (LinkTypeRelType) obj;
    } //-- com.hola.feeds.atom.types.LinkTypeRelType valueOf(java.lang.String) 

}
