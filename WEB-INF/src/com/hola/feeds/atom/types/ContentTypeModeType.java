/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.atom.types;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ContentTypeModeType.
 * 
 * @version $Revision$ $Date$
 */
public class ContentTypeModeType implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * The xml type
     */
    public static final int XML_TYPE = 0;

    /**
     * The instance of the xml type
     */
    public static final ContentTypeModeType XML = new ContentTypeModeType(XML_TYPE, "xml");

    /**
     * The escaped type
     */
    public static final int ESCAPED_TYPE = 1;

    /**
     * The instance of the escaped type
     */
    public static final ContentTypeModeType ESCAPED = new ContentTypeModeType(ESCAPED_TYPE, "escaped");

    /**
     * The base64 type
     */
    public static final int BASE64_TYPE = 2;

    /**
     * The instance of the base64 type
     */
    public static final ContentTypeModeType BASE64 = new ContentTypeModeType(BASE64_TYPE, "base64");

    /**
     * Field _memberTable
     */
    private static java.util.Hashtable _memberTable = init();

    /**
     * Field type
     */
    private int type = -1;

    /**
     * Field stringValue
     */
    private java.lang.String stringValue = null;


      //----------------/
     //- Constructors -/
    //----------------/

    private ContentTypeModeType(int type, java.lang.String value) {
        super();
        this.type = type;
        this.stringValue = value;
    } //-- com.hola.feeds.atom.types.ContentTypeModeType(int, java.lang.String)


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method enumerateReturns an enumeration of all possible
     * instances of ContentTypeModeType
     */
    public static java.util.Enumeration enumerate()
    {
        return _memberTable.elements();
    } //-- java.util.Enumeration enumerate() 

    /**
     * Method getTypeReturns the type of this ContentTypeModeType
     */
    public int getType()
    {
        return this.type;
    } //-- int getType() 

    /**
     * Method init
     */
    private static java.util.Hashtable init()
    {
        Hashtable members = new Hashtable();
        members.put("xml", XML);
        members.put("escaped", ESCAPED);
        members.put("base64", BASE64);
        return members;
    } //-- java.util.Hashtable init() 

    /**
     * Method readResolve will be called during deserialization to
     * replace the deserialized object with the correct constant
     * instance. <br/>
     */
    private java.lang.Object readResolve()
    {
        return valueOf(this.stringValue);
    } //-- java.lang.Object readResolve() 

    /**
     * Method toStringReturns the String representation of this
     * ContentTypeModeType
     */
    public java.lang.String toString()
    {
        return this.stringValue;
    } //-- java.lang.String toString() 

    /**
     * Method valueOfReturns a new ContentTypeModeType based on the
     * given String value.
     * 
     * @param string
     */
    public static com.hola.feeds.atom.types.ContentTypeModeType valueOf(java.lang.String string)
    {
        java.lang.Object obj = null;
        if (string != null) obj = _memberTable.get(string);
        if (obj == null) {
            String err = "'" + string + "' is not a valid ContentTypeModeType";
            throw new IllegalArgumentException(err);
        }
        return (ContentTypeModeType) obj;
    } //-- com.hola.feeds.atom.types.ContentTypeModeType valueOf(java.lang.String) 

}
