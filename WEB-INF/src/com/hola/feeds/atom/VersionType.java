/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hola.feeds.atom;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.Serializable;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class VersionType.
 * 
 * @version $Revision$ $Date$
 */
public class VersionType implements java.io.Serializable {


      //----------------/
     //- Constructors -/
    //----------------/

    public VersionType() {
        super();
    } //-- com.hola.feeds.atom.VersionType()

}
