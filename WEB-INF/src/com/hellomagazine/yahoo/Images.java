/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.9</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.yahoo;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Images.
 * 
 * @version $Revision$ $Date$
 */
public class Images extends com.hellomagazine.Document 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _imageList
     */
    private java.util.ArrayList _imageList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Images() 
     {
        super();
        _imageList = new ArrayList();
    } //-- com.hellomagazine.yahoo.Images()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addImage
     * 
     * 
     * 
     * @param vImage
     */
    public void addImage(java.lang.String vImage)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_imageList.size() < 2)) {
            throw new IndexOutOfBoundsException();
        }
        _imageList.add(vImage);
    } //-- void addImage(java.lang.String) 

    /**
     * Method addImage
     * 
     * 
     * 
     * @param index
     * @param vImage
     */
    public void addImage(int index, java.lang.String vImage)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_imageList.size() < 2)) {
            throw new IndexOutOfBoundsException();
        }
        _imageList.add(index, vImage);
    } //-- void addImage(int, java.lang.String) 

    /**
     * Method clearImage
     * 
     */
    public void clearImage()
    {
        _imageList.clear();
    } //-- void clearImage() 

    /**
     * Method enumerateImage
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateImage()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_imageList.iterator());
    } //-- java.util.Enumeration enumerateImage() 

    /**
     * Method getImage
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String getImage(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _imageList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_imageList.get(index);
    } //-- java.lang.String getImage(int) 

    /**
     * Method getImage
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String[] getImage()
    {
        int size = _imageList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_imageList.get(index);
        }
        return mArray;
    } //-- java.lang.String[] getImage() 

    /**
     * Method getImageCount
     * 
     * 
     * 
     * @return int
     */
    public int getImageCount()
    {
        return _imageList.size();
    } //-- int getImageCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeImage
     * 
     * 
     * 
     * @param vImage
     * @return boolean
     */
    public boolean removeImage(java.lang.String vImage)
    {
        boolean removed = _imageList.remove(vImage);
        return removed;
    } //-- boolean removeImage(java.lang.String) 

    /**
     * Method setImage
     * 
     * 
     * 
     * @param index
     * @param vImage
     */
    public void setImage(int index, java.lang.String vImage)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _imageList.size())) {
            throw new IndexOutOfBoundsException();
        }
        if (!(index < 2)) {
            throw new IndexOutOfBoundsException();
        }
        _imageList.set(index, vImage);
    } //-- void setImage(int, java.lang.String) 

    /**
     * Method setImage
     * 
     * 
     * 
     * @param imageArray
     */
    public void setImage(java.lang.String[] imageArray)
    {
        //-- copy array
        _imageList.clear();
        for (int i = 0; i < imageArray.length; i++) {
            _imageList.add(imageArray[i]);
        }
    } //-- void setImage(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Images
     */
    public static com.hellomagazine.yahoo.Images unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.yahoo.Images) Unmarshaller.unmarshal(com.hellomagazine.yahoo.Images.class, reader);
    } //-- com.hellomagazine.yahoo.Images unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
