/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.9</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.yahoo;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Articles.
 * 
 * @version $Revision$ $Date$
 */
public class Articles extends com.hellomagazine.Document 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _articleList
     */
    private java.util.ArrayList _articleList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Articles() 
     {
        super();
        _articleList = new ArrayList();
    } //-- com.hellomagazine.yahoo.Articles()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addArticle
     * 
     * 
     * 
     * @param vArticle
     */
    public void addArticle(com.hellomagazine.yahoo.Article vArticle)
        throws java.lang.IndexOutOfBoundsException
    {
        _articleList.add(vArticle);
    } //-- void addArticle(com.hellomagazine.yahoo.Article) 

    /**
     * Method addArticle
     * 
     * 
     * 
     * @param index
     * @param vArticle
     */
    public void addArticle(int index, com.hellomagazine.yahoo.Article vArticle)
        throws java.lang.IndexOutOfBoundsException
    {
        _articleList.add(index, vArticle);
    } //-- void addArticle(int, com.hellomagazine.yahoo.Article) 

    /**
     * Method clearArticle
     * 
     */
    public void clearArticle()
    {
        _articleList.clear();
    } //-- void clearArticle() 

    /**
     * Method enumerateArticle
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateArticle()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_articleList.iterator());
    } //-- java.util.Enumeration enumerateArticle() 

    /**
     * Method getArticle
     * 
     * 
     * 
     * @param index
     * @return Article
     */
    public com.hellomagazine.yahoo.Article getArticle(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _articleList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hellomagazine.yahoo.Article) _articleList.get(index);
    } //-- com.hellomagazine.yahoo.Article getArticle(int) 

    /**
     * Method getArticle
     * 
     * 
     * 
     * @return Article
     */
    public com.hellomagazine.yahoo.Article[] getArticle()
    {
        int size = _articleList.size();
        com.hellomagazine.yahoo.Article[] mArray = new com.hellomagazine.yahoo.Article[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hellomagazine.yahoo.Article) _articleList.get(index);
        }
        return mArray;
    } //-- com.hellomagazine.yahoo.Article[] getArticle() 

    /**
     * Method getArticleCount
     * 
     * 
     * 
     * @return int
     */
    public int getArticleCount()
    {
        return _articleList.size();
    } //-- int getArticleCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeArticle
     * 
     * 
     * 
     * @param vArticle
     * @return boolean
     */
    public boolean removeArticle(com.hellomagazine.yahoo.Article vArticle)
    {
        boolean removed = _articleList.remove(vArticle);
        return removed;
    } //-- boolean removeArticle(com.hellomagazine.yahoo.Article) 

    /**
     * Method setArticle
     * 
     * 
     * 
     * @param index
     * @param vArticle
     */
    public void setArticle(int index, com.hellomagazine.yahoo.Article vArticle)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _articleList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _articleList.set(index, vArticle);
    } //-- void setArticle(int, com.hellomagazine.yahoo.Article) 

    /**
     * Method setArticle
     * 
     * 
     * 
     * @param articleArray
     */
    public void setArticle(com.hellomagazine.yahoo.Article[] articleArray)
    {
        //-- copy array
        _articleList.clear();
        for (int i = 0; i < articleArray.length; i++) {
            _articleList.add(articleArray[i]);
        }
    } //-- void setArticle(com.hellomagazine.yahoo.Article) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Articles
     */
    public static com.hellomagazine.yahoo.Articles unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.yahoo.Articles) Unmarshaller.unmarshal(com.hellomagazine.yahoo.Articles.class, reader);
    } //-- com.hellomagazine.yahoo.Articles unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
