/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.9</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.yahoo;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Article.
 * 
 * @version $Revision$ $Date$
 */
public class Article extends com.hellomagazine.Document 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * internal content storage
     */
    private java.lang.String _content = "";

    /**
     * Field _author
     */
    private java.lang.String _author;

    /**
     * Field _caption_photo_1
     */
    private java.lang.String _caption_photo_1;

    /**
     * Field _caption_photo_2
     */
    private java.lang.String _caption_photo_2;

    /**
     * Field _category
     */
    private java.lang.String _category;

    /**
     * Field _date
     */
    private java.lang.String _date;

    /**
     * Field _headline
     */
    private java.lang.String _headline;

    /**
     * Field _url
     */
    private java.lang.String _url;

    /**
     * Field _relatedList
     */
    private java.util.ArrayList _relatedList;

    /**
     * Field _story
     */
    private java.lang.String _story;

    /**
     * Field _summary
     */
    private java.lang.String _summary;

    /**
     * Field _images
     */
    private com.hellomagazine.yahoo.Images _images;

    /**
     * Field _mediaid
     */
    private java.lang.Integer _mediaid;

    /**
     * Field _provider
     */
    private java.lang.String _provider;


      //----------------/
     //- Constructors -/
    //----------------/

    public Article() 
     {
        super();
        setContent("");
        _relatedList = new ArrayList();
    } //-- com.hellomagazine.yahoo.Article()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addRelated
     * 
     * 
     * 
     * @param vRelated
     */
    public void addRelated(java.lang.String vRelated)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_relatedList.size() < 3)) {
            throw new IndexOutOfBoundsException();
        }
        _relatedList.add(vRelated);
    } //-- void addRelated(java.lang.String) 

    /**
     * Method addRelated
     * 
     * 
     * 
     * @param index
     * @param vRelated
     */
    public void addRelated(int index, java.lang.String vRelated)
        throws java.lang.IndexOutOfBoundsException
    {
        if (!(_relatedList.size() < 3)) {
            throw new IndexOutOfBoundsException();
        }
        _relatedList.add(index, vRelated);
    } //-- void addRelated(int, java.lang.String) 

    /**
     * Method clearRelated
     * 
     */
    public void clearRelated()
    {
        _relatedList.clear();
    } //-- void clearRelated() 

    /**
     * Method enumerateRelated
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateRelated()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_relatedList.iterator());
    } //-- java.util.Enumeration enumerateRelated() 

    /**
     * Returns the value of field 'author'.
     * 
     * @return String
     * @return the value of field 'author'.
     */
    public java.lang.String getAuthor()
    {
        return this._author;
    } //-- java.lang.String getAuthor() 

    /**
     * Returns the value of field 'caption_photo_1'.
     * 
     * @return String
     * @return the value of field 'caption_photo_1'.
     */
    public java.lang.String getCaption_photo_1()
    {
        return this._caption_photo_1;
    } //-- java.lang.String getCaption_photo_1() 

    /**
     * Returns the value of field 'caption_photo_2'.
     * 
     * @return String
     * @return the value of field 'caption_photo_2'.
     */
    public java.lang.String getCaption_photo_2()
    {
        return this._caption_photo_2;
    } //-- java.lang.String getCaption_photo_2() 

    /**
     * Returns the value of field 'category'.
     * 
     * @return String
     * @return the value of field 'category'.
     */
    public java.lang.String getCategory()
    {
        return this._category;
    } //-- java.lang.String getCategory() 

    /**
     * Returns the value of field 'content'. The field 'content'
     * has the following description: internal content storage
     * 
     * @return String
     * @return the value of field 'content'.
     */
    public java.lang.String getContent()
    {
        return this._content;
    } //-- java.lang.String getContent() 

    /**
     * Returns the value of field 'date'.
     * 
     * @return String
     * @return the value of field 'date'.
     */
    public java.lang.String getDate()
    {
        return this._date;
    } //-- java.lang.String getDate() 

    /**
     * Returns the value of field 'headline'.
     * 
     * @return String
     * @return the value of field 'headline'.
     */
    public java.lang.String getHeadline()
    {
        return this._headline;
    } //-- java.lang.String getHeadline() 

    /**
     * Returns the value of field 'images'.
     * 
     * @return Images
     * @return the value of field 'images'.
     */
    public com.hellomagazine.yahoo.Images getImages()
    {
        return this._images;
    } //-- com.hellomagazine.yahoo.Images getImages() 

    /**
     * Returns the value of field 'mediaid'.
     * 
     * @return Integer
     * @return the value of field 'mediaid'.
     */
    public java.lang.Integer getMediaid()
    {
        return this._mediaid;
    } //-- java.lang.Integer getMediaid() 

    /**
     * Returns the value of field 'provider'.
     * 
     * @return String
     * @return the value of field 'provider'.
     */
    public java.lang.String getProvider()
    {
        return this._provider;
    } //-- java.lang.String getProvider() 

    /**
     * Method getRelated
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String getRelated(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _relatedList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_relatedList.get(index);
    } //-- java.lang.String getRelated(int) 

    /**
     * Method getRelated
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String[] getRelated()
    {
        int size = _relatedList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_relatedList.get(index);
        }
        return mArray;
    } //-- java.lang.String[] getRelated() 

    /**
     * Method getRelatedCount
     * 
     * 
     * 
     * @return int
     */
    public int getRelatedCount()
    {
        return _relatedList.size();
    } //-- int getRelatedCount() 

    /**
     * Returns the value of field 'story'.
     * 
     * @return String
     * @return the value of field 'story'.
     */
    public java.lang.String getStory()
    {
        return this._story;
    } //-- java.lang.String getStory() 

    /**
     * Returns the value of field 'summary'.
     * 
     * @return String
     * @return the value of field 'summary'.
     */
    public java.lang.String getSummary()
    {
        return this._summary;
    } //-- java.lang.String getSummary() 

    /**
     * Returns the value of field 'url'.
     * 
     * @return String
     * @return the value of field 'url'.
     */
    public java.lang.String getUrl()
    {
        return this._url;
    } //-- java.lang.String getUrl() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeRelated
     * 
     * 
     * 
     * @param vRelated
     * @return boolean
     */
    public boolean removeRelated(java.lang.String vRelated)
    {
        boolean removed = _relatedList.remove(vRelated);
        return removed;
    } //-- boolean removeRelated(java.lang.String) 

    /**
     * Sets the value of field 'author'.
     * 
     * @param author the value of field 'author'.
     */
    public void setAuthor(java.lang.String author)
    {
        this._author = author;
    } //-- void setAuthor(java.lang.String) 

    /**
     * Sets the value of field 'caption_photo_1'.
     * 
     * @param caption_photo_1 the value of field 'caption_photo_1'.
     */
    public void setCaption_photo_1(java.lang.String caption_photo_1)
    {
        this._caption_photo_1 = caption_photo_1;
    } //-- void setCaption_photo_1(java.lang.String) 

    /**
     * Sets the value of field 'caption_photo_2'.
     * 
     * @param caption_photo_2 the value of field 'caption_photo_2'.
     */
    public void setCaption_photo_2(java.lang.String caption_photo_2)
    {
        this._caption_photo_2 = caption_photo_2;
    } //-- void setCaption_photo_2(java.lang.String) 

    /**
     * Sets the value of field 'category'.
     * 
     * @param category the value of field 'category'.
     */
    public void setCategory(java.lang.String category)
    {
        this._category = category;
    } //-- void setCategory(java.lang.String) 

    /**
     * Sets the value of field 'content'. The field 'content' has
     * the following description: internal content storage
     * 
     * @param content the value of field 'content'.
     */
    public void setContent(java.lang.String content)
    {
        this._content = content;
    } //-- void setContent(java.lang.String) 

    /**
     * Sets the value of field 'date'.
     * 
     * @param date the value of field 'date'.
     */
    public void setDate(java.lang.String date)
    {
        this._date = date;
    } //-- void setDate(java.lang.String) 

    /**
     * Sets the value of field 'headline'.
     * 
     * @param headline the value of field 'headline'.
     */
    public void setHeadline(java.lang.String headline)
    {
        this._headline = headline;
    } //-- void setHeadline(java.lang.String) 

    /**
     * Sets the value of field 'images'.
     * 
     * @param images the value of field 'images'.
     */
    public void setImages(com.hellomagazine.yahoo.Images images)
    {
        this._images = images;
    } //-- void setImages(com.hellomagazine.yahoo.Images) 

    /**
     * Sets the value of field 'mediaid'.
     * 
     * @param mediaid the value of field 'mediaid'.
     */
    public void setMediaid(java.lang.Integer mediaid)
    {
        this._mediaid = mediaid;
    } //-- void setMediaid(java.lang.Integer) 

    /**
     * Sets the value of field 'provider'.
     * 
     * @param provider the value of field 'provider'.
     */
    public void setProvider(java.lang.String provider)
    {
        this._provider = provider;
    } //-- void setProvider(java.lang.String) 

    /**
     * Method setRelated
     * 
     * 
     * 
     * @param index
     * @param vRelated
     */
    public void setRelated(int index, java.lang.String vRelated)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _relatedList.size())) {
            throw new IndexOutOfBoundsException();
        }
        if (!(index < 3)) {
            throw new IndexOutOfBoundsException();
        }
        _relatedList.set(index, vRelated);
    } //-- void setRelated(int, java.lang.String) 

    /**
     * Method setRelated
     * 
     * 
     * 
     * @param relatedArray
     */
    public void setRelated(java.lang.String[] relatedArray)
    {
        //-- copy array
        _relatedList.clear();
        for (int i = 0; i < relatedArray.length; i++) {
            _relatedList.add(relatedArray[i]);
        }
    } //-- void setRelated(java.lang.String) 

    /**
     * Sets the value of field 'story'.
     * 
     * @param story the value of field 'story'.
     */
    public void setStory(java.lang.String story)
    {
        this._story = story;
    } //-- void setStory(java.lang.String) 

    /**
     * Sets the value of field 'summary'.
     * 
     * @param summary the value of field 'summary'.
     */
    public void setSummary(java.lang.String summary)
    {
        this._summary = summary;
    } //-- void setSummary(java.lang.String) 

    /**
     * Sets the value of field 'url'.
     * 
     * @param url the value of field 'url'.
     */
    public void setUrl(java.lang.String url)
    {
        this._url = url;
    } //-- void setUrl(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Article
     */
    public static com.hellomagazine.yahoo.Article unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.yahoo.Article) Unmarshaller.unmarshal(com.hellomagazine.yahoo.Article.class, reader);
    } //-- com.hellomagazine.yahoo.Article unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
