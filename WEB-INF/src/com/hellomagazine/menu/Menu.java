/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.9</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.menu;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Menu.
 * 
 * @version $Revision$ $Date$
 */
public class Menu extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _id
     */
    private java.lang.String _id;

    /**
     * Field _submenuList
     */
    private java.util.ArrayList _submenuList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Menu() 
     {
        super();
        _submenuList = new ArrayList();
    } //-- com.hellomagazine.menu.Menu()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addSubmenu
     * 
     * 
     * 
     * @param vSubmenu
     */
    public void addSubmenu(com.hellomagazine.menu.Submenu vSubmenu)
        throws java.lang.IndexOutOfBoundsException
    {
        _submenuList.add(vSubmenu);
    } //-- void addSubmenu(com.hellomagazine.menu.Submenu) 

    /**
     * Method addSubmenu
     * 
     * 
     * 
     * @param index
     * @param vSubmenu
     */
    public void addSubmenu(int index, com.hellomagazine.menu.Submenu vSubmenu)
        throws java.lang.IndexOutOfBoundsException
    {
        _submenuList.add(index, vSubmenu);
    } //-- void addSubmenu(int, com.hellomagazine.menu.Submenu) 

    /**
     * Method clearSubmenu
     * 
     */
    public void clearSubmenu()
    {
        _submenuList.clear();
    } //-- void clearSubmenu() 

    /**
     * Method enumerateSubmenu
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateSubmenu()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_submenuList.iterator());
    } //-- java.util.Enumeration enumerateSubmenu() 

    /**
     * Returns the value of field 'id'.
     * 
     * @return String
     * @return the value of field 'id'.
     */
    public java.lang.String getId()
    {
        return this._id;
    } //-- java.lang.String getId() 

    /**
     * Method getSubmenu
     * 
     * 
     * 
     * @param index
     * @return Submenu
     */
    public com.hellomagazine.menu.Submenu getSubmenu(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _submenuList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hellomagazine.menu.Submenu) _submenuList.get(index);
    } //-- com.hellomagazine.menu.Submenu getSubmenu(int) 

    /**
     * Method getSubmenu
     * 
     * 
     * 
     * @return Submenu
     */
    public com.hellomagazine.menu.Submenu[] getSubmenu()
    {
        int size = _submenuList.size();
        com.hellomagazine.menu.Submenu[] mArray = new com.hellomagazine.menu.Submenu[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hellomagazine.menu.Submenu) _submenuList.get(index);
        }
        return mArray;
    } //-- com.hellomagazine.menu.Submenu[] getSubmenu() 

    /**
     * Method getSubmenuCount
     * 
     * 
     * 
     * @return int
     */
    public int getSubmenuCount()
    {
        return _submenuList.size();
    } //-- int getSubmenuCount() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeSubmenu
     * 
     * 
     * 
     * @param vSubmenu
     * @return boolean
     */
    public boolean removeSubmenu(com.hellomagazine.menu.Submenu vSubmenu)
    {
        boolean removed = _submenuList.remove(vSubmenu);
        return removed;
    } //-- boolean removeSubmenu(com.hellomagazine.menu.Submenu) 

    /**
     * Sets the value of field 'id'.
     * 
     * @param id the value of field 'id'.
     */
    public void setId(java.lang.String id)
    {
        this._id = id;
    } //-- void setId(java.lang.String) 

    /**
     * Method setSubmenu
     * 
     * 
     * 
     * @param index
     * @param vSubmenu
     */
    public void setSubmenu(int index, com.hellomagazine.menu.Submenu vSubmenu)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _submenuList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _submenuList.set(index, vSubmenu);
    } //-- void setSubmenu(int, com.hellomagazine.menu.Submenu) 

    /**
     * Method setSubmenu
     * 
     * 
     * 
     * @param submenuArray
     */
    public void setSubmenu(com.hellomagazine.menu.Submenu[] submenuArray)
    {
        //-- copy array
        _submenuList.clear();
        for (int i = 0; i < submenuArray.length; i++) {
            _submenuList.add(submenuArray[i]);
        }
    } //-- void setSubmenu(com.hellomagazine.menu.Submenu) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Menu
     */
    public static com.hellomagazine.menu.Menu unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.menu.Menu) Unmarshaller.unmarshal(com.hellomagazine.menu.Menu.class, reader);
    } //-- com.hellomagazine.menu.Menu unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
