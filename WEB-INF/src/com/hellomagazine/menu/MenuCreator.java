/*
 * Created on 28-feb-2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.hellomagazine.menu;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.exolab.castor.util.LocalConfiguration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * @author armando
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MenuCreator {
    public static void main(String[] args) {
        Menu m= new Menu();
        m.setId("id");
        Submenu sm= new Submenu();
        sm.setName("SM1");
        m.addSubmenu(sm);
        Submenu sm2= new Submenu();
        sm2.setName("SM2");
        sm.addSubmenu(sm2);
        Submenu sm5= new Submenu();
        sm5.setName("SM3");
        sm2.addSubmenu(sm5);
        LocalConfiguration.getInstance().getProperties().setProperty("org.exolab.castor.indent","true");
        try {
            m.marshal(new FileWriter("menu.xml"));
        } catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        try {
            Menu m2= Menu.unmarshal(new FileReader("menu.xml"));
            Submenu sm3= m2.getSubmenu(0);
            Submenu sm4= (Submenu) sm3.getSubmenu(0);
            Submenu sm6= (Submenu) sm4.getSubmenu(0);
            System.out.println(sm6.getName());
        } catch (MarshalException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (ValidationException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        
    }
}
