/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.9</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.menu;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Submenu.
 * 
 * @version $Revision$ $Date$
 */
public class Submenu extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _name
     */
    private java.lang.String _name;

    /**
     * Field _description
     */
    private java.lang.String _description;

    /**
     * Field _URL
     */
    private java.lang.String _URL;

    /**
     * Field _submenuList
     */
    private java.util.ArrayList _submenuList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Submenu() 
     {
        super();
        _submenuList = new ArrayList();
    } //-- com.hellomagazine.menu.Submenu()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addSubmenu
     * 
     * 
     * 
     * @param vSubmenu
     */
    public void addSubmenu(java.lang.Object vSubmenu)
        throws java.lang.IndexOutOfBoundsException
    {
        _submenuList.add(vSubmenu);
    } //-- void addSubmenu(java.lang.Object) 

    /**
     * Method addSubmenu
     * 
     * 
     * 
     * @param index
     * @param vSubmenu
     */
    public void addSubmenu(int index, java.lang.Object vSubmenu)
        throws java.lang.IndexOutOfBoundsException
    {
        _submenuList.add(index, vSubmenu);
    } //-- void addSubmenu(int, java.lang.Object) 

    /**
     * Method clearSubmenu
     * 
     */
    public void clearSubmenu()
    {
        _submenuList.clear();
    } //-- void clearSubmenu() 

    /**
     * Method enumerateSubmenu
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateSubmenu()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_submenuList.iterator());
    } //-- java.util.Enumeration enumerateSubmenu() 

    /**
     * Returns the value of field 'description'.
     * 
     * @return String
     * @return the value of field 'description'.
     */
    public java.lang.String getDescription()
    {
        return this._description;
    } //-- java.lang.String getDescription() 

    /**
     * Returns the value of field 'name'.
     * 
     * @return String
     * @return the value of field 'name'.
     */
    public java.lang.String getName()
    {
        return this._name;
    } //-- java.lang.String getName() 

    /**
     * Method getSubmenu
     * 
     * 
     * 
     * @param index
     * @return Object
     */
    public java.lang.Object getSubmenu(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _submenuList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (java.lang.Object) _submenuList.get(index);
    } //-- java.lang.Object getSubmenu(int) 

    /**
     * Method getSubmenu
     * 
     * 
     * 
     * @return Object
     */
    public java.lang.Object[] getSubmenu()
    {
        int size = _submenuList.size();
        java.lang.Object[] mArray = new java.lang.Object[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (java.lang.Object) _submenuList.get(index);
        }
        return mArray;
    } //-- java.lang.Object[] getSubmenu() 

    /**
     * Method getSubmenuCount
     * 
     * 
     * 
     * @return int
     */
    public int getSubmenuCount()
    {
        return _submenuList.size();
    } //-- int getSubmenuCount() 

    /**
     * Returns the value of field 'URL'.
     * 
     * @return String
     * @return the value of field 'URL'.
     */
    public java.lang.String getURL()
    {
        return this._URL;
    } //-- java.lang.String getURL() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeSubmenu
     * 
     * 
     * 
     * @param vSubmenu
     * @return boolean
     */
    public boolean removeSubmenu(java.lang.Object vSubmenu)
    {
        boolean removed = _submenuList.remove(vSubmenu);
        return removed;
    } //-- boolean removeSubmenu(java.lang.Object) 

    /**
     * Sets the value of field 'description'.
     * 
     * @param description the value of field 'description'.
     */
    public void setDescription(java.lang.String description)
    {
        this._description = description;
    } //-- void setDescription(java.lang.String) 

    /**
     * Sets the value of field 'name'.
     * 
     * @param name the value of field 'name'.
     */
    public void setName(java.lang.String name)
    {
        this._name = name;
    } //-- void setName(java.lang.String) 

    /**
     * Method setSubmenu
     * 
     * 
     * 
     * @param index
     * @param vSubmenu
     */
    public void setSubmenu(int index, java.lang.Object vSubmenu)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _submenuList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _submenuList.set(index, vSubmenu);
    } //-- void setSubmenu(int, java.lang.Object) 

    /**
     * Method setSubmenu
     * 
     * 
     * 
     * @param submenuArray
     */
    public void setSubmenu(java.lang.Object[] submenuArray)
    {
        //-- copy array
        _submenuList.clear();
        for (int i = 0; i < submenuArray.length; i++) {
            _submenuList.add(submenuArray[i]);
        }
    } //-- void setSubmenu(java.lang.Object) 

    /**
     * Sets the value of field 'URL'.
     * 
     * @param URL the value of field 'URL'.
     */
    public void setURL(java.lang.String URL)
    {
        this._URL = URL;
    } //-- void setURL(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Submenu
     */
    public static com.hellomagazine.menu.Submenu unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.menu.Submenu) Unmarshaller.unmarshal(com.hellomagazine.menu.Submenu.class, reader);
    } //-- com.hellomagazine.menu.Submenu unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
