/*
 * Created on 11-nov-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hellomagazine.ed.jbag;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.xml.serialize.Method;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.exolab.castor.util.LocalConfiguration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.DocumentHandler;

import com.hellomagazine.ed.ticker.TickerForm;
import com.hellomagazine.jbag.Action;
import com.hellomagazine.jbag.Command;
import com.hellomagazine.jbag.Description;
import com.hellomagazine.jbag.Entry;
import com.hellomagazine.jbag.Fmt;
import com.hellomagazine.jbag.Image;
import com.hellomagazine.jbag.Item;
import com.hellomagazine.jbag.JList;
import com.hellomagazine.jbag.JView;
import com.hellomagazine.jbag.Jbag;
import com.hellomagazine.jbag.Lf;
import com.hellomagazine.jbag.Midlet;
import com.hellomagazine.jbag.Par;
import com.hellomagazine.jbag.Piece;
import com.hellomagazine.jbag.Rasterpic;
import com.hellomagazine.jbag.Splash;
import com.hellomagazine.newsticker.Headline;
import com.hellomagazine.newsticker.Ticker;
import com.hola.util.text.Filter;
import com.mindprod.base64.Base64;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageEncoder;

/**
 * @author armando
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class WapFactory {
    static Map images= new HashMap();
    private static final int MAX_STORIES= 8;
    private static final int MAX_IMAGES= 8;

    public WapFactory() {
    }

    public static Jbag create() {
        Jbag jbag= new Jbag();
        Description d= new Description();
        Entry e= new Entry();
        e.setRef("start");
        d.setEntry(e);
        Splash s= new Splash();
        s.setImg("splash");
        s.setProgressbar("no");
        s.setWaitforclick("yes");
        s.setDelay(new Integer(0));
        d.setSplash(s);
        Midlet m= new Midlet();
        m.setName("Hello");
        m.setIcon("icon.png");
        d.setMidlet(m);

        Action a= new Action();
        a.setEvent("onWapOpen");
        Command c= new Command();
        c.setCmd("useVersion");
        c.setValue("0");
        a.addCommand(c);
        d.addAction(a);

        a= new Action();
        a.setEvent("onSubscriptionStart");
        c= new Command();
        c.setCmd("useVersion");
        c.setValue("0");
        a.addCommand(c);
        c= new Command();
        c.setCmd("createSubscription");
        c.setValue("overwrite=yes");
        a.addCommand(c);
        c= new Command();
        c.setCmd("updateSubscriptionTime");
        c.setValue("forever");
        a.addCommand(c);
        c= new Command();
        c.setCmd("sendWapPush");
        c.setValue("");
        a.addCommand(c);
        d.addAction(a);

        a= new Action();
        a.setEvent("onSubscriptionStop");
        c= new Command();
        c.setCmd("cancelSubscription");
        c.setValue("");
        a.addCommand(c);
        c= new Command();
        c.setCmd("sendSM");
        c.setValue("Your subscription has been canceled!");
        a.addCommand(c);
        d.addAction(a);

        a= new Action();
        a.setEvent("onDownload");
        c= new Command();
        c.setCmd("useSubscription");
        c.setValue("");
        a.addCommand(c);
        d.addAction(a);

        a= new Action();
        a.setEvent("onUpdateRequest");
        c= new Command();
        c.setCmd("verifySubscription");
        c.setValue("");
        a.addCommand(c);
        c= new Command();
        c.setCmd("updateContent");
        c.setValue("newest");
        a.addCommand(c);
        d.addAction(a);

        jbag.setDescription(d);

        addAbout(jbag);

        String[] icons= { "jl_icon", "jl_top", "jl_bottom", "splash",
                "update_icon", "icon" };

        for (int j= 0; j < icons.length; j++){
            Rasterpic r= new Rasterpic();
            r.setName(icons[j]);
            r.setLink("file");
            r.setFormat("png");
            r.setContent(icons[j] + ".png");
            jbag.addRasterpic(r);
        }

        return jbag;
    }

    private static void addAbout(Jbag jbag) {
        JView jw= new JView();
        jw.setName("about");
        jw.setResource(new Integer("0"));

        Piece p= new Piece();
        p.setImage(makeImage("jl_top"));
        Par pa= new Par();
        pa.addFmt(makeFmt("m", "000000", "b", "Service provided by...", 0));
        p.setPar(pa);
        jw.addPiece(p);

        p= new Piece();
        pa= new Par();
        pa
                .addFmt(makeFmt(
                        "s",
                        "000000",
                        "",
                        "HELLO! mobile services are provided by www.hellomagazine.com in partnership with PlayerMedia. The WAP component is powered by www.jbag.net",1));

        pa
        .addFmt(makeFmt(
                "s",
                "000000",
                "",
                "Details of the service and help pages can be found at www.playermedia.co.uk.",1));
        pa
        .addFmt(makeFmt(
                "s",
                "000000",
                "",
                "If you are experiencing a specific problem please e-mail help@playermedia.co.uk or write to PlayerMedia Ltd, Glyn House, 16 City Rd, London EC1 2AA, including the following information:",0));

        pa
        .addFmt(makeFmt(
                "s",
                "000000",
                "",
                "* the code of the product you ordered",0));
        pa
        .addFmt(makeFmt(
                "s",
                "000000",
                "",
                "* your mobile phone number",0));
        pa
        .addFmt(makeFmt(
                "s",
                "000000",
                "",
                "* your mobile network",0));
        pa
        .addFmt(makeFmt(
                "s",
                "000000",
                "",
                "* your handset make and model, plus detailed description of the problem experienced",1));
        pa
        .addFmt(makeFmt(
                "s",
                "000000",
                "",
                "For any other queries please e-mail hello@playermedia.co.uk.",1));
        
        
        p.setPar(pa);
        jw.addPiece(p);   
        
        jbag.addJView(jw);   
    }

    private static Fmt makeFmt(String size, String color, String style,
            String content, int nl) {
        Fmt f= new Fmt();
        f.setFont_size(size);
        f.setTxt_color(color);
        f.setFont_style(style);
        f.setContent(content);
        for (int i= 0; i< nl; i++){
            f.addLf(new Lf());
        }
        return f;
    }

    private static Image makeImage(String ref) {
        Image i= new Image();
        i.setRef(ref);
        return i;
    }

    public static void createJList(Jbag jbag, List itemList, Map stories)
            throws IOException {
        JList jl= new JList();
        jl.setName("start");
        jl.setHeight(new Integer(20));
        jl.setImg_top("jl_top");
        jl.setImg_bottom("jl_bottom");
        jl.setResource(new Integer(1));

        Item i= new Item();
        i.setOnClick("CMD:update");
        i.setIcon("update_icon");
        i.setContent("Update content");
        jl.addItem(i);

        int n= 0;
        for (Iterator it= itemList.iterator(); it.hasNext();){
            Headline h= (Headline) it.next();
            Headline c= (Headline) stories.get(h.getId());
            String[] hs= h.getImage();
            String[] cs= (c != null) ? c.getImage() : null;

            String[] ims= (cs != null && cs.length > hs.length) ? cs : hs;

            int numImgs= ims.length;
            //            System.out.println(numImgs + " imagenes");

            if (n >= MAX_STORIES){
                break;
            }

            n++;
            i= new Item();
            i.setOnClick("CRF:hello_wapservice;renderlink;check_payment;story_" + n);
            i.setIcon("jl_icon");
            i.setContent(h.getText());
            jl.addItem(i);

            JView jv= new JView();
            jv.setName("story_" + n);
            jv.setResource(new Integer(n + 1));

//            List heads= parts(h.getText(), 15);
//            for (int j= 0, m= heads.size(); j < m; j++){
//                String s= (String) heads.get(j);
                Piece pi1= new Piece();
//                if (j == 0){
                    Image im1= new Image();
                    im1.setRef("jl_top");
                    pi1.setImage(im1);
//                }
                Par pa1= new Par();
                Fmt f1= new Fmt();
                f1.setTxt_color("000000");
                f1.setFont_style("b"); // o "" o "i"
                f1.setFont_size("m"); // o "s"
                f1.setContent(Filter.toIso(h.getText()));
                pa1.addFmt(f1);
                pi1.setPar(pa1);
                jv.addPiece(pi1);
//            }

//            System.out.println(h.getSummary());
//            System.out.print("\n");
            List paras= toPars(h.getSummary());
//            System.out.println(paras.size()+" parrafos");
            for (int m= 0, p= paras.size(); m < p; m++){
//                                System.out.println("Parrafo "+m);
                String para= (String) paras.get(m);
                Piece pi= new Piece();
                Image im= new Image();

                if (n <= MAX_IMAGES && m < numImgs){
                    String name= "img_" + m + "_story_" + n;
                    im.setRef(name);
                    Rasterpic r= new Rasterpic();
                    r.setName(name);
                    r.setLink("file");
                    r.setFormat("png");
                    r.setContent("img_" + m + "_story_" + n + ".png");
                    jbag.addRasterpic(r);
                    images.put(name, toPNG(ims[m]));
                    pi.setImage(im);
                }

                Par pa= new Par();

                Pattern p1= Pattern.compile("<([^>]+)>([^<]+)</%1>");
                Matcher m1= p1.matcher(para);
//                System.out.println(para);
                int t= 0;
                
                Fmt f= null;
                /*
                while (m1.find()){
                    String subp= para.substring(t, m1.start());
                    if (subp.length() > 0){
                        f= new Fmt();
                        f.setTxt_color("000000");
                        f.setFont_style(""); // o "" o "i"
                        f.setFont_size("s"); // o "s"
                        f.setContent(subp);
                        pa.addFmt(f);
                    }
                    t= m1.end();
                    
                    String style= m1.group(1);
                    String content= m1.group(2);

                    if (content.length() > 0){
                        f= new Fmt();
                        f.setTxt_color("000000");
                        f.setFont_style(style); // o "" o "i"
                        f.setFont_size("s"); // o "s"
                        f.setContent(content);
                        pa.addFmt(f);
                    }
                }
                */
                f= new Fmt();
                f.setTxt_color("000000");
                f.setFont_style(""); // o "" o "i"
                f.setFont_size("s"); // o "s"
                f.setContent(Filter.toIso(para.substring(t)));
                
//                f.addLf(new Lf());
                pa.addFmt(f);

                pi.setPar(pa);
                jv.addPiece(pi);
            }
            jbag.addJView(jv);
        }
        i= new Item();
        i.setOnClick("REF:about");
        i.setIcon("jl_icon");
        i.setContent("Service provided by...");
        jl.addItem(i);
        jbag.setJList(jl);
    }

    public static byte[] toPNG(String image) throws IOException {
        Base64 encoder= new Base64();
        byte[] ba= encoder.decode(image);
        ByteArrayInputStream in= new ByteArrayInputStream(ba);
        BufferedImage ibuffer= ImageIO.read(in);
        ByteArrayOutputStream out= new ByteArrayOutputStream();
        ImageEncoder iencoder= ImageCodec.createImageEncoder("PNG", out, null);
        iencoder.encode(ibuffer);
        return out.toByteArray();
    }

    public static List toPars(String text) {
        //        System.out.println(text);
        List pars= new ArrayList();
        int pb= 0, id= 0;
        while (pb >= 0){
            pb= text.indexOf("\r\n\r\n", id);
            String par= (pb >= 0) ? text.substring(id, pb) : text.substring(id);
            pars.add(par);
            id= pb + 4;
        }
        /*
         * Pattern p1= Pattern.compile("([^\\n]+)"); Pattern p2=
         * Pattern.compile("([0-9]+)(x([0-9]+))?"); String name= null, type=
         * null;
         * 
         * int w= 0, h= 0; int maxw= 0, maxh= 0; Matcher m1= p1.matcher(file);
         * if (m1.matches()){ name= m1.group(1); type= m1.group(2);
         * 
         * Matcher m2= p2.matcher(name); if (m2.matches()){ w=
         * Integer.parseInt(m2.group(1)); if (m2.group(2) != null){ h=
         * Integer.parseInt(m2.group(3)); } else{ h= 0; } //
         * System.out.println(w + ":" + h + ":" + type); } }
         *  
         */
        return pars;
    }

    public static List parts(String s, int max) {
        List l= new ArrayList();
        StringTokenizer st= new StringTokenizer(s, " ");
        StringBuffer sb= new StringBuffer();
        while (st.hasMoreTokens()){
            String word= st.nextToken();
            if (word.length() + sb.length() > max){
                l.add(sb.toString());
                sb= new StringBuffer();
            }
            sb.append(word + " ");
        }
        l.add(sb.toString());
        return l;
    }

    public static Map getImages() {
        return images;
    }

    public static String getJBag() throws IOException {
        Ticker t= TickerForm.loadTicker("wap");
        //        Ticker c= TickerForm.loadTicker("headlines2");
        Map stories= TickerForm.loadContext();
        List headlines= Arrays.asList(t.getHeadline());
        
        Jbag j= create();
        createJList(j, headlines, stories);
        ByteArrayOutputStream bo= new ByteArrayOutputStream();
        LocalConfiguration.getInstance().getProperties().setProperty(
                "org.exolab.castor.indent", "true");
        
//      LocalConfiguration.getInstance().getProperties().setProperty(
//      "org.exolab.castor.serializer", "com.hola.ed.util.JBagSerializer");
        
//        FileOutputStream fo= new FileOutputStream(new File("c:\\temp",
//                "jbag.xml"));
//        OutputStreamWriter sw= new OutputStreamWriter(fo, "UTF-8");

//        Marshaller m2= new Marshaller(sw);
//        m2.setEncoding("UTF-8");
        
        // Create output format
        OutputFormat format = new OutputFormat(Method.XML, "ISO-8859-1", true);
        
     // Define the names of the XML elements to put > around
//        String[] cdata = {"fmt"};
//        format.setCDataElements(cdata);
//        format.setNonEscapingElements(cdata); // Those elements should NOT be escaped..
        
        OutputStreamWriter writer= new OutputStreamWriter(bo, "ISO-8859-1");
        
     // Create the serializer
        XMLSerializer serializer = new XMLSerializer(writer, format);
     
     // Create the document handler
        DocumentHandler handler = serializer.asDocumentHandler();
        
     // Create the marshaller
        Marshaller m = new Marshaller(handler);        
        
        try{
            m.marshal(j);
//            m2.marshal(j);
        } catch (MarshalException e){
            e.printStackTrace();
        } catch (ValidationException e){
            e.printStackTrace();
        }
        
        writer.flush();
        writer.close();
        String source= new String(bo.toByteArray());
        
        return (source);
    }

    private static String cut(String s, int n) {
        if (n >= s.length()){
            return s;
        }

        String r= s.substring(0, n - 2);
        int t= r.length() - 1;
        while (t > 0 && Character.isLetterOrDigit(r.charAt(t))){
            r= r.substring(0, t--);
        }

        return r.substring(0, r.length() - 1) + "...";
    }

    public static void main(String[] args) throws IOException {
        //        System.out.println(cut("Esto es una prueba", 8));
                System.out.println(getJBag());
    }

}