package com.hellomagazine.ed.jbag;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.Vector;
import java.util.prefs.BackingStoreException;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;

import org.omg.CORBA.IntHolder;
import org.omg.CORBA.StringHolder;

import com.jbag.cdei.JCDEI;
import com.jbag.cdei.JCDEI_XmlRpc;
import com.jbag.cdei.JCDEI_XmlRpc_tContainerData;
import com.jbag.cdei.JCDEI_XmlRpc_tJbagData;
import com.jbag.cdei.JCDEI_XmlRpc_tJbagResource;
import com.jbag.cdei.JCDEI_XmlRpc_tJbagResourceChange;
import com.jbag.cdei.JCDEI_tContainerData;
import com.jbag.cdei.JCdeiException;
import com.jbag.cdei.JCdeiLoginStatus;
import com.jbag.cdei.JCdeiPreferences;

/*
 * Created on 28.09.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author Martin B�uchler
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class WapCDEI implements PreferenceChangeListener, Observer {
    //////////////////// Model //////////////////////////////////
    /*
     * Model is the CDei Interface
     */
    //	private JCDEI m_CDEI = null;
    private JCDEI_XmlRpc m_CDEI= null;
    private JCdeiPreferences m_CdeiPrefs= null;
    private JCdeiLoginStatus m_LoginStatus= null;

    public JCdeiLoginStatus getLoginStatus() {
        if (m_LoginStatus == null)
            SetLoginStatus(new JCdeiLoginStatus());
        return m_LoginStatus;
    }

    private void SetLoginStatus(JCdeiLoginStatus newStatus) {
        if (m_LoginStatus != null){
            m_LoginStatus.deleteObserver(this);
        }
        m_LoginStatus= newStatus;
        m_LoginStatus.addObserver(this);
    }

    public JCdeiPreferences getCdeiPrefs() {
        if (m_CdeiPrefs == null){
            m_CdeiPrefs= new JCdeiPreferences();
            m_CdeiPrefs.addPreferenceChangeListener(this);
        }
        return m_CdeiPrefs;
    }

    private JCDEI createCDEI() {
        if (getCdeiPrefs().getUseXmlRpc()){
            m_CDEI= new JCDEI_XmlRpc(getCdeiPrefs());
        } else{
            ;// @todo implement CDEI_SOAP
        }
        return m_CDEI;
    }

    public JCDEI_XmlRpc getCDEI()
    //public JCDEI getCDEI()
    {
        if (m_CDEI == null)
            createCDEI();
        return m_CDEI;
    }

    public void OnPrefsCdeiChanged() {
        // CDEI neu erstellen, da URL oder Protokoll ge�ndert wurden
        createCDEI();
        // alle Views aktualisieren (panels, Actions)
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.prefs.PreferenceChangeListener#preferenceChange(java.util.prefs.PreferenceChangeEvent)
     */
    public void preferenceChange(PreferenceChangeEvent arg0) {
        OnPrefsCdeiChanged();
    }
    
    public static void test(){
        WapCDEI t= new WapCDEI();
        JCDEI_XmlRpc jcdei= t.getCDEI();
        jcdei.setAuthentication();
        System.out.println(jcdei.getConnected());
        StringHolder sh= new StringHolder();
        try{
            jcdei.getCdeiPrefs().setHasDebugMode(true);
        } catch (BackingStoreException e1){
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try{
            jcdei.Login(1, "hello", "hello", sh);
            System.out.println("Back from login");
            JCDEI_tContainerData cd= new JCDEI_XmlRpc_tContainerData();
            System.out.println("Tryng to get root");
            jcdei.ContainerGetRoot(cd);
            System.out.println("ID: " + cd.getID() + ":" + cd.getDescription());
            Vector containers= cd.getContainers();

            for (int i= 0; i < containers.size(); i++){
                Integer c= (Integer) containers.get(i);
                JCDEI_tContainerData cd1= new JCDEI_XmlRpc_tContainerData();
                jcdei.ContainerGet(c.intValue(), cd1);
                System.out.println(cd1.getID() + ":" + cd1.getName());
                Vector jbags= cd1.getJbags();
                for (int j= 0; j < jbags.size(); j++){
                    Integer id= (Integer) jbags.get(j);
                    JCDEI_XmlRpc_tJbagData jd= new JCDEI_XmlRpc_tJbagData();
                    jcdei.JbagGet(id.intValue(), jd);
                    System.out.println("JBag:" + jd.getName() + " "
                            + jd.getID());
//                    FileWriter fw= new FileWriter(new File("C:\\temp", jd.getName()));
//                    fw.write(jd.getSource());
//                    fw.close();
                    Vector resources= jd.getResources();
                    for (int m= 0; m < resources.size(); m++){
                        JCDEI_XmlRpc_tJbagResource o= (JCDEI_XmlRpc_tJbagResource) resources
                                .get(m);
                        System.out.println(o.getName() + ":" + o.getType());
                        
                        byte[] b= o.getContent(); FileOutputStream f= new
                        FileOutputStream(new File( "C:\\temp", o.getName()));
                        f.write(b); f.close();
                        
                    }
                }
            }

        } catch (MalformedURLException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JCdeiException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void sendJbag() {
        WapCDEI t= new WapCDEI();
        JCDEI_XmlRpc jcdei= t.getCDEI();
        jcdei.setAuthentication();
        System.out.println(jcdei.getConnected());
        StringHolder sh= new StringHolder();
        try{
            jcdei.getCdeiPrefs().setHasDebugMode(true);
        } catch (BackingStoreException e1){
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try{
            jcdei.Login(1, "hello", "hello", sh);

//            jcdei.JbagRemove(98);
            JCDEI_XmlRpc_tJbagData jd= new JCDEI_XmlRpc_tJbagData();
            jd.setSource(new String(WapFactory.getJBag()));
            System.out.println(WapFactory.getJBag());
            jd.setName("Hellotest");
            jd.setParent(312);
            jd.setID(0);
            jd.setDescription("Delete at will");
            
            Vector resources= new Vector();
            
            File dir= new File("jbag");
            File[] files= dir.listFiles();
            for (int m= 0; m < files.length; m++){
                if (!files[m].isFile()){
                    continue;
                }
                JCDEI_XmlRpc_tJbagResource jr= new JCDEI_XmlRpc_tJbagResource();
                jr.setName(files[m].getName());
                jr.setType("rasterpic_png");

                FileInputStream f= new FileInputStream(files[m]);
                System.out.println(f.available()+" disponible en "+files[m].getName());
                byte[] input= new byte[f.available()];
                f.read(input);
                jr.setContent(input);
                f.close();
                resources.add(jr);
            }
            
            Map images= WapFactory.getImages();
            
            for (Iterator it= images.keySet().iterator(); it.hasNext();){
                String key= (String) it.next();
                JCDEI_XmlRpc_tJbagResource jr= new JCDEI_XmlRpc_tJbagResource();
                jr.setName(key+".png");
                jr.setType("rasterpic_png");

                byte[] content= (byte[]) images.get(key);
                jr.setContent(content);
                System.out.println("Imagen "+key+" : "+content.length);
                resources.add(jr);
                
//                FileOutputStream f= new FileOutputStream(new File( "C:\\temp", key+".png"));
//                f.write(content); 
//                f.close(); 
            }
                       
            jd.setResources(resources);
            IntHolder ih= new IntHolder();
            jcdei.JbagAdd(jd, ih);
            System.out.println("Nuevo JBag "+ih+"("+ih.value+")");

        } catch (MalformedURLException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JCdeiException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public static String updateJbag(int id) {
        WapCDEI t= new WapCDEI();
        JCDEI_XmlRpc jcdei= t.getCDEI();
        Map res= new HashMap();

        jcdei.setAuthentication();

        System.out.println(jcdei.getConnected());
        StringHolder sh= new StringHolder();
        try{
            jcdei.Login(1, "hello", "hello", sh);
                        
            JCDEI_XmlRpc_tJbagData jdo= new JCDEI_XmlRpc_tJbagData();
            jcdei.JbagGet(id, jdo);
            Vector resources= jdo.getResources();
            for (int m= 0; m < resources.size(); m++){
                JCDEI_XmlRpc_tJbagResource o= (JCDEI_XmlRpc_tJbagResource) resources
                        .get(m);
                res.put(o.getName(), o);
            }
            System.out.println(resources.size()+" imagenes en JBag:" + jdo.getName() + " " + jdo.getID());
                        
            String source= WapFactory.getJBag();
                        
            Map nres= new HashMap();                       
            Map images= WapFactory.getImages();

            for (Iterator it= images.keySet().iterator(); it.hasNext();){
                String key= (String) it.next();
                JCDEI_XmlRpc_tJbagResource jr= new JCDEI_XmlRpc_tJbagResource();
                jr.setName(key+".png");
                jr.setType("rasterpic_png");

                byte[] content= (byte[]) images.get(key);
                jr.setContent(content);
                System.out.println("Imagen "+key+" : "+content.length);
                nres.put(key+".png",jr);
            }

            Vector cresources= new Vector();
            // Added, unchanged or modified depend on new vs old
            for (Iterator it= nres.keySet().iterator(); it.hasNext();){
                char change;
                String name= (String) it.next();
                JCDEI_XmlRpc_tJbagResource jrn= (JCDEI_XmlRpc_tJbagResource) nres.get(name);
                if (res.containsKey(name)){
                    JCDEI_XmlRpc_tJbagResource jro= (JCDEI_XmlRpc_tJbagResource) res.get(name);
                    change= (Arrays.equals(jro.getContent(), jrn.getContent())) 
                    	? JCDEI_XmlRpc_tJbagResourceChange.CHANGE_TYPE_UNCHANGED
                    	: JCDEI_XmlRpc_tJbagResourceChange.CHANGE_TYPE_MODIFIED;
 
                }
                else{
                    change= JCDEI_XmlRpc_tJbagResourceChange.CHANGE_TYPE_ADDED;
                }
                JCDEI_XmlRpc_tJbagResourceChange ch= new JCDEI_XmlRpc_tJbagResourceChange();
                if (change == JCDEI_XmlRpc_tJbagResourceChange.CHANGE_TYPE_UNCHANGED){
                    jrn.setContent(" ".getBytes());
                }
                ch.assign(jrn);
                ch.setChangeType(change);
                cresources.add(ch);
            }
            
            String[] icons= {"jl_icon.png", "jl_top.png", "jl_bottom.png",
                    "splash.png", "update_icon.png", "icon.png" };
            
            Set iconset= new HashSet();
            iconset.addAll(Arrays.asList(icons));
            
            // Deleted are old vs new
            for (Iterator it= res.keySet().iterator(); it.hasNext();){
                String name= (String) it.next();
                JCDEI_XmlRpc_tJbagResource jro= (JCDEI_XmlRpc_tJbagResource) res.get(name);
                JCDEI_XmlRpc_tJbagResourceChange ch= new JCDEI_XmlRpc_tJbagResourceChange();
                jro.setContent(" ".getBytes());
                ch.assign(jro);
                if (iconset.contains(name)){
                    ch.setChangeType(JCDEI_XmlRpc_tJbagResourceChange.CHANGE_TYPE_UNCHANGED);
                }
                else if (!nres.containsKey(name)){
                    ch.setChangeType(JCDEI_XmlRpc_tJbagResourceChange.CHANGE_TYPE_REMOVED);
                }
                else{
                    continue;
                }
                cresources.add(ch);
            }

            for (int i= 0; i< cresources.size(); i++){
                JCDEI_XmlRpc_tJbagResourceChange ch= (JCDEI_XmlRpc_tJbagResourceChange) cresources.get(i);                
                System.out.println(ch.getName()+":"+ch.getChangeType());
            }
            
            jcdei.JbagUpdateContent(id, source , cresources);
            System.out.println("updated");
            
//            JCDEI_XmlRpc_tJbagData jd= new JCDEI_XmlRpc_tJbagData();
//            jcdei.JbagGet(id, jd);
//            showJBag(jd);
        } catch (MalformedURLException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JCdeiException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "ok";
    }
    
    public static void showJBag(JCDEI_XmlRpc_tJbagData jd){
        System.out.println("JBag:" + jd.getName() + " "
                + jd.getID());
        System.out.println(jd.getSource());
        Vector resources= jd.getResources();
        for (int m= 0; m < resources.size(); m++){
            JCDEI_XmlRpc_tJbagResource o= (JCDEI_XmlRpc_tJbagResource) resources
                    .get(m);
            System.out.println(o.getName() + ":" + o.getType());
        }
    }

    public void update(Observable arg0, Object arg1) {
        if (arg0 == getLoginStatus()){
            try{
                getCdeiPrefs().setPassword(getLoginStatus().getPassword());
                getCdeiPrefs().setUserName(getLoginStatus().getUserName());
                getCdeiPrefs().setSessionID(getLoginStatus().getSessionID());

                getCDEI().updateAuthentication();
            } catch (Exception e){
                ;//e.
            }
        }
    }
    
    public static void main(String[] args) {

            //        test();
//      sendJbag();
      updateJbag(322);
//            System.out.println(WapFactory.getJBag());

  }
}
