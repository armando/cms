package com.hellomagazine.ed.newvision;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


/*
 * Created on 22-oct-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author armando
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Sms {
    private Calendar c= null;
    private String message;
    private int max= 160;
         
    public Sms(){
        c= Calendar.getInstance();
//        System.out.println("Creando mensaje");
    }
        
    public String send() throws Exception{
        String result= null;
        if (c.getTime().after(new Date())){            
            result= NVHttpClient.send(this);
        }
        else{
            result= "tarde";
        }
        return result;
    }
      
    /**
     * @return Returns the day.
     */
    public String getDay() {
        return String.valueOf(c.get(Calendar.DAY_OF_MONTH));
    }
    
    public String getDay(TimeZone t, Locale l) {
        Calendar locale= Calendar.getInstance(t, l);
        locale.setTime(c.getTime());
        return String.valueOf(locale.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * @param day The day to set.
     */
    public void setDay(String _day) {
        c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(_day));
    }
    /**
     * @return Returns the hour.
     */
    public String getHour() {
        return String.valueOf(c.get(Calendar.HOUR_OF_DAY)-1); // UK
    }
    
    public String getHour(TimeZone t, Locale l) {
        Calendar locale= Calendar.getInstance(t, Locale.UK);
        locale.setTime(c.getTime());
        return String.valueOf(locale.get(Calendar.HOUR_OF_DAY));
    }
    
    /**
     * @param hour The hour to set.
     */
    public void setHour(String _hour) {
        c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(_hour));
    }
    /**
     * @return Returns the message.
     */
    public String getMessage(){
        return message.length() <= max ? message : message.substring(0, max);
    }
    /**
     * @param message The message to set.
     */
    public void setMessage(String _message) {
        this.message= _message;
    }
    /**
     * @return Returns the minute.
     */
    public String getMinute() {
        return String.valueOf(c.get(Calendar.DAY_OF_MONTH));
    }
    /**
     * @param minute The minute to set.
     */
    public void setMinute(String _minute) {
        c.set(Calendar.MINUTE, Integer.parseInt(_minute));
    }
    /**
     * @return Returns the month.
     */
    public String getMonth() {
        return String.valueOf(c.get(Calendar.MONTH)+1);
    }

    public String getMonth(TimeZone t, Locale l) {
        Calendar locale= Calendar.getInstance(t, Locale.UK);
        locale.setTime(c.getTime());
        return String.valueOf(locale.get(Calendar.MONTH)+1);
    }

    /**
     * @param month The month to set.
     */
    public void setMonth(String _month) {
        c.set(Calendar.MONTH, Integer.parseInt(_month));
    }
    /**
     * @return Returns the year.
     */
    public String getYear() {
        return String.valueOf(c.get(Calendar.YEAR));
    }

    public String getYear(TimeZone t, Locale l) {
        Calendar locale= Calendar.getInstance(t, Locale.UK);
        locale.setTime(c.getTime());
        return String.valueOf(locale.get(Calendar.YEAR));
    }

    /**
     * @param year The year to set.
     */
    public void setYear(String _year) {
        c.set(Calendar.YEAR, Integer.parseInt(_year));
    }
    
    public String toString(){
        return message+" at "+c.getTime();
    }
    /**
     * @return Returns the max.
     */
    public int getMax() {
        return max;
    }
    /**
     * @param max The max to set.
     */
    public void setMax(int m) {
        this.max= m;
    }

    /**
     * @param sending
     */
    public void setDate(Date sending) {
        c.setTime(sending);
    }    
}
