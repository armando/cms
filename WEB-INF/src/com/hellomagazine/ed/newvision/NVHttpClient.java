package com.hellomagazine.ed.newvision;

import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

import com.hellomagazine.ed.ticker.TickerForm;

/*
 * Created on 21-oct-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author Armando Ramos <armando@hola.com>
 * 
 * Based on sample code from commons_httpclient
 */
public class NVHttpClient {
    static final String LOGON_SITE= "newvisions.textgenerator.co.uk";
    static final TimeZone tz= TimeZone.getTimeZone("Europe/London");
    static final Locale l= Locale.UK;
    static final int LOGON_PORT= 80;

    public NVHttpClient() {
        super();
    }

    public static String send(Sms mess) throws Exception {
        int code= 200;
        HttpClient client= new HttpClient();
        client.getHostConfiguration().setHost(LOGON_SITE, LOGON_PORT, "http");
//        client.getHostConfiguration().setHost("localhost", 8080, "http");
        GetMethod authget= new GetMethod("/index.php");
        client.executeMethod(authget);
        authget.releaseConnection();

        PostMethod authpost= new PostMethod("/index.php");
        NameValuePair userid= new NameValuePair("username", "hello");
        NameValuePair password= new NameValuePair("password", "hellomag");
        authpost.setRequestBody(new NameValuePair[] { userid, password });
//        authpost.setFollowRedirects(true);
        client.executeMethod(authpost);
        authpost.releaseConnection();
        
        int statuscode= authpost.getStatusCode();
        if ((statuscode == HttpStatus.SC_MOVED_TEMPORARILY)
                || (statuscode == HttpStatus.SC_MOVED_PERMANENTLY)
                || (statuscode == HttpStatus.SC_SEE_OTHER)
                || (statuscode == HttpStatus.SC_TEMPORARY_REDIRECT)){
            Header header= authpost.getResponseHeader("location");
            if (header != null){
                String newuri= header.getValue();
                if ((newuri == null) || (newuri.equals(""))){
                    newuri= "/";
                }
                //                System.out.println("Redirect target: " + newuri);
                GetMethod redirect= new GetMethod(newuri);

                client.executeMethod(redirect);
                //                System.out.println("Redirect: "
                //                        + redirect.getStatusLine().toString());
                // release any connection resources used by the method
                redirect.releaseConnection();
            } else{
                System.out.println("Invalid redirect");
                return ("failed");
            }
        }

        authpost= new PostMethod(
              "/index.php?module=alerts&page=nm&id=21525");
                
        authpost.addRequestHeader("content-type","application/x-www-form-urlencoded; charset=UTF-8");
        
        NameValuePair message= new NameValuePair("message", mess.getMessage());
        NameValuePair day= new NameValuePair("send_time_day", mess.getDay(tz, l));
        NameValuePair month= new NameValuePair("send_time_month", mess.getMonth(tz, l));
        NameValuePair year= new NameValuePair("send_time_year", mess.getYear(tz, l));
        NameValuePair hour= new NameValuePair("send_time_hour", mess.getHour(tz, l));
        NameValuePair minute= new NameValuePair("send_time_minute", mess.getMinute());
        NameValuePair submitted= new NameValuePair("submittedForm", "YES");
        NameValuePair premium= new NameValuePair("premium", "1");
        NameValuePair setsend= new NameValuePair("set_send_time", "2");
        authpost.setRequestBody(new NameValuePair[] { message, day, month,
                year, hour, minute, submitted, premium, setsend });
        
        client.executeMethod(authpost);
        System.out.println(authpost.getRequestCharSet());
        code= authpost.getStatusCode();
        
        System.out.println("Enviando: " + mess.getMessage()+"..." + authpost.getStatusLine());

        // release any connection resources used by the method
        authpost.releaseConnection();

        GetMethod logoutget= new GetMethod("/index.php?module=logout");

        client.executeMethod(logoutget);
        //        System.out.println("Logout get: "
        //                + logoutget.getStatusLine().toString());
        // release any connection resources used by the method
        logoutget.releaseConnection();
        return ( (code==200)?"ok":"error");
    }

    public static void main(String[] args) throws Exception {
        Sms message= TickerForm.getSMS();
        try{
            message.send();

        } catch (Exception e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}