/*
 * Created on 1-Jun-2005
 *
 * Generic form for a list of objects
 * 
 */
package com.hellomagazine.ed.ticker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

import com.hellomagazine.newsticker.Headline;
import com.hellomagazine.newsticker.Ticker;
import com.hola.Elemento;

/**
 * @author Armando Ramos <armando@hola.com>
 * 
 */
public class ListForm {
    private static final long DAY= 24L * 60L * 60L * 1000L;
    private Ticker _work= null, _context= null;
    private DataModel _elementos= null;
    private Elemento[] _wrapped= null;
    private static final String BASE= "headlines";
    private String _workName= null;
    private String _elementoName= null;
    private Method countMethod= null, getElementMethod= null;
    private Method clearElementsMethod= null, setElementsMethod= null;
    private Method removeElementMethod= null, getElementsMethod= null;
    private Method addElementMethod= null, setElementMethod= null;
    private static String base= "/www/data/hello/newsticker";

    public ListForm() throws SecurityException, NoSuchMethodException, ClassNotFoundException {
        try {
            javax.naming.Context env=
                (javax.naming.Context) new InitialContext().lookup(
                    "java:comp/env");
            base= (String) env.lookup("base");
        }
        catch (NamingException e) {
            e.printStackTrace();
        }
        _context= loadLista(BASE);
        setWorkName(BASE);
        
        Class[] args={Integer.class};
        Class[] args2={Array.class};
        
        Class elemento= Class.forName(_elementoName);
        Class[] args3= {elemento};
        Class[] args4= {Integer.class, elemento};
        countMethod= _work.getClass().getDeclaredMethod("get"+_elementoName+"Count", null);
        getElementMethod= _work.getClass().getDeclaredMethod("get"+_elementoName, args);
        getElementsMethod= _work.getClass().getDeclaredMethod("get"+_elementoName, null);
        clearElementsMethod= _work.getClass().getDeclaredMethod("clear"+_elementoName, null);
        setElementMethod= _work.getClass().getDeclaredMethod("set"+_elementoName, args4);
        setElementsMethod= _work.getClass().getDeclaredMethod("set"+_elementoName, args2);
        addElementMethod= _work.getClass().getDeclaredMethod("add"+_elementoName, args3);
        removeElementMethod= _work.getClass().getDeclaredMethod("remove"+_elementoName, args3);
    }

    public static Ticker loadLista(String listaName) {
        Ticker loaded= null;
        try {
            File f= new File(base, listaName+ ".xml");

            FileInputStream fr= null;
            if (f.canRead()) {
                fr= new FileInputStream(f);
            }
            
           Unmarshaller tickunm= new Unmarshaller(Ticker.class);
           tickunm.setWhitespacePreserve(true);
//            loaded= (fr == null) ? new Ticker() : (Ticker) Ticker.unmarshal(new InputStreamReader(fr, "UTF8"));
          loaded= (fr == null) ? new Ticker() : (Ticker) tickunm.unmarshal(new InputStreamReader(fr, "UTF8"));
        } catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return loaded;
    }

    public static void saveLista(String listaName, Ticker lista) {
    	// Static saver for convenience, cause of confusion as the instance saver should use it
    	// instead of doing itself (next factorization)
        try {
            System.out.println("Saving " + listaName + " to store...");
            File f= new File(base, listaName + ".xml");

            FileOutputStream fw= new FileOutputStream(f);
            lista.marshal(new OutputStreamWriter(fw, "UTF8"));
            System.out
                    .println("..." + lista.getHeadlineCount() + " headlines");
            fw.close();
        } catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
        
    public int getOrder(int id, Ticker l) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        int order= -1;
        Integer count= (Integer) countMethod.invoke(l, null);
        for (int i= 0; i < count.intValue(); i++) {
            Object[] args= null;
            args[0]= new Integer(i);
            Elemento h= (Elemento) getElementMethod.invoke(l, args);
            //            System.out.println("Comparando con
            // "+headline.getId().intValue());
            if (h.getId().intValue() == id) {
                order= i;
                break;
            }
        }
//        System.out.println("order = " + order);
        return order;
    }

    public int getOrder(int id) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        return getOrder(id, _work);
    }

    public List getOrders() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Integer num= (Integer) countMethod.invoke(_work, null);
        List orders= new ArrayList();
        for (int i= 0; i <= num.intValue(); i++) {
            orders.add(new SelectItem(new Integer(i), String.valueOf(i + 1),
                    null));
        }
        return orders;
    }

    public Integer getElementCount() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        return (Integer) countMethod.invoke(_work, null);
    }

    public int getMaxID() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        int max= 0;
        Integer count= (Integer) countMethod.invoke(_work, null);
        Object[] args= null;
        for (int i= 0; i < count.intValue(); i++) {
            args[0]= new Integer(i);
            Elemento h= (Elemento) getElementMethod.invoke(_work, args);
            if (h.getId().intValue() > max) {
                max= h.getId().intValue();
            }
        }
        return max;
    }

    public Elemento getElemento(int id, Ticker lista) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Headline h= null;
        int order= getOrder(id, lista);
        return getElementoByOrder(order, lista);
    }

    public Elemento getElementoByOrder(int order, Ticker lista) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Elemento h= null;
        Integer count= (Integer)countMethod.invoke(_work, null);
        //        System.out.println("Pidiendo "+ order + " de " +count + "
        // headlines");
        
        if (order >= 0 && order < count.intValue()) {
            Object[] args= {new Integer(order)};
            h= (Elemento) getElementMethod.invoke(_work, args);
        }
        return h;
    }
    
    public DataModel getElementos() {
        return _elementos;
    }

    public void setElementos(DataModel elementos) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        clearElementsMethod.invoke(_work, null);
        System.out.println("Actualizando headlines en " + _workName);
        Object[] args= null;
        args[0]= elementos.getWrappedData();
        setElementsMethod.invoke(_work, args);
    }

    public void deleteElemento(int id) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        int order= getOrder(id, _work);
        System.out.println("Deleting " + id);
        Object[] args= null;
        if (order >= 0) {
            args[0]= new Integer(order);
            Elemento e= (Elemento) getElementMethod.invoke(_work, args);
            args[0]= e;
            removeElementMethod.invoke(_work, args);
            _elementos.setWrappedData(getElementsMethod.invoke(_work, null));
        }
        save();
    }

    public void saveElement(Elemento h, int id, int ord) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        int order= getOrder(id, _work);
        boolean inRange= (ord >= 0 && ord < getElementCount().intValue());
//        System.out.println("Saving " + h.getText() + " con id " + id
//                + " en pos " + ord + " order=" + order);
        Object[] args= null;
        if (order < 0) {
            if (inRange) {
                args[0]= new Integer(ord);
                args[1]= h;
                addElementMethod.invoke(_work, args);
//                System.out.println("Adding in ord " + ord);
            } else {
                args[0]= h;
                addElementMethod.invoke(_work, args);
//                System.out.println("Adding at the end ");
            }
        } else {
            args[0]= new Integer(ord);
            args[1]= h;
            setElementMethod.invoke(_work, args);
//            System.out.println("Grabado " + id + " en pos " + order + ": "
//                    + h.getType());
            if (order != ord && inRange) {
                //				System.out.println("Pasando "+order+" a "+ord);
                reorderHeadlines(order, ord);
            }
        }
        _elementos.setWrappedData(getElementsMethod.invoke(_work, null));
        save();
    }

    /**
     * @param order
     * @param neword
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    private void reorderHeadlines(int order, int neword) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        if (order > neword) {
            for (int i= order; i > neword; i--) {
                swap(i, i - 1);
            }
        } else {
            for (int i= order; i < neword; i++) {
                swap(i, i + 1);
            }
        }
    }

    public String save() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        try {
            _work.setId("id");
            Elemento[] wrapped= (Elemento[]) _elementos.getWrappedData();
//            System.out.println("Sorting...");
            Arrays.sort(wrapped, new ElementComparator());
            for (int i= 0, n= wrapped.length; i < n; i++) {
                Elemento h= wrapped[i];
                h.setOrder(new Integer(i));
//                System.out.println(i + ": ID= " + h.getId() + ": order= "
//                        + h.getOrder());
            }
            Object[] args= null;
            args[0]= wrapped;
            setElementsMethod.invoke(_work, args);
            FileOutputStream fo= new FileOutputStream(new File(base,_workName + ".xml"));
            _work.marshal(new OutputStreamWriter(fo, "UTF8"));
            fo.close();
//            System.out.println("Grabando " + _workName + " en " + base);
        } catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "go_back";
    }

    public void move(int _id, String dir) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        int order= getOrder(_id, _work);
        int max= getElementCount().intValue();
        int inc= dir.equals("up") ? -1 : 1;
        int dest= order + inc;
        //		System.out.println("Moviendo "+_id+" de "+order+" a "+dest);
        if (dest >= 0 && dest < max) {
            swap(dest, order);
            save();
        }
        /*
         * for (int i=0; i <headlines.size(); i++){ Headline h= (Headline)
         * headlines.get(i); System.out.print(h.getId()+","); }
         * System.out.println("");
         */
    }

    private void swap(int orig, int dest) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Object[] args= null;
        args[0]= new Integer(orig);
        Elemento h1= (Elemento) getElementMethod.invoke(_work, args);
        args[0]= new Integer(dest);
        Elemento h2= (Elemento) getElementMethod.invoke(_work, args);
        h1.setOrder(new Integer(dest));
        h2.setOrder(new Integer(orig));
        args[0]= new Integer(dest);
        args[1]= h1;
        setElementMethod.invoke(_work, args);
        args[0]= new Integer(orig);
        args[1]= h2;
        setElementMethod.invoke(_work, args);
    }

    /**
     * @return
     */
    public String getWorkName() {
        return _workName;
    }

    /**
     * @param string
     */
    public void setWorkName(String string) {
        if (string.equals(_workName)) {
            return;
        }
        _workName= string;
        if (getIsContext()) {
            _work= _context;
        } else {
            _work= loadLista(_workName);
            if (_work.getHeadlineCount() == 0) {
                System.out.println("Creando nuevo ticker para " + _workName);
                /*
                 * 
                 * for (int i= 0; i < context.getHeadlineCount(); i++) {
                 * Headline h= context.getHeadline(i); Headline wh= new
                 * Headline(); wh.setId(h.getId()); wh.setOrder(h.getOrder());
                 * wh.setText(h.getText()); work.addHeadline(wh); }
                 */
            }
        }
        _elementos= new ArrayDataModel(_work.getHeadline());
    }

    public boolean getIsContext() {
        return _workName.equals(BASE);
    }

    public boolean getHasTypes(){
    	return _workName.equals(BASE) || _workName.equals("ticker");
    }
    
    public Ticker getContext() {
        return _context;
    }

    public Ticker getWork() {
        return _work;
    }

    public String expire() {
        Map params= FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap();
        String ex= (String) params.get("exp:expire");
        System.out.println("Expiring older than " + ex + " days");
        try {
            int days= Integer.parseInt(ex);
            for (int i= 0, n= _work.getHeadlineCount(); i < n; i++) {
                Headline h= _work.getHeadline(i);
                Date d= new Date();
                long diff= d.getTime() - h.getCreated().getTime();
                if (diff > DAY * days) {
                    System.out.println("Expiring " + h.getURL() + ":" + diff
                            / 3600000L + " hours");
                }
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return null;
    }
    
    public static void main(String[] args){
        ListForm tf= null;
        try{
            tf= new ListForm();
        } catch (SecurityException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchMethodException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}