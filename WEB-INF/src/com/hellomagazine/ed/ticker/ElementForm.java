package com.hellomagazine.ed.ticker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.hellomagazine.newsticker.Headline;
import com.hellomagazine.newsticker.Ticker;
import com.mindprod.base64.Base64;

/**
 * @author Armando Ramos <armando@hola.com>
 */
public class ElementForm {
    private Integer id;
    private int _id, _order= -1;
    private String _text;
    private String _URL;
    private String _type;
    private String _summary;
    private List _images = new ArrayList();
    private Date _live;

    private static List types= new ArrayList();
    static {
        types.add(new SelectItem("text/html", "Black Underlined", null));
        types.add(new SelectItem("text/hellor", "Red", null));
        types.add(new SelectItem("text/hellorb", "Red Boldface", null));
    }

    public int getId() {
        return _id;
    }

    public String add() {
        Ticker t= null;
        if (getTicker().getIsContext() || _id == 0) {
            _id= getTicker().getMaxID() + 1;
            t= getTicker().getWork();
        } else {
            t= getTicker().getContext();
        }
        _order= getTicker().getHeadlineCount();
        //System.out.println("Add: Setting _id"+_id+" context
        // "+getTicker().getIsContext());
        Headline headline= getTicker().getHeadline(_id, t);
        if (headline != null) {
            _text = headline.getText();
            _URL = headline.getURL();
            _type = headline.getType();
            _summary = headline.getSummary().replace('|','\n');
            _images = Arrays.asList(headline.getImage());
            _live = (headline.getLive() == null)?new Date():headline.getLive();
            save();
        } else {
            //			System.out.println("headline null");
        }
        return "go_headline";
    }

    public void setId(int newId) {
        _id= newId;
        if (_id <= 0 && getTicker().getIsContext()) {
            _id= getTicker().getMaxID() + 1;
            _order= getTicker().getHeadlineCount();
        }
//        System.out.println("Setting _id=" + _id + " context "
//                + getTicker().getIsContext());
        Headline headline= getTicker().getHeadline(_id, getTicker().getWork());
        if (headline != null) {
            _text= headline.getText();
            _URL= headline.getURL();
            _type= headline.getType();
            _summary= headline.getSummary() != null ? headline.getSummary().replace('|','\n') : null;
            _live = headline.getLive();
            _images= new ArrayList(Arrays.asList(headline.getImage()));
       }
    }

    private Headline getHeadline() {
//        System.out.println("Creating headline from form " + _id);
        Headline h= new Headline();
        h.setId(new Integer(_id));
        h.setOrder(new Integer(_order));
        h.setText(_text);
//        h.setSummary(_summary.replace('\n','|'));
        h.setSummary(_summary);
        h.setURL(_URL);
        h.setType(_type);
        h.setCreated(new Date());
        h.setLive(_live);
        
        String[] img = new String[_images.size()];
        for (int i = 0; i < _images.size(); i++)
        {
            img[i] = (String)_images.get(i);
        }
        h.setImage(img);
        return h;
    }

    public String save() {
        Headline h= getHeadline();
        getTicker().saveHeadline(h, _id, _order);
//        System.out.println("Grabando " + _id + " en posicion " + _order);
        return "ok_next";
    }

    public String delete() {
        getTicker().deleteHeadline(_id);
//        System.out.println("Borrando "+_id);
        return "ok_next";
    }

    private TickerForm getTicker() {
        Object obj= FacesContext.getCurrentInstance().getApplication()
                .getVariableResolver().resolveVariable(
                        FacesContext.getCurrentInstance(), "tickerForm");
        return (TickerForm) obj;
    }

    public String up() {
        getTicker().move(_id, "up");
        return "ok";
    }

    public String down() {
        getTicker().move(_id, "down");
        return "ok";
    }

    /**
     * @return
     */
    public String getText() {
        return _text;
    }

    

    public String getImage1Name()
    {
        TickerForm tickerForm = getTicker();
        return "/" + tickerForm.getWorkName() + "/" + _order + "/0.jpg";
    }

    public UploadedFile getImage1(){return null;}
    public void setImage1(UploadedFile upFile) {
        try
        {
            Base64 encoder= new Base64();
//            System.out.println(_images);
            _images.add(0, encoder.encode(upFile.getBytes()));
            save();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void deleteImage1()
    {
        if (_images.size() >= 1)
        {
            _images.remove(0);
            save();
        }
    }
    
    public UploadedFile getImage2(){return null;}
    public String getImage2Name()
    {
        TickerForm tickerForm = getTicker();
        return "/" + tickerForm.getWorkName() + "/" + _order + "/1.jpg";
    }
    
    public void setImage2(UploadedFile upFile) {
//        System.out.println("WEEEEE");
        try
        {
            Base64 encoder= new Base64();
            if (_images.size() >= 2)
            {
                _images.remove(1);
            }
            _images.add(1, encoder.encode(upFile.getBytes()));
            save();            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void deleteImage2()
    {
        if (_images.size() >= 2)
        {
            _images.remove(1);
            save();
        }
    }
    
    public boolean isImage1Set()
    {
        return (_images.size() >= 1);
    }

    public boolean isImage2Set()
    {
        return (_images.size() >= 2);
    }

    /**
     * @return
     */
    public String getURL() {
        return _URL;
    }

    /**
     * @param string
     */
    public void setText(String string) {
        _text= string;
    }

    /**
     * @param string
     */
    public void setURL(String string) {
        _URL= string;
    }

    /**
     * @return
     */
    public String getType() {
        return _type;
    }

    /**
     * @param string
     */
    public void setType(String string) {
        _type= string;
    }

    public List getTypes() {
        return types;
    }

    /**
     * @return
     */
    public String getSummary() {
        return _summary;
    }

    /**
     * @param string
     */
    public void setSummary(String string) {
        _summary= string.replaceAll("<br>","");
        _summary= _summary.replaceAll("<br/>","");
//        System.out.println("newline en: "+ string.indexOf('\n'));
    }

    /**
     * @return
     */
    public int getOrder() {
        if (_order == -1) {
            _order= getTicker().getOrder(_id);
        }
        return _order;
    }

    /**
     * @param i
     */
    public void setOrder(int in) {
        _order= in;
        save();
    }
    
    public Date getLiveDate()
    {
        return _live;
    }
    
    public String getLive(){
        return _live.toGMTString();
    }
    
    public void setLiveDate(Date live)
    {
        _live = live;
        save();
    }

    
   
}