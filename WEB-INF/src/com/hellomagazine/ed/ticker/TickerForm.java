/*
 * Created on 31-may-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hellomagazine.ed.ticker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

import com.hellomagazine.ed.jbag.CDEI;
import com.hellomagazine.ed.jbag.WapCDEI;
import com.hellomagazine.ed.newvision.Sms;
import com.hellomagazine.newsticker.Headline;
import com.hellomagazine.newsticker.Ticker;
import com.hellomagazine.util.ftp.FtpC;
import com.hola.ed.webdav.Sender;

/**
 * @author armando
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TickerForm {
    private static final long DAY= 24L * 60L * 60L * 1000L;
    private Ticker _work= null, _context= null;
    private DataModel _headlines= null;
    private Headline[] _wrapped= null;
    private static final String BASE= "headlines";
    private String _workName= null;
    private static String base= "/www/data/hello/newsticker";

    public TickerForm() {
/*
        try {
            javax.naming.Context env=
                (javax.naming.Context) new InitialContext().lookup(
                    "java:comp/env");
            base= (String) env.lookup("base") + "newsticker";
        }
        catch (NamingException e) {
            e.printStackTrace();
        }
        */
        
        _context= loadTicker(BASE);
        setWorkName(BASE);
    }

    public static Ticker loadTicker(String tickerName) {
        Ticker loaded= null;
        try {
            File f= new File(base, tickerName+ ".xml");
            System.out.println("Fichero: "+f.toURI());

            FileInputStream fr= null;
            if (f.canRead()) {
                fr= new FileInputStream(f);
            }
            
           Unmarshaller tickunm= new Unmarshaller(Ticker.class);
           tickunm.setWhitespacePreserve(true);
//            loaded= (fr == null) ? new Ticker() : (Ticker) Ticker.unmarshal(new InputStreamReader(fr, "UTF-8"));
          loaded= (fr == null) ? new Ticker() : (Ticker) tickunm.unmarshal(new InputStreamReader(fr, "UTF-8"));
        } catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        System.out.println("Devolviendo "+loaded.getId()+"-"+loaded.getHeadlineCount()+" headlines");
        return loaded;
    }

    public static void saveTicker(String tickerName, Ticker ticker) {
    	// Static saver for convenience, cause of confusion as the instance saver should use it
    	// instead of doing itself (next factorization)
        try {
            System.out.println("Saving " + tickerName + " to store...");
            File f= new File(base, tickerName + ".xml");

            FileOutputStream fw= new FileOutputStream(f);
            ticker.marshal(new OutputStreamWriter(fw, "UTF-8"));
            System.out
                    .println("..." + ticker.getHeadlineCount() + " headlines");
            fw.close();
        } catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
        
    public int getOrder(int id, Ticker t) {
        int order= -1;
        //        System.out.println("Buscando orden de "+id);
        int count= t.getHeadlineCount();
        for (int i= 0; i < count; i++) {
            Headline headline= t.getHeadline(i);
            //            System.out.println("Comparando con
            // "+headline.getId().intValue());
            if (headline.getId().intValue() == id) {
                order= i;
                break;
            }
        }
//        System.out.println("order = " + order);
        return order;
    }

    public int getOrder(int id) {
        return getOrder(id, _work);
    }

    public List getOrders() {
        int num= _work.getHeadlineCount();
        List orders= new ArrayList();
        for (int i= 0; i <= num; i++) {
            orders.add(new SelectItem(new Integer(i), String.valueOf(i + 1),
                    null));
        }
        return orders;
    }

    public int getHeadlineCount() {
        return _work.getHeadlineCount();
    }

    public int getMaxID() {
        int max= 0;
        int count= _work.getHeadlineCount();
        for (int i= 0; i < count; i++) {
            Headline h= _work.getHeadline(i);
            if (h.getId().intValue() > max) {
                max= h.getId().intValue();
            }
        }
        return max;
    }

    public Headline getHeadline(int id, Ticker ticker) {
        Headline h= null;
        int order= getOrder(id, ticker);
        return getHeadlineByOrder(order, ticker);
    }

    public Headline getHeadlineByOrder(int order, Ticker ticker) {
        Headline h= null;
        int count= ticker.getHeadlineCount();
        //        System.out.println("Pidiendo "+ order + " de " +count + "
        // headlines");
        if (order >= 0 && order < count) {
            h= ticker.getHeadline(order);
        }
        return h;
    }

    public List getHeadlinesOptions() {
        Set ids= new HashSet();
        for (int i= 0, n= _work.getHeadlineCount(); i < n; i++) {
            Headline h= _work.getHeadline(i);
            ids.add(h.getId());
        }
        List orders= new ArrayList();
        for (int i= 0, n= _context.getHeadlineCount(); i < n; i++) {
            Headline h= _context.getHeadline(i);
            Integer id= h.getId();
            if (!ids.contains(id)) {
                orders
                        .add(new SelectItem(String.valueOf(id), h.getText(),
                                null));
            }
        }
        return orders;
    }

    public boolean getEmptyRepository(){
        List l= this.getHeadlinesOptions();
        return (l.size() == 0);
    }
    
    public DataModel getHeadlines() {
        return _headlines;
    }

    public void setHeadlines(DataModel headlines) {
        _work.clearHeadline();
        System.out.println("Actualizando headlines en " + _workName);
        _work.setHeadline((Headline[]) headlines.getWrappedData());
    }

    public void deleteHeadline(int id) {
        int order= getOrder(id, _work);
        System.out.println("Deleting " + id);
        if (order >= 0) {
            _work.removeHeadline(_work.getHeadline(order));
            _headlines.setWrappedData(_work.getHeadline());
        }
        save();
    }

    public void saveHeadline(Headline h, int id, int ord) {
        int order= getOrder(id, _work);
        boolean inRange= (ord >= 0 && ord < _work.getHeadlineCount());
        System.out.println("Saving " + h.getText() + " con id " + id+ " en pos " + ord + " order=" + order);
        if (order < 0) {
            if (inRange) {
                _work.addHeadline(ord, h);
//                System.out.println("Adding in ord " + ord);
            } else {
                _work.addHeadline(h);
//                System.out.println("Adding at the end ");
            }
        } else {
            _work.setHeadline(order, h);
//            System.out.println("Grabado " + id + " en pos " + order + ": "
//                    + h.getType());
            if (order != ord && inRange) {
                //				System.out.println("Pasando "+order+" a "+ord);
                reorderHeadlines(order, ord);
            }
        }
        _headlines.setWrappedData(_work.getHeadline());
        save();
    }

    /**
     * @param order
     * @param neword
     */
    private void reorderHeadlines(int order, int neword) {
        if (order > neword) {
            for (int i= order; i > neword; i--) {
                swap(i, i - 1);
            }
        } else {
            for (int i= order; i < neword; i++) {
                swap(i, i + 1);
            }
        }
    }

    public String save() {
        try {
            _work.setId("id");
            Headline[] wrapped= (Headline[]) _headlines.getWrappedData();
            System.out.println("Sorting...");
            Arrays.sort(wrapped, new HeadlineComparator());
            for (int i= 0, n= wrapped.length; i < n; i++) {
                Headline h= wrapped[i];
                h.setOrder(new Integer(i));
                System.out.println(i + ": ID= " + h.getId() + ": order= "  + h.getOrder());
            }
            _work.setHeadline(wrapped);
            FileOutputStream fo= new FileOutputStream(new File(base,_workName + ".xml"));
            _work.marshal(new OutputStreamWriter(fo, "UTF-8"));
            fo.close();
            System.out.println("Grabando " + _workName + " en " + base);
        } catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "go_back";
    }

    public void move(int _id, String dir) {
        int order= getOrder(_id, _work);
        int max= _work.getHeadlineCount();
        int inc= dir.equals("up") ? -1 : 1;
        int dest= order + inc;
        //		System.out.println("Moviendo "+_id+" de "+order+" a "+dest);
        if (dest >= 0 && dest < max) {
            swap(dest, order);
            save();
        }
        /*
         * for (int i=0; i <headlines.size(); i++){ Headline h= (Headline)
         * headlines.get(i); System.out.print(h.getId()+","); }
         * System.out.println("");
         */
    }

    private void swap(int orig, int dest) {
        Headline h1= _work.getHeadline(orig);
        Headline h2= _work.getHeadline(dest);
        h1.setOrder(new Integer(dest));
        h2.setOrder(new Integer(orig));
        _work.setHeadline(dest, h1);
        _work.setHeadline(orig, h2);
    }

    /**
     * @return
     */
    public String getWorkName() {
        return _workName;
    }

    /**
     * @param string
     */
    public void setWorkName(String string) {
        if (string.equals(_workName)) {
            return;
        }
        _workName= string;
        if (getIsContext()) {
            _work= _context;
        } else {
            _work= loadTicker(_workName);
            if (_work.getHeadlineCount() == 0) {
                System.out.println("Creando nuevo ticker para " + _workName);
                /*
                 * 
                 * for (int i= 0; i < context.getHeadlineCount(); i++) {
                 * Headline h= context.getHeadline(i); Headline wh= new
                 * Headline(); wh.setId(h.getId()); wh.setOrder(h.getOrder());
                 * wh.setText(h.getText()); work.addHeadline(wh); }
                 */
            }
        }
        _headlines= new ArrayDataModel(_work.getHeadline());
    }

    public boolean getIsContext() {
        return _workName.equals(BASE);
    }

    public boolean getHasTypes(){
    	return _workName.equals(BASE) || _workName.equals("ticker");
    }
    
    public Ticker getContext() {
        return _context;
    }

    public Ticker getWork() {
        return _work;
    }

    public String expire() {
        Map params= FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap();
        String ex= (String) params.get("exp:expire");
        System.out.println("Expiring older than " + ex + " days");
        try {
            int days= Integer.parseInt(ex);
            for (int i= 0, n= _work.getHeadlineCount(); i < n; i++) {
                Headline h= _work.getHeadline(i);
                Date d= new Date();
                long diff= d.getTime() - h.getCreated().getTime();
                if (diff > DAY * days) {
                    System.out.println("Expiring " + h.getURL() + ":" + diff
                            / 3600000L + " hours");
                }
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return null;
    }

    /**
     * @return
     */
    public static Map loadContext() {
        TickerForm t= new TickerForm();
        Headline[] heads= t.getContext().getHeadline();
        Map headlines= new HashMap();
        for (int i=0, n= heads.length; i < n; i++){
            headlines.put(heads[i].getId(), heads[i]);
        }
        return headlines;
    }
    
    public static Sms getSMS() {
        int MAX= 151;
        Ticker sms= loadTicker("sms");
        StringBuffer sb= new StringBuffer();
        Sms message= new Sms();
        message.setMax(MAX);
        Date sending= new Date();
        for (int i= 0; i < sms.getHeadlineCount(); i++){
            Headline h= sms.getHeadline(i);
            Date d= h.getLive();
            String head= h.getText();
//            System.out.println(head);
            if (d.compareTo(new Date()) >= 0){
//                    && (sb.length() + head.length() + 3) <= MAX){
                if (d.compareTo(sending) > 0){
                    sending= d;
                }
                sb.append(head).append(" * ");
                //                h.setLive(new Date());
                //                TickerForm.saveTicker("sms", sms);
            }
        }
        if (sb.length() > 0){
            message.setMessage(sb.toString());
            message.setDate(sending);
            return message;
        }
        return null;
    }

    public static void resetSMS() {
        Ticker sms= loadTicker("sms");
        for (int i= 0; i < sms.getHeadlineCount(); i++){
            Headline h= sms.getHeadline(i);
            h.setLive(new Date());
        }
        TickerForm.saveTicker("sms", sms);
    }

    
    public String getSmsText(){
        Sms sms= getSMS();
        if (sms == null){
            System.out.println("Null SMS");
            return "";
        }
        String text= getSMS().getMessage();
        text= (text != null) ? text + " ("+ text.length()+ " characters)" : "";
        return text;
    }
    
    public boolean getIsSms(){
        return _workName.equals("sms");
    }

    public String sendSms(){
        String result= null;
        Sms sms= getSMS();
        try{
            result= sms.send();
            System.out.println(sms.toString()+":"+result);
            if (result.equals("ok")){
//                resetSMS();
            }
        } catch (Exception e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }
    
    public boolean getIsJBag(){
        return _workName.equals("jbag");
    }

    public String sendJBag(){
        return CDEI.updateJbag(59);
    }
    
    public boolean getIsNTL(){
        return _workName.equals("ntl");
    }

    public String sendNTL(){
        return FtpC.sendNTL();
    }
    
    public boolean getIsTesco(){
        return _workName.equals("tesco");
    }

    public String sendTesco(){
        return FtpC.sendTesco();
    }
    
    public boolean getIsWap(){
        return _workName.equals("wap");
    }
    
    public String sendWap(){
        return WapCDEI.updateJbag(322);
    }
    
    public boolean getIsTicker(){
        return _workName.equals("ticker");
    }
    
    public String sendTicker(){
        boolean r= Sender.send(base, "ticker");
        //            Runtime.getRuntime().exec("/usr/bin/scp -i /usr/local/tomcat/id_dsa /www/data/hello/newsticker/wap.xml atila@pu2.hola.com:/www/data/hello/newsticker/wap.xml");
        
    return r ? "ok" : "error";
    }

    public boolean getIsBlueyonder(){
        return _workName.equals("blueyonder");
    }
    
    public String sendBlueyonder(){
        boolean r= Sender.send(base, "blueyonder");
        //            Runtime.getRuntime().exec("/usr/bin/scp -i /usr/local/tomcat/id_dsa /www/data/hello/newsticker/wap.xml atila@pu2.hola.com:/www/data/hello/newsticker/wap.xml");
        
    return r ? "ok" : "error";
    }

    public boolean getIsUKOnline(){
        return _workName.equals("ukonline");
    }
    
    public String sendUKOnline(){
        boolean r= Sender.send(base, "ukonline");
        //            Runtime.getRuntime().exec("/usr/bin/scp -i /usr/local/tomcat/id_dsa /www/data/hello/newsticker/wap.xml atila@pu2.hola.com:/www/data/hello/newsticker/wap.xml");
        
    return r ? "ok" : "error";
    }
    
    public boolean getIsBiography(){
        return _workName.equals("bc");
    }
    
    public String sendBiography(){
        boolean r= Sender.send(base, "bc");
        //            Runtime.getRuntime().exec("/usr/bin/scp -i /usr/local/tomcat/id_dsa /www/data/hello/newsticker/wap.xml atila@pu2.hola.com:/www/data/hello/newsticker/wap.xml");
        
        return r ? "ok" : "error";
    }
    
    public boolean getIsYahoo(){
        return _workName.equals("yahoo");
    }

    public String sendYahoo(){
        return FtpC.sendYahoo();
    }
    
    public static void main(String[] args){
        TickerForm tf= new TickerForm();
        System.out.println(tf.sendWap());
    }
}