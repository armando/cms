/*
 * Created on Jun 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hellomagazine.ed.ticker;

import java.util.Comparator;

import com.hellomagazine.newsticker.Headline;

/**
 * @author armando
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

public class ElementComparator implements Comparator{
	public int compare(Object a, Object b){
		Headline h1= (Headline) a;
		Headline h2= (Headline) b;
		return h1.getOrder().intValue() - h2.getOrder().intValue();
	}
	public boolean equals(Object a, Object b){
		Headline h1= (Headline) a;
		Headline h2= (Headline) b;
		return h1.getOrder().equals(h2.getOrder());
	}
}