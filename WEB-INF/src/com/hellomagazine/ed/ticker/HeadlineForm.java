package com.hellomagazine.ed.ticker;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import com.hellomagazine.menu.Menu;
import com.hellomagazine.menu.Submenu;
import com.hellomagazine.newsticker.Headline;
import com.hellomagazine.newsticker.Ticker;
import com.mindprod.base64.Base64;

/**
 * @author Armando Ramos <armando@hola.com>
 */
public class HeadlineForm {
    private Integer id;
    private int _id, _order= -1;
    private String _text;
    private String _URL;
    private String _type;
    private String _deck;
    private String _byline;
    private List _teaser;
    private String _summary;
    private String _fulltext;
    private List _images = new ArrayList();
    private Date _live, _expire;
    private List _related= new ArrayList(3);
    private List _caption= new ArrayList(2);

    private static List types= new ArrayList();
    static {
        types.add(new SelectItem("text/html", "Black Underlined", null));
        types.add(new SelectItem("text/hellor", "Red", null));
        types.add(new SelectItem("text/hellorb", "Red Boldface", null));
    }

    private static List hbtypes= new ArrayList();
    static {
        Menu menu= null;
        try {
            menu= Menu.unmarshal(new FileReader("/www/data/hello/newsticker/menu.xml"));
        } catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        for (int i=0, n= menu.getSubmenuCount(); i < n; i++){
            Submenu sm= menu.getSubmenu(i);
            for (int j=0, m= sm.getSubmenuCount(); j < m; j++){
                Submenu ssm= (Submenu) sm.getSubmenu(j);
                String urlname= sm.getName()+"/"+ssm.getName();
                String name= sm.getDescription()+"/"+ssm.getDescription();
                hbtypes.add(new SelectItem(urlname, name, null));
//                System.out.println(urlname+"-"+name);
            }
        }
    }
    
    
    public int getId() {
        return _id;
    }

    public String add() {
        System.out.println("Entrando en add");
        Ticker t= null;
        if (getTicker().getIsContext() || _id == 0) {
            _id= getTicker().getMaxID() + 1;
            t= getTicker().getWork();
        } else {
            t= getTicker().getContext();
        }
        _order= getTicker().getHeadlineCount();
        System.out.println("Add: Setting _id"+_id+" context "+getTicker().getIsContext());
        Headline headline= getTicker().getHeadline(_id, t);
        if (headline != null) {
            _text = headline.getText();
            _URL = headline.getURL();
            _type = headline.getType();
            _deck = headline.getDeck();
            _byline = headline.getByline();
            _teaser = Arrays.asList(headline.getTeaser());
            _summary = headline.getSummary();
            _fulltext = headline.getFulltext();
            _images = Arrays.asList(headline.getImage());
            _related= Arrays.asList(headline.getRelated());
            _caption= Arrays.asList(headline.getCaption());
            _live = (headline.getLive() == null)?new Date():headline.getLive();
            _expire = (headline.getLive() == null)?new Date():headline.getExpire();
            save();
        } else {
            			System.out.println("headline null");
        }
        return "go_headline";
    }

    public void setId(int newId) {
        _id= newId;
        if (_id <= 0 && getTicker().getIsContext()) {
            _id= getTicker().getMaxID() + 1;
            _order= getTicker().getHeadlineCount();
        }
//        System.out.println("Setting _id=" + _id + " context "
//                + getTicker().getIsContext());
        Headline headline= getTicker().getHeadline(_id, getTicker().getWork());
        if (headline != null) {
            _text= headline.getText();
            _URL= headline.getURL();
            _type= headline.getType();
            _deck= headline.getDeck();
            _byline= headline.getByline();
            _teaser= new ArrayList(Arrays.asList(headline.getTeaser()));
            
            _summary= headline.getSummary() != null ? headline.getSummary() : null;
            _fulltext= headline.getFulltext() != null ? headline.getFulltext() : null;
            _live = headline.getLive();
            _expire = headline.getExpire();
            _images= new ArrayList(Arrays.asList(headline.getImage()));
            _related= new ArrayList(Arrays.asList(headline.getRelated()));
            _caption= new ArrayList(Arrays.asList(headline.getCaption()));
       }
    }

    private Headline getHeadline() {
//        System.out.println("Creating headline from form " + _id);
        Headline h= new Headline();
        h.setId(new Integer(_id));
        h.setOrder(new Integer(_order));
        h.setText(_text);
//        h.setSummary(_summary.replace('\n','|'));
        h.setSummary(_summary);
        h.setFulltext(_fulltext);
        h.setURL(_URL);
        h.setType(_type);
        h.setDeck(_deck);
        h.setByline(_byline);
        h.setCreated(new Date());
        h.setLive(_live);
        h.setExpire(_expire);
/*
        String[] tea = new String[_teaser.size()];
        for (int i = 0; i < _teaser.size(); i++)
        {
            tea[i] = (String)_teaser.get(i);
        }
        */
        String[] img = new String[_images.size()];
        for (int i = 0; i < _images.size(); i++)
        {
            img[i] = (String)_images.get(i);
        }
        String[] cap = new String[_caption.size()];
        for (int i = 0; i < _caption.size(); i++)
        {
            cap[i] = (String)_caption.get(i);
        }
        String[] rel = new String[_related.size()];
        for (int i = 0; i < _related.size(); i++)
        {
            rel[i] = (String)_related.get(i);
        }
//        h.setTeaser(tea);
        h.setImage(img);
        h.setCaption(cap);
        h.setRelated(rel);
        return h;
    }

    public String save() {
        Headline h= getHeadline();
        getTicker().saveHeadline(h, _id, _order);
//        System.out.println("Grabando " + _id + " en posicion " + _order);
        return "ok_next";
    }

    public String delete() {
        System.out.println("Borrando "+_id+" de "+getTicker().getWorkName());
        getTicker().deleteHeadline(_id);
//        System.out.println("Borrando "+_id);
        return "ok_next";
    }

    private TickerForm getTicker() {
        Object obj= FacesContext.getCurrentInstance().getApplication()
                .getVariableResolver().resolveVariable(
                        FacesContext.getCurrentInstance(), "tickerForm");
        return (TickerForm) obj;
    }

    public String up() {
        getTicker().move(_id, "up");
        return "ok";
    }

    public String down() {
        getTicker().move(_id, "down");
        return "ok";
    }

    /**
     * @return
     */
    public String getText() {
        return _text;
    }

    

    public String getImage1Name()
    {
        TickerForm tickerForm = getTicker();
        return "/" + tickerForm.getWorkName() + "/" + _order + "/0.jpg";
    }

    public UploadedFile getImage1(){return null;}
    public void setImage1(UploadedFile upFile) {
        try
        {
            Base64 encoder= new Base64();
//            System.out.println(_images);
            _images.add(0, encoder.encode(upFile.getBytes()));
            save();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void deleteImage1()
    {
        if (_images.size() >= 1)
        {
            _images.remove(0);
            save();
        }
    }
    
    public UploadedFile getImage2(){return null;}
    public String getImage2Name()
    {
        TickerForm tickerForm = getTicker();
        return "/" + tickerForm.getWorkName() + "/" + _order + "/1.jpg";
    }
    
    public void setImage2(UploadedFile upFile) {
//        System.out.println("WEEEEE");
        try
        {
            Base64 encoder= new Base64();
            if (_images.size() >= 2)
            {
                _images.remove(1);
            }
            _images.add(1, encoder.encode(upFile.getBytes()));
            save();            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void deleteImage2()
    {
        if (_images.size() >= 2)
        {
            _images.remove(1);
            save();
        }
    }
    
    public boolean isImage1Set()
    {
        return (_images.size() >= 1);
    }

    public boolean isImage2Set()
    {
        return (_images.size() >= 2);
    }

    /**
     * @return
     */
    public String getURL() {
        return _URL;
    }

    /**
     * @param string
     */
    public void setText(String string) {
        _text= string;
    }

    /**
     * @param string
     */
    public void setURL(String string) {
        _URL= string;
    }

    /**
     * @return
     */
    public String getDeck() {
        return _deck;
    }

    /**
     * @param string
     */
    public void setDeck(String string) {
        _deck= string;
    }

    /**
     * @return
     */
    public String getByline() {
        return _byline;
    }

    /**
     * @param string
     */
    public void setByline(String string) {
        _byline= string;
    }

    /**
     * @return
     */
    public String getType() {
        return _type;
    }

    /**
     * @param string
     */
    public void setType(String string) {
        _type= string;
    }

    
    
    public List getTypes() {
        return hbtypes;
    }

    /**
     * @return
     */
    public String getSummary() {
        return _summary;
    }

    /**
     * @param string
     */
    public void setSummary(String string) {
        _summary= string.replaceAll("<br>","");
        _summary= _summary.replaceAll("<br/>","");
//        System.out.println("newline en: "+ string.indexOf('\n'));
    }

    /**
     * @return
     */
    public int getOrder() {
        if (_order == -1) {
            _order= getTicker().getOrder(_id);
        }
        return _order;
    }

    /**
     * @param i
     */
    public void setOrder(int in) {
        _order= in;
        save();
    }
    
    public Date getLiveDate()
    {
        return _live;
    }
    
    public String getLive(){
        return _live.toGMTString();
    }
    
    public void setLiveDate(Date live){
        _live = live;
        save();
    }

    public String getExpire(){
        return _expire.toGMTString();
    }
    
    public void setExpiryDate(Date expire){
        _expire = expire;
        save();
    }
    
    public String getCaption1() {
        return _caption.size()>0 ? (String) _caption.get(0) : "";
    }
    
    public void setCaption1(String caption) {
        if (_caption.size()<1){
            this._caption.add(0, caption);
        }
        else{
            this._caption.set(0, caption);
        }
    }

    public String getCaption2() {
        return _caption.size()>1 ? (String) _caption.get(1) : "";
    }
    
    public void setCaption2(String caption) {
        if (_caption.size()<2){
            this._caption.add(1, caption);
        }
        else{
            this._caption.set(1, caption);
        }
    }

    public String getFulltext() {
        return _fulltext;
    }
    
    public void setFulltext(String fulltext) {
        System.out.println(fulltext);
        this._fulltext = fulltext;
    }
    
    public String getRelated1() {
        return _related.size()>0 ? (String) _related.get(0) : "";
    }
    
    public void setRelated1(String related) {
        if (_related.size()<1){
            this._related.add(0, related);
        }
        else{
            this._related.set(0, related);
        }
    }

    public String getRelated2() {
        return _related.size()>1 ? (String) _related.get(1) : "";
    }
    
    public void setRelated2(String related) {
        if (_related.size()<2){
            this._related.add(1, related);
        }
        else{
            this._related.set(1, related);
        }
    }

    public String getRelated3() {
        return _related.size()>2 ? (String) _related.get(2) : "";
    }
    
    public void setRelated3(String related) {
        if (_related.size()<3){
            this._related.add(2, related);
        }
        else{
            this._related.set(2, related);
        }
    }

}