/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.jbag;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Par.
 * 
 * @version $Revision$ $Date$
 */
public class Par extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _fmtList
     */
    private java.util.ArrayList _fmtList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Par() {
        super();
        _fmtList = new ArrayList();
    } //-- com.hellomagazine.jbag.Par()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addFmt
     * 
     * @param vFmt
     */
    public void addFmt(com.hellomagazine.jbag.Fmt vFmt)
        throws java.lang.IndexOutOfBoundsException
    {
        _fmtList.add(vFmt);
    } //-- void addFmt(com.hellomagazine.jbag.Fmt) 

    /**
     * Method addFmt
     * 
     * @param index
     * @param vFmt
     */
    public void addFmt(int index, com.hellomagazine.jbag.Fmt vFmt)
        throws java.lang.IndexOutOfBoundsException
    {
        _fmtList.add(index, vFmt);
    } //-- void addFmt(int, com.hellomagazine.jbag.Fmt) 

    /**
     * Method clearFmt
     */
    public void clearFmt()
    {
        _fmtList.clear();
    } //-- void clearFmt() 

    /**
     * Method enumerateFmt
     */
    public java.util.Enumeration enumerateFmt()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_fmtList.iterator());
    } //-- java.util.Enumeration enumerateFmt() 

    /**
     * Method getFmt
     * 
     * @param index
     */
    public com.hellomagazine.jbag.Fmt getFmt(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _fmtList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hellomagazine.jbag.Fmt) _fmtList.get(index);
    } //-- com.hellomagazine.jbag.Fmt getFmt(int) 

    /**
     * Method getFmt
     */
    public com.hellomagazine.jbag.Fmt[] getFmt()
    {
        int size = _fmtList.size();
        com.hellomagazine.jbag.Fmt[] mArray = new com.hellomagazine.jbag.Fmt[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hellomagazine.jbag.Fmt) _fmtList.get(index);
        }
        return mArray;
    } //-- com.hellomagazine.jbag.Fmt[] getFmt() 

    /**
     * Method getFmtCount
     */
    public int getFmtCount()
    {
        return _fmtList.size();
    } //-- int getFmtCount() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeFmt
     * 
     * @param vFmt
     */
    public boolean removeFmt(com.hellomagazine.jbag.Fmt vFmt)
    {
        boolean removed = _fmtList.remove(vFmt);
        return removed;
    } //-- boolean removeFmt(com.hellomagazine.jbag.Fmt) 

    /**
     * Method setFmt
     * 
     * @param index
     * @param vFmt
     */
    public void setFmt(int index, com.hellomagazine.jbag.Fmt vFmt)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _fmtList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _fmtList.set(index, vFmt);
    } //-- void setFmt(int, com.hellomagazine.jbag.Fmt) 

    /**
     * Method setFmt
     * 
     * @param fmtArray
     */
    public void setFmt(com.hellomagazine.jbag.Fmt[] fmtArray)
    {
        //-- copy array
        _fmtList.clear();
        for (int i = 0; i < fmtArray.length; i++) {
            _fmtList.add(fmtArray[i]);
        }
    } //-- void setFmt(com.hellomagazine.jbag.Fmt) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.jbag.Par) Unmarshaller.unmarshal(com.hellomagazine.jbag.Par.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
