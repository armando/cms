/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.jbag;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Action.
 * 
 * @version $Revision$ $Date$
 */
public class Action extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _event
     */
    private java.lang.String _event;

    /**
     * Field _commandList
     */
    private java.util.ArrayList _commandList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Action() {
        super();
        _commandList = new ArrayList();
    } //-- com.hellomagazine.jbag.Action()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addCommand
     * 
     * @param vCommand
     */
    public void addCommand(com.hellomagazine.jbag.Command vCommand)
        throws java.lang.IndexOutOfBoundsException
    {
        _commandList.add(vCommand);
    } //-- void addCommand(com.hellomagazine.jbag.Command) 

    /**
     * Method addCommand
     * 
     * @param index
     * @param vCommand
     */
    public void addCommand(int index, com.hellomagazine.jbag.Command vCommand)
        throws java.lang.IndexOutOfBoundsException
    {
        _commandList.add(index, vCommand);
    } //-- void addCommand(int, com.hellomagazine.jbag.Command) 

    /**
     * Method clearCommand
     */
    public void clearCommand()
    {
        _commandList.clear();
    } //-- void clearCommand() 

    /**
     * Method enumerateCommand
     */
    public java.util.Enumeration enumerateCommand()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_commandList.iterator());
    } //-- java.util.Enumeration enumerateCommand() 

    /**
     * Method getCommand
     * 
     * @param index
     */
    public com.hellomagazine.jbag.Command getCommand(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _commandList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hellomagazine.jbag.Command) _commandList.get(index);
    } //-- com.hellomagazine.jbag.Command getCommand(int) 

    /**
     * Method getCommand
     */
    public com.hellomagazine.jbag.Command[] getCommand()
    {
        int size = _commandList.size();
        com.hellomagazine.jbag.Command[] mArray = new com.hellomagazine.jbag.Command[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hellomagazine.jbag.Command) _commandList.get(index);
        }
        return mArray;
    } //-- com.hellomagazine.jbag.Command[] getCommand() 

    /**
     * Method getCommandCount
     */
    public int getCommandCount()
    {
        return _commandList.size();
    } //-- int getCommandCount() 

    /**
     * Returns the value of field 'event'.
     * 
     * @return the value of field 'event'.
     */
    public java.lang.String getEvent()
    {
        return this._event;
    } //-- java.lang.String getEvent() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeCommand
     * 
     * @param vCommand
     */
    public boolean removeCommand(com.hellomagazine.jbag.Command vCommand)
    {
        boolean removed = _commandList.remove(vCommand);
        return removed;
    } //-- boolean removeCommand(com.hellomagazine.jbag.Command) 

    /**
     * Method setCommand
     * 
     * @param index
     * @param vCommand
     */
    public void setCommand(int index, com.hellomagazine.jbag.Command vCommand)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _commandList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _commandList.set(index, vCommand);
    } //-- void setCommand(int, com.hellomagazine.jbag.Command) 

    /**
     * Method setCommand
     * 
     * @param commandArray
     */
    public void setCommand(com.hellomagazine.jbag.Command[] commandArray)
    {
        //-- copy array
        _commandList.clear();
        for (int i = 0; i < commandArray.length; i++) {
            _commandList.add(commandArray[i]);
        }
    } //-- void setCommand(com.hellomagazine.jbag.Command) 

    /**
     * Sets the value of field 'event'.
     * 
     * @param event the value of field 'event'.
     */
    public void setEvent(java.lang.String event)
    {
        this._event = event;
    } //-- void setEvent(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.jbag.Action) Unmarshaller.unmarshal(com.hellomagazine.jbag.Action.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
