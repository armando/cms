/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.jbag;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Description.
 * 
 * @version $Revision$ $Date$
 */
public class Description extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _entry
     */
    private com.hellomagazine.jbag.Entry _entry;

    /**
     * Field _splash
     */
    private com.hellomagazine.jbag.Splash _splash;

    /**
     * Field _midlet
     */
    private com.hellomagazine.jbag.Midlet _midlet;

    /**
     * Field _actionList
     */
    private java.util.ArrayList _actionList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Description() {
        super();
        _actionList = new ArrayList();
    } //-- com.hellomagazine.jbag.Description()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addAction
     * 
     * @param vAction
     */
    public void addAction(com.hellomagazine.jbag.Action vAction)
        throws java.lang.IndexOutOfBoundsException
    {
        _actionList.add(vAction);
    } //-- void addAction(com.hellomagazine.jbag.Action) 

    /**
     * Method addAction
     * 
     * @param index
     * @param vAction
     */
    public void addAction(int index, com.hellomagazine.jbag.Action vAction)
        throws java.lang.IndexOutOfBoundsException
    {
        _actionList.add(index, vAction);
    } //-- void addAction(int, com.hellomagazine.jbag.Action) 

    /**
     * Method clearAction
     */
    public void clearAction()
    {
        _actionList.clear();
    } //-- void clearAction() 

    /**
     * Method enumerateAction
     */
    public java.util.Enumeration enumerateAction()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_actionList.iterator());
    } //-- java.util.Enumeration enumerateAction() 

    /**
     * Method getAction
     * 
     * @param index
     */
    public com.hellomagazine.jbag.Action getAction(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _actionList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hellomagazine.jbag.Action) _actionList.get(index);
    } //-- com.hellomagazine.jbag.Action getAction(int) 

    /**
     * Method getAction
     */
    public com.hellomagazine.jbag.Action[] getAction()
    {
        int size = _actionList.size();
        com.hellomagazine.jbag.Action[] mArray = new com.hellomagazine.jbag.Action[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hellomagazine.jbag.Action) _actionList.get(index);
        }
        return mArray;
    } //-- com.hellomagazine.jbag.Action[] getAction() 

    /**
     * Method getActionCount
     */
    public int getActionCount()
    {
        return _actionList.size();
    } //-- int getActionCount() 

    /**
     * Returns the value of field 'entry'.
     * 
     * @return the value of field 'entry'.
     */
    public com.hellomagazine.jbag.Entry getEntry()
    {
        return this._entry;
    } //-- com.hellomagazine.jbag.Entry getEntry() 

    /**
     * Returns the value of field 'midlet'.
     * 
     * @return the value of field 'midlet'.
     */
    public com.hellomagazine.jbag.Midlet getMidlet()
    {
        return this._midlet;
    } //-- com.hellomagazine.jbag.Midlet getMidlet() 

    /**
     * Returns the value of field 'splash'.
     * 
     * @return the value of field 'splash'.
     */
    public com.hellomagazine.jbag.Splash getSplash()
    {
        return this._splash;
    } //-- com.hellomagazine.jbag.Splash getSplash() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAction
     * 
     * @param vAction
     */
    public boolean removeAction(com.hellomagazine.jbag.Action vAction)
    {
        boolean removed = _actionList.remove(vAction);
        return removed;
    } //-- boolean removeAction(com.hellomagazine.jbag.Action) 

    /**
     * Method setAction
     * 
     * @param index
     * @param vAction
     */
    public void setAction(int index, com.hellomagazine.jbag.Action vAction)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _actionList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _actionList.set(index, vAction);
    } //-- void setAction(int, com.hellomagazine.jbag.Action) 

    /**
     * Method setAction
     * 
     * @param actionArray
     */
    public void setAction(com.hellomagazine.jbag.Action[] actionArray)
    {
        //-- copy array
        _actionList.clear();
        for (int i = 0; i < actionArray.length; i++) {
            _actionList.add(actionArray[i]);
        }
    } //-- void setAction(com.hellomagazine.jbag.Action) 

    /**
     * Sets the value of field 'entry'.
     * 
     * @param entry the value of field 'entry'.
     */
    public void setEntry(com.hellomagazine.jbag.Entry entry)
    {
        this._entry = entry;
    } //-- void setEntry(com.hellomagazine.jbag.Entry) 

    /**
     * Sets the value of field 'midlet'.
     * 
     * @param midlet the value of field 'midlet'.
     */
    public void setMidlet(com.hellomagazine.jbag.Midlet midlet)
    {
        this._midlet = midlet;
    } //-- void setMidlet(com.hellomagazine.jbag.Midlet) 

    /**
     * Sets the value of field 'splash'.
     * 
     * @param splash the value of field 'splash'.
     */
    public void setSplash(com.hellomagazine.jbag.Splash splash)
    {
        this._splash = splash;
    } //-- void setSplash(com.hellomagazine.jbag.Splash) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.jbag.Description) Unmarshaller.unmarshal(com.hellomagazine.jbag.Description.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
