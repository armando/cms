/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.jbag;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Splash.
 * 
 * @version $Revision$ $Date$
 */
public class Splash extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _img
     */
    private java.lang.String _img;

    /**
     * Field _progressbar
     */
    private java.lang.String _progressbar;

    /**
     * Field _waitforclick
     */
    private java.lang.String _waitforclick;

    /**
     * Field _delay
     */
    private java.lang.Integer _delay;


      //----------------/
     //- Constructors -/
    //----------------/

    public Splash() {
        super();
    } //-- com.hellomagazine.jbag.Splash()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'delay'.
     * 
     * @return the value of field 'delay'.
     */
    public java.lang.Integer getDelay()
    {
        return this._delay;
    } //-- java.lang.Integer getDelay() 

    /**
     * Returns the value of field 'img'.
     * 
     * @return the value of field 'img'.
     */
    public java.lang.String getImg()
    {
        return this._img;
    } //-- java.lang.String getImg() 

    /**
     * Returns the value of field 'progressbar'.
     * 
     * @return the value of field 'progressbar'.
     */
    public java.lang.String getProgressbar()
    {
        return this._progressbar;
    } //-- java.lang.String getProgressbar() 

    /**
     * Returns the value of field 'waitforclick'.
     * 
     * @return the value of field 'waitforclick'.
     */
    public java.lang.String getWaitforclick()
    {
        return this._waitforclick;
    } //-- java.lang.String getWaitforclick() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'delay'.
     * 
     * @param delay the value of field 'delay'.
     */
    public void setDelay(java.lang.Integer delay)
    {
        this._delay = delay;
    } //-- void setDelay(java.lang.Integer) 

    /**
     * Sets the value of field 'img'.
     * 
     * @param img the value of field 'img'.
     */
    public void setImg(java.lang.String img)
    {
        this._img = img;
    } //-- void setImg(java.lang.String) 

    /**
     * Sets the value of field 'progressbar'.
     * 
     * @param progressbar the value of field 'progressbar'.
     */
    public void setProgressbar(java.lang.String progressbar)
    {
        this._progressbar = progressbar;
    } //-- void setProgressbar(java.lang.String) 

    /**
     * Sets the value of field 'waitforclick'.
     * 
     * @param waitforclick the value of field 'waitforclick'.
     */
    public void setWaitforclick(java.lang.String waitforclick)
    {
        this._waitforclick = waitforclick;
    } //-- void setWaitforclick(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.jbag.Splash) Unmarshaller.unmarshal(com.hellomagazine.jbag.Splash.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
