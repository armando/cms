/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.jbag;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Piece.
 * 
 * @version $Revision$ $Date$
 */
public class Piece extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _image
     */
    private com.hellomagazine.jbag.Image _image;

    /**
     * Field _par
     */
    private com.hellomagazine.jbag.Par _par;


      //----------------/
     //- Constructors -/
    //----------------/

    public Piece() {
        super();
    } //-- com.hellomagazine.jbag.Piece()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'image'.
     * 
     * @return the value of field 'image'.
     */
    public com.hellomagazine.jbag.Image getImage()
    {
        return this._image;
    } //-- com.hellomagazine.jbag.Image getImage() 

    /**
     * Returns the value of field 'par'.
     * 
     * @return the value of field 'par'.
     */
    public com.hellomagazine.jbag.Par getPar()
    {
        return this._par;
    } //-- com.hellomagazine.jbag.Par getPar() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'image'.
     * 
     * @param image the value of field 'image'.
     */
    public void setImage(com.hellomagazine.jbag.Image image)
    {
        this._image = image;
    } //-- void setImage(com.hellomagazine.jbag.Image) 

    /**
     * Sets the value of field 'par'.
     * 
     * @param par the value of field 'par'.
     */
    public void setPar(com.hellomagazine.jbag.Par par)
    {
        this._par = par;
    } //-- void setPar(com.hellomagazine.jbag.Par) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.jbag.Piece) Unmarshaller.unmarshal(com.hellomagazine.jbag.Piece.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
