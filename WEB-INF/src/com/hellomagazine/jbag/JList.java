/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.jbag;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class JList.
 * 
 * @version $Revision$ $Date$
 */
public class JList extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _name
     */
    private java.lang.String _name;

    /**
     * Field _height
     */
    private java.lang.Integer _height;

    /**
     * Field _img_top
     */
    private java.lang.String _img_top;

    /**
     * Field _img_bottom
     */
    private java.lang.String _img_bottom;

    /**
     * Field _resource
     */
    private java.lang.Integer _resource;

    /**
     * Field _itemList
     */
    private java.util.ArrayList _itemList;


      //----------------/
     //- Constructors -/
    //----------------/

    public JList() {
        super();
        _itemList = new ArrayList();
    } //-- com.hellomagazine.jbag.JList()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addItem
     * 
     * @param vItem
     */
    public void addItem(com.hellomagazine.jbag.Item vItem)
        throws java.lang.IndexOutOfBoundsException
    {
        _itemList.add(vItem);
    } //-- void addItem(com.hellomagazine.jbag.Item) 

    /**
     * Method addItem
     * 
     * @param index
     * @param vItem
     */
    public void addItem(int index, com.hellomagazine.jbag.Item vItem)
        throws java.lang.IndexOutOfBoundsException
    {
        _itemList.add(index, vItem);
    } //-- void addItem(int, com.hellomagazine.jbag.Item) 

    /**
     * Method clearItem
     */
    public void clearItem()
    {
        _itemList.clear();
    } //-- void clearItem() 

    /**
     * Method enumerateItem
     */
    public java.util.Enumeration enumerateItem()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_itemList.iterator());
    } //-- java.util.Enumeration enumerateItem() 

    /**
     * Returns the value of field 'height'.
     * 
     * @return the value of field 'height'.
     */
    public java.lang.Integer getHeight()
    {
        return this._height;
    } //-- java.lang.Integer getHeight() 

    /**
     * Returns the value of field 'img_bottom'.
     * 
     * @return the value of field 'img_bottom'.
     */
    public java.lang.String getImg_bottom()
    {
        return this._img_bottom;
    } //-- java.lang.String getImg_bottom() 

    /**
     * Returns the value of field 'img_top'.
     * 
     * @return the value of field 'img_top'.
     */
    public java.lang.String getImg_top()
    {
        return this._img_top;
    } //-- java.lang.String getImg_top() 

    /**
     * Method getItem
     * 
     * @param index
     */
    public com.hellomagazine.jbag.Item getItem(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _itemList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hellomagazine.jbag.Item) _itemList.get(index);
    } //-- com.hellomagazine.jbag.Item getItem(int) 

    /**
     * Method getItem
     */
    public com.hellomagazine.jbag.Item[] getItem()
    {
        int size = _itemList.size();
        com.hellomagazine.jbag.Item[] mArray = new com.hellomagazine.jbag.Item[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hellomagazine.jbag.Item) _itemList.get(index);
        }
        return mArray;
    } //-- com.hellomagazine.jbag.Item[] getItem() 

    /**
     * Method getItemCount
     */
    public int getItemCount()
    {
        return _itemList.size();
    } //-- int getItemCount() 

    /**
     * Returns the value of field 'name'.
     * 
     * @return the value of field 'name'.
     */
    public java.lang.String getName()
    {
        return this._name;
    } //-- java.lang.String getName() 

    /**
     * Returns the value of field 'resource'.
     * 
     * @return the value of field 'resource'.
     */
    public java.lang.Integer getResource()
    {
        return this._resource;
    } //-- java.lang.Integer getResource() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeItem
     * 
     * @param vItem
     */
    public boolean removeItem(com.hellomagazine.jbag.Item vItem)
    {
        boolean removed = _itemList.remove(vItem);
        return removed;
    } //-- boolean removeItem(com.hellomagazine.jbag.Item) 

    /**
     * Sets the value of field 'height'.
     * 
     * @param height the value of field 'height'.
     */
    public void setHeight(java.lang.Integer height)
    {
        this._height = height;
    } //-- void setHeight(java.lang.Integer) 

    /**
     * Sets the value of field 'img_bottom'.
     * 
     * @param img_bottom the value of field 'img_bottom'.
     */
    public void setImg_bottom(java.lang.String img_bottom)
    {
        this._img_bottom = img_bottom;
    } //-- void setImg_bottom(java.lang.String) 

    /**
     * Sets the value of field 'img_top'.
     * 
     * @param img_top the value of field 'img_top'.
     */
    public void setImg_top(java.lang.String img_top)
    {
        this._img_top = img_top;
    } //-- void setImg_top(java.lang.String) 

    /**
     * Method setItem
     * 
     * @param index
     * @param vItem
     */
    public void setItem(int index, com.hellomagazine.jbag.Item vItem)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _itemList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _itemList.set(index, vItem);
    } //-- void setItem(int, com.hellomagazine.jbag.Item) 

    /**
     * Method setItem
     * 
     * @param itemArray
     */
    public void setItem(com.hellomagazine.jbag.Item[] itemArray)
    {
        //-- copy array
        _itemList.clear();
        for (int i = 0; i < itemArray.length; i++) {
            _itemList.add(itemArray[i]);
        }
    } //-- void setItem(com.hellomagazine.jbag.Item) 

    /**
     * Sets the value of field 'name'.
     * 
     * @param name the value of field 'name'.
     */
    public void setName(java.lang.String name)
    {
        this._name = name;
    } //-- void setName(java.lang.String) 

    /**
     * Sets the value of field 'resource'.
     * 
     * @param resource the value of field 'resource'.
     */
    public void setResource(java.lang.Integer resource)
    {
        this._resource = resource;
    } //-- void setResource(java.lang.Integer) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.jbag.JList) Unmarshaller.unmarshal(com.hellomagazine.jbag.JList.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
