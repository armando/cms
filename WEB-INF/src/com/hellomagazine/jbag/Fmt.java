/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.jbag;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Fmt.
 * 
 * @version $Revision$ $Date$
 */
public class Fmt extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _txt_color
     */
    private java.lang.String _txt_color;

    /**
     * Field _font_style
     */
    private java.lang.String _font_style;

    /**
     * Field _font_size
     */
    private java.lang.String _font_size;

    /**
     * internal content storage
     */
    private java.lang.String _content = "";

    /**
     * Field _lfList
     */
    private java.util.ArrayList _lfList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Fmt() {
        super();
        setContent("");
        _lfList = new ArrayList();
    } //-- com.hellomagazine.jbag.Fmt()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addLf
     * 
     * @param vLf
     */
    public void addLf(com.hellomagazine.jbag.Lf vLf)
        throws java.lang.IndexOutOfBoundsException
    {
        _lfList.add(vLf);
    } //-- void addLf(com.hellomagazine.jbag.Lf) 

    /**
     * Method addLf
     * 
     * @param index
     * @param vLf
     */
    public void addLf(int index, com.hellomagazine.jbag.Lf vLf)
        throws java.lang.IndexOutOfBoundsException
    {
        _lfList.add(index, vLf);
    } //-- void addLf(int, com.hellomagazine.jbag.Lf) 

    /**
     * Method clearLf
     */
    public void clearLf()
    {
        _lfList.clear();
    } //-- void clearLf() 

    /**
     * Method enumerateLf
     */
    public java.util.Enumeration enumerateLf()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_lfList.iterator());
    } //-- java.util.Enumeration enumerateLf() 

    /**
     * Returns the value of field 'content'. The field 'content'
     * has the following description: internal content storage
     * 
     * @return the value of field 'content'.
     */
    public java.lang.String getContent()
    {
        return this._content;
    } //-- java.lang.String getContent() 

    /**
     * Returns the value of field 'font_size'.
     * 
     * @return the value of field 'font_size'.
     */
    public java.lang.String getFont_size()
    {
        return this._font_size;
    } //-- java.lang.String getFont_size() 

    /**
     * Returns the value of field 'font_style'.
     * 
     * @return the value of field 'font_style'.
     */
    public java.lang.String getFont_style()
    {
        return this._font_style;
    } //-- java.lang.String getFont_style() 

    /**
     * Method getLf
     * 
     * @param index
     */
    public com.hellomagazine.jbag.Lf getLf(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _lfList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hellomagazine.jbag.Lf) _lfList.get(index);
    } //-- com.hellomagazine.jbag.Lf getLf(int) 

    /**
     * Method getLf
     */
    public com.hellomagazine.jbag.Lf[] getLf()
    {
        int size = _lfList.size();
        com.hellomagazine.jbag.Lf[] mArray = new com.hellomagazine.jbag.Lf[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hellomagazine.jbag.Lf) _lfList.get(index);
        }
        return mArray;
    } //-- com.hellomagazine.jbag.Lf[] getLf() 

    /**
     * Method getLfCount
     */
    public int getLfCount()
    {
        return _lfList.size();
    } //-- int getLfCount() 

    /**
     * Returns the value of field 'txt_color'.
     * 
     * @return the value of field 'txt_color'.
     */
    public java.lang.String getTxt_color()
    {
        return this._txt_color;
    } //-- java.lang.String getTxt_color() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeLf
     * 
     * @param vLf
     */
    public boolean removeLf(com.hellomagazine.jbag.Lf vLf)
    {
        boolean removed = _lfList.remove(vLf);
        return removed;
    } //-- boolean removeLf(com.hellomagazine.jbag.Lf) 

    /**
     * Sets the value of field 'content'. The field 'content' has
     * the following description: internal content storage
     * 
     * @param content the value of field 'content'.
     */
    public void setContent(java.lang.String content)
    {
        this._content = content;
    } //-- void setContent(java.lang.String) 

    /**
     * Sets the value of field 'font_size'.
     * 
     * @param font_size the value of field 'font_size'.
     */
    public void setFont_size(java.lang.String font_size)
    {
        this._font_size = font_size;
    } //-- void setFont_size(java.lang.String) 

    /**
     * Sets the value of field 'font_style'.
     * 
     * @param font_style the value of field 'font_style'.
     */
    public void setFont_style(java.lang.String font_style)
    {
        this._font_style = font_style;
    } //-- void setFont_style(java.lang.String) 

    /**
     * Method setLf
     * 
     * @param index
     * @param vLf
     */
    public void setLf(int index, com.hellomagazine.jbag.Lf vLf)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _lfList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _lfList.set(index, vLf);
    } //-- void setLf(int, com.hellomagazine.jbag.Lf) 

    /**
     * Method setLf
     * 
     * @param lfArray
     */
    public void setLf(com.hellomagazine.jbag.Lf[] lfArray)
    {
        //-- copy array
        _lfList.clear();
        for (int i = 0; i < lfArray.length; i++) {
            _lfList.add(lfArray[i]);
        }
    } //-- void setLf(com.hellomagazine.jbag.Lf) 

    /**
     * Sets the value of field 'txt_color'.
     * 
     * @param txt_color the value of field 'txt_color'.
     */
    public void setTxt_color(java.lang.String txt_color)
    {
        this._txt_color = txt_color;
    } //-- void setTxt_color(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.jbag.Fmt) Unmarshaller.unmarshal(com.hellomagazine.jbag.Fmt.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
