/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.jbag;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Jbag.
 * 
 * @version $Revision$ $Date$
 */
public class Jbag extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _description
     */
    private com.hellomagazine.jbag.Description _description;

    /**
     * Field _jList
     */
    private com.hellomagazine.jbag.JList _jList;

    /**
     * Field _jViewList
     */
    private java.util.ArrayList _jViewList;

    /**
     * Field _rasterpicList
     */
    private java.util.ArrayList _rasterpicList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Jbag() {
        super();
        _jViewList = new ArrayList();
        _rasterpicList = new ArrayList();
    } //-- com.hellomagazine.jbag.Jbag()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addJView
     * 
     * @param vJView
     */
    public void addJView(com.hellomagazine.jbag.JView vJView)
        throws java.lang.IndexOutOfBoundsException
    {
        _jViewList.add(vJView);
    } //-- void addJView(com.hellomagazine.jbag.JView) 

    /**
     * Method addJView
     * 
     * @param index
     * @param vJView
     */
    public void addJView(int index, com.hellomagazine.jbag.JView vJView)
        throws java.lang.IndexOutOfBoundsException
    {
        _jViewList.add(index, vJView);
    } //-- void addJView(int, com.hellomagazine.jbag.JView) 

    /**
     * Method addRasterpic
     * 
     * @param vRasterpic
     */
    public void addRasterpic(com.hellomagazine.jbag.Rasterpic vRasterpic)
        throws java.lang.IndexOutOfBoundsException
    {
        _rasterpicList.add(vRasterpic);
    } //-- void addRasterpic(com.hellomagazine.jbag.Rasterpic) 

    /**
     * Method addRasterpic
     * 
     * @param index
     * @param vRasterpic
     */
    public void addRasterpic(int index, com.hellomagazine.jbag.Rasterpic vRasterpic)
        throws java.lang.IndexOutOfBoundsException
    {
        _rasterpicList.add(index, vRasterpic);
    } //-- void addRasterpic(int, com.hellomagazine.jbag.Rasterpic) 

    /**
     * Method clearJView
     */
    public void clearJView()
    {
        _jViewList.clear();
    } //-- void clearJView() 

    /**
     * Method clearRasterpic
     */
    public void clearRasterpic()
    {
        _rasterpicList.clear();
    } //-- void clearRasterpic() 

    /**
     * Method enumerateJView
     */
    public java.util.Enumeration enumerateJView()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_jViewList.iterator());
    } //-- java.util.Enumeration enumerateJView() 

    /**
     * Method enumerateRasterpic
     */
    public java.util.Enumeration enumerateRasterpic()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_rasterpicList.iterator());
    } //-- java.util.Enumeration enumerateRasterpic() 

    /**
     * Returns the value of field 'description'.
     * 
     * @return the value of field 'description'.
     */
    public com.hellomagazine.jbag.Description getDescription()
    {
        return this._description;
    } //-- com.hellomagazine.jbag.Description getDescription() 

    /**
     * Returns the value of field 'jList'.
     * 
     * @return the value of field 'jList'.
     */
    public com.hellomagazine.jbag.JList getJList()
    {
        return this._jList;
    } //-- com.hellomagazine.jbag.JList getJList() 

    /**
     * Method getJView
     * 
     * @param index
     */
    public com.hellomagazine.jbag.JView getJView(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _jViewList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hellomagazine.jbag.JView) _jViewList.get(index);
    } //-- com.hellomagazine.jbag.JView getJView(int) 

    /**
     * Method getJView
     */
    public com.hellomagazine.jbag.JView[] getJView()
    {
        int size = _jViewList.size();
        com.hellomagazine.jbag.JView[] mArray = new com.hellomagazine.jbag.JView[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hellomagazine.jbag.JView) _jViewList.get(index);
        }
        return mArray;
    } //-- com.hellomagazine.jbag.JView[] getJView() 

    /**
     * Method getJViewCount
     */
    public int getJViewCount()
    {
        return _jViewList.size();
    } //-- int getJViewCount() 

    /**
     * Method getRasterpic
     * 
     * @param index
     */
    public com.hellomagazine.jbag.Rasterpic getRasterpic(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _rasterpicList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hellomagazine.jbag.Rasterpic) _rasterpicList.get(index);
    } //-- com.hellomagazine.jbag.Rasterpic getRasterpic(int) 

    /**
     * Method getRasterpic
     */
    public com.hellomagazine.jbag.Rasterpic[] getRasterpic()
    {
        int size = _rasterpicList.size();
        com.hellomagazine.jbag.Rasterpic[] mArray = new com.hellomagazine.jbag.Rasterpic[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hellomagazine.jbag.Rasterpic) _rasterpicList.get(index);
        }
        return mArray;
    } //-- com.hellomagazine.jbag.Rasterpic[] getRasterpic() 

    /**
     * Method getRasterpicCount
     */
    public int getRasterpicCount()
    {
        return _rasterpicList.size();
    } //-- int getRasterpicCount() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeJView
     * 
     * @param vJView
     */
    public boolean removeJView(com.hellomagazine.jbag.JView vJView)
    {
        boolean removed = _jViewList.remove(vJView);
        return removed;
    } //-- boolean removeJView(com.hellomagazine.jbag.JView) 

    /**
     * Method removeRasterpic
     * 
     * @param vRasterpic
     */
    public boolean removeRasterpic(com.hellomagazine.jbag.Rasterpic vRasterpic)
    {
        boolean removed = _rasterpicList.remove(vRasterpic);
        return removed;
    } //-- boolean removeRasterpic(com.hellomagazine.jbag.Rasterpic) 

    /**
     * Sets the value of field 'description'.
     * 
     * @param description the value of field 'description'.
     */
    public void setDescription(com.hellomagazine.jbag.Description description)
    {
        this._description = description;
    } //-- void setDescription(com.hellomagazine.jbag.Description) 

    /**
     * Sets the value of field 'jList'.
     * 
     * @param jList the value of field 'jList'.
     */
    public void setJList(com.hellomagazine.jbag.JList jList)
    {
        this._jList = jList;
    } //-- void setJList(com.hellomagazine.jbag.JList) 

    /**
     * Method setJView
     * 
     * @param index
     * @param vJView
     */
    public void setJView(int index, com.hellomagazine.jbag.JView vJView)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _jViewList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _jViewList.set(index, vJView);
    } //-- void setJView(int, com.hellomagazine.jbag.JView) 

    /**
     * Method setJView
     * 
     * @param JViewArray
     */
    public void setJView(com.hellomagazine.jbag.JView[] JViewArray)
    {
        //-- copy array
        _jViewList.clear();
        for (int i = 0; i < JViewArray.length; i++) {
            _jViewList.add(JViewArray[i]);
        }
    } //-- void setJView(com.hellomagazine.jbag.JView) 

    /**
     * Method setRasterpic
     * 
     * @param index
     * @param vRasterpic
     */
    public void setRasterpic(int index, com.hellomagazine.jbag.Rasterpic vRasterpic)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _rasterpicList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _rasterpicList.set(index, vRasterpic);
    } //-- void setRasterpic(int, com.hellomagazine.jbag.Rasterpic) 

    /**
     * Method setRasterpic
     * 
     * @param rasterpicArray
     */
    public void setRasterpic(com.hellomagazine.jbag.Rasterpic[] rasterpicArray)
    {
        //-- copy array
        _rasterpicList.clear();
        for (int i = 0; i < rasterpicArray.length; i++) {
            _rasterpicList.add(rasterpicArray[i]);
        }
    } //-- void setRasterpic(com.hellomagazine.jbag.Rasterpic) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.jbag.Jbag) Unmarshaller.unmarshal(com.hellomagazine.jbag.Jbag.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
