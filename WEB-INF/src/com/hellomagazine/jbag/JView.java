/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.5.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.jbag;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class JView.
 * 
 * @version $Revision$ $Date$
 */
public class JView extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _name
     */
    private java.lang.String _name;

    /**
     * Field _resource
     */
    private java.lang.Integer _resource;

    /**
     * Field _pieceList
     */
    private java.util.ArrayList _pieceList;


      //----------------/
     //- Constructors -/
    //----------------/

    public JView() {
        super();
        _pieceList = new ArrayList();
    } //-- com.hellomagazine.jbag.JView()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addPiece
     * 
     * @param vPiece
     */
    public void addPiece(com.hellomagazine.jbag.Piece vPiece)
        throws java.lang.IndexOutOfBoundsException
    {
        _pieceList.add(vPiece);
    } //-- void addPiece(com.hellomagazine.jbag.Piece) 

    /**
     * Method addPiece
     * 
     * @param index
     * @param vPiece
     */
    public void addPiece(int index, com.hellomagazine.jbag.Piece vPiece)
        throws java.lang.IndexOutOfBoundsException
    {
        _pieceList.add(index, vPiece);
    } //-- void addPiece(int, com.hellomagazine.jbag.Piece) 

    /**
     * Method clearPiece
     */
    public void clearPiece()
    {
        _pieceList.clear();
    } //-- void clearPiece() 

    /**
     * Method enumeratePiece
     */
    public java.util.Enumeration enumeratePiece()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_pieceList.iterator());
    } //-- java.util.Enumeration enumeratePiece() 

    /**
     * Returns the value of field 'name'.
     * 
     * @return the value of field 'name'.
     */
    public java.lang.String getName()
    {
        return this._name;
    } //-- java.lang.String getName() 

    /**
     * Method getPiece
     * 
     * @param index
     */
    public com.hellomagazine.jbag.Piece getPiece(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _pieceList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hellomagazine.jbag.Piece) _pieceList.get(index);
    } //-- com.hellomagazine.jbag.Piece getPiece(int) 

    /**
     * Method getPiece
     */
    public com.hellomagazine.jbag.Piece[] getPiece()
    {
        int size = _pieceList.size();
        com.hellomagazine.jbag.Piece[] mArray = new com.hellomagazine.jbag.Piece[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hellomagazine.jbag.Piece) _pieceList.get(index);
        }
        return mArray;
    } //-- com.hellomagazine.jbag.Piece[] getPiece() 

    /**
     * Method getPieceCount
     */
    public int getPieceCount()
    {
        return _pieceList.size();
    } //-- int getPieceCount() 

    /**
     * Returns the value of field 'resource'.
     * 
     * @return the value of field 'resource'.
     */
    public java.lang.Integer getResource()
    {
        return this._resource;
    } //-- java.lang.Integer getResource() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removePiece
     * 
     * @param vPiece
     */
    public boolean removePiece(com.hellomagazine.jbag.Piece vPiece)
    {
        boolean removed = _pieceList.remove(vPiece);
        return removed;
    } //-- boolean removePiece(com.hellomagazine.jbag.Piece) 

    /**
     * Sets the value of field 'name'.
     * 
     * @param name the value of field 'name'.
     */
    public void setName(java.lang.String name)
    {
        this._name = name;
    } //-- void setName(java.lang.String) 

    /**
     * Method setPiece
     * 
     * @param index
     * @param vPiece
     */
    public void setPiece(int index, com.hellomagazine.jbag.Piece vPiece)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _pieceList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _pieceList.set(index, vPiece);
    } //-- void setPiece(int, com.hellomagazine.jbag.Piece) 

    /**
     * Method setPiece
     * 
     * @param pieceArray
     */
    public void setPiece(com.hellomagazine.jbag.Piece[] pieceArray)
    {
        //-- copy array
        _pieceList.clear();
        for (int i = 0; i < pieceArray.length; i++) {
            _pieceList.add(pieceArray[i]);
        }
    } //-- void setPiece(com.hellomagazine.jbag.Piece) 

    /**
     * Sets the value of field 'resource'.
     * 
     * @param resource the value of field 'resource'.
     */
    public void setResource(java.lang.Integer resource)
    {
        this._resource = resource;
    } //-- void setResource(java.lang.Integer) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static java.lang.Object unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.jbag.JView) Unmarshaller.unmarshal(com.hellomagazine.jbag.JView.class, reader);
    } //-- java.lang.Object unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
