/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.9</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.newsticker;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Headline.
 * 
 * @version $Revision$ $Date$
 */
public class Headline extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _order
     */
    private java.lang.Integer _order;

    /**
     * Field _id
     */
    private java.lang.Integer _id;

    /**
     * Field _type
     */
    private java.lang.String _type;

    /**
     * Field _text
     */
    private java.lang.String _text;

    /**
     * Field _deck
     */
    private java.lang.String _deck;

    /**
     * Field _byline
     */
    private java.lang.String _byline;

    /**
     * Field _teaserList
     */
    private java.util.ArrayList _teaserList;

    /**
     * Field _URL
     */
    private java.lang.String _URL;

    /**
     * Field _summary
     */
    private java.lang.String _summary;

    /**
     * Field _fulltext
     */
    private java.lang.String _fulltext;

    /**
     * Field _imageList
     */
    private java.util.ArrayList _imageList;

    /**
     * Field _captionList
     */
    private java.util.ArrayList _captionList;

    /**
     * Field _relatedList
     */
    private java.util.ArrayList _relatedList;

    /**
     * Field _created
     */
    private java.util.Date _created;

    /**
     * Field _editor
     */
    private java.util.Date _editor;

    /**
     * Field _modified
     */
    private java.util.Date _modified;

    /**
     * Field _live
     */
    private java.util.Date _live;

    /**
     * Field _expire
     */
    private java.util.Date _expire;


      //----------------/
     //- Constructors -/
    //----------------/

    public Headline() 
     {
        super();
        _teaserList = new ArrayList();
        _imageList = new ArrayList();
        _captionList = new ArrayList();
        _relatedList = new ArrayList();
    } //-- com.hellomagazine.newsticker.Headline()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addCaption
     * 
     * 
     * 
     * @param vCaption
     */
    public void addCaption(java.lang.String vCaption)
        throws java.lang.IndexOutOfBoundsException
    {
        _captionList.add(vCaption);
    } //-- void addCaption(java.lang.String) 

    /**
     * Method addCaption
     * 
     * 
     * 
     * @param index
     * @param vCaption
     */
    public void addCaption(int index, java.lang.String vCaption)
        throws java.lang.IndexOutOfBoundsException
    {
        _captionList.add(index, vCaption);
    } //-- void addCaption(int, java.lang.String) 

    /**
     * Method addImage
     * 
     * 
     * 
     * @param vImage
     */
    public void addImage(java.lang.String vImage)
        throws java.lang.IndexOutOfBoundsException
    {
        _imageList.add(vImage);
    } //-- void addImage(java.lang.String) 

    /**
     * Method addImage
     * 
     * 
     * 
     * @param index
     * @param vImage
     */
    public void addImage(int index, java.lang.String vImage)
        throws java.lang.IndexOutOfBoundsException
    {
        _imageList.add(index, vImage);
    } //-- void addImage(int, java.lang.String) 

    /**
     * Method addRelated
     * 
     * 
     * 
     * @param vRelated
     */
    public void addRelated(java.lang.String vRelated)
        throws java.lang.IndexOutOfBoundsException
    {
        _relatedList.add(vRelated);
    } //-- void addRelated(java.lang.String) 

    /**
     * Method addRelated
     * 
     * 
     * 
     * @param index
     * @param vRelated
     */
    public void addRelated(int index, java.lang.String vRelated)
        throws java.lang.IndexOutOfBoundsException
    {
        _relatedList.add(index, vRelated);
    } //-- void addRelated(int, java.lang.String) 

    /**
     * Method addTeaser
     * 
     * 
     * 
     * @param vTeaser
     */
    public void addTeaser(java.lang.String vTeaser)
        throws java.lang.IndexOutOfBoundsException
    {
        _teaserList.add(vTeaser);
    } //-- void addTeaser(java.lang.String) 

    /**
     * Method addTeaser
     * 
     * 
     * 
     * @param index
     * @param vTeaser
     */
    public void addTeaser(int index, java.lang.String vTeaser)
        throws java.lang.IndexOutOfBoundsException
    {
        _teaserList.add(index, vTeaser);
    } //-- void addTeaser(int, java.lang.String) 

    /**
     * Method clearCaption
     * 
     */
    public void clearCaption()
    {
        _captionList.clear();
    } //-- void clearCaption() 

    /**
     * Method clearImage
     * 
     */
    public void clearImage()
    {
        _imageList.clear();
    } //-- void clearImage() 

    /**
     * Method clearRelated
     * 
     */
    public void clearRelated()
    {
        _relatedList.clear();
    } //-- void clearRelated() 

    /**
     * Method clearTeaser
     * 
     */
    public void clearTeaser()
    {
        _teaserList.clear();
    } //-- void clearTeaser() 

    /**
     * Method enumerateCaption
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateCaption()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_captionList.iterator());
    } //-- java.util.Enumeration enumerateCaption() 

    /**
     * Method enumerateImage
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateImage()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_imageList.iterator());
    } //-- java.util.Enumeration enumerateImage() 

    /**
     * Method enumerateRelated
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateRelated()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_relatedList.iterator());
    } //-- java.util.Enumeration enumerateRelated() 

    /**
     * Method enumerateTeaser
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateTeaser()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_teaserList.iterator());
    } //-- java.util.Enumeration enumerateTeaser() 

    /**
     * Returns the value of field 'byline'.
     * 
     * @return String
     * @return the value of field 'byline'.
     */
    public java.lang.String getByline()
    {
        return this._byline;
    } //-- java.lang.String getByline() 

    /**
     * Method getCaption
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String getCaption(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _captionList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_captionList.get(index);
    } //-- java.lang.String getCaption(int) 

    /**
     * Method getCaption
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String[] getCaption()
    {
        int size = _captionList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_captionList.get(index);
        }
        return mArray;
    } //-- java.lang.String[] getCaption() 

    /**
     * Method getCaptionCount
     * 
     * 
     * 
     * @return int
     */
    public int getCaptionCount()
    {
        return _captionList.size();
    } //-- int getCaptionCount() 

    /**
     * Returns the value of field 'created'.
     * 
     * @return Date
     * @return the value of field 'created'.
     */
    public java.util.Date getCreated()
    {
        return this._created;
    } //-- java.util.Date getCreated() 

    /**
     * Returns the value of field 'deck'.
     * 
     * @return String
     * @return the value of field 'deck'.
     */
    public java.lang.String getDeck()
    {
        return this._deck;
    } //-- java.lang.String getDeck() 

    /**
     * Returns the value of field 'editor'.
     * 
     * @return Date
     * @return the value of field 'editor'.
     */
    public java.util.Date getEditor()
    {
        return this._editor;
    } //-- java.util.Date getEditor() 

    /**
     * Returns the value of field 'expire'.
     * 
     * @return Date
     * @return the value of field 'expire'.
     */
    public java.util.Date getExpire()
    {
        return this._expire;
    } //-- java.util.Date getExpire() 

    /**
     * Returns the value of field 'fulltext'.
     * 
     * @return String
     * @return the value of field 'fulltext'.
     */
    public java.lang.String getFulltext()
    {
        return this._fulltext;
    } //-- java.lang.String getFulltext() 

    /**
     * Returns the value of field 'id'.
     * 
     * @return Integer
     * @return the value of field 'id'.
     */
    public java.lang.Integer getId()
    {
        return this._id;
    } //-- java.lang.Integer getId() 

    /**
     * Method getImage
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String getImage(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _imageList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_imageList.get(index);
    } //-- java.lang.String getImage(int) 

    /**
     * Method getImage
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String[] getImage()
    {
        int size = _imageList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_imageList.get(index);
        }
        return mArray;
    } //-- java.lang.String[] getImage() 

    /**
     * Method getImageCount
     * 
     * 
     * 
     * @return int
     */
    public int getImageCount()
    {
        return _imageList.size();
    } //-- int getImageCount() 

    /**
     * Returns the value of field 'live'.
     * 
     * @return Date
     * @return the value of field 'live'.
     */
    public java.util.Date getLive()
    {
        return this._live;
    } //-- java.util.Date getLive() 

    /**
     * Returns the value of field 'modified'.
     * 
     * @return Date
     * @return the value of field 'modified'.
     */
    public java.util.Date getModified()
    {
        return this._modified;
    } //-- java.util.Date getModified() 

    /**
     * Returns the value of field 'order'.
     * 
     * @return Integer
     * @return the value of field 'order'.
     */
    public java.lang.Integer getOrder()
    {
        return this._order;
    } //-- java.lang.Integer getOrder() 

    /**
     * Method getRelated
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String getRelated(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _relatedList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_relatedList.get(index);
    } //-- java.lang.String getRelated(int) 

    /**
     * Method getRelated
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String[] getRelated()
    {
        int size = _relatedList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_relatedList.get(index);
        }
        return mArray;
    } //-- java.lang.String[] getRelated() 

    /**
     * Method getRelatedCount
     * 
     * 
     * 
     * @return int
     */
    public int getRelatedCount()
    {
        return _relatedList.size();
    } //-- int getRelatedCount() 

    /**
     * Returns the value of field 'summary'.
     * 
     * @return String
     * @return the value of field 'summary'.
     */
    public java.lang.String getSummary()
    {
        return this._summary;
    } //-- java.lang.String getSummary() 

    /**
     * Method getTeaser
     * 
     * 
     * 
     * @param index
     * @return String
     */
    public java.lang.String getTeaser(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _teaserList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (String)_teaserList.get(index);
    } //-- java.lang.String getTeaser(int) 

    /**
     * Method getTeaser
     * 
     * 
     * 
     * @return String
     */
    public java.lang.String[] getTeaser()
    {
        int size = _teaserList.size();
        java.lang.String[] mArray = new java.lang.String[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (String)_teaserList.get(index);
        }
        return mArray;
    } //-- java.lang.String[] getTeaser() 

    /**
     * Method getTeaserCount
     * 
     * 
     * 
     * @return int
     */
    public int getTeaserCount()
    {
        return _teaserList.size();
    } //-- int getTeaserCount() 

    /**
     * Returns the value of field 'text'.
     * 
     * @return String
     * @return the value of field 'text'.
     */
    public java.lang.String getText()
    {
        return this._text;
    } //-- java.lang.String getText() 

    /**
     * Returns the value of field 'type'.
     * 
     * @return String
     * @return the value of field 'type'.
     */
    public java.lang.String getType()
    {
        return this._type;
    } //-- java.lang.String getType() 

    /**
     * Returns the value of field 'URL'.
     * 
     * @return String
     * @return the value of field 'URL'.
     */
    public java.lang.String getURL()
    {
        return this._URL;
    } //-- java.lang.String getURL() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeCaption
     * 
     * 
     * 
     * @param vCaption
     * @return boolean
     */
    public boolean removeCaption(java.lang.String vCaption)
    {
        boolean removed = _captionList.remove(vCaption);
        return removed;
    } //-- boolean removeCaption(java.lang.String) 

    /**
     * Method removeImage
     * 
     * 
     * 
     * @param vImage
     * @return boolean
     */
    public boolean removeImage(java.lang.String vImage)
    {
        boolean removed = _imageList.remove(vImage);
        return removed;
    } //-- boolean removeImage(java.lang.String) 

    /**
     * Method removeRelated
     * 
     * 
     * 
     * @param vRelated
     * @return boolean
     */
    public boolean removeRelated(java.lang.String vRelated)
    {
        boolean removed = _relatedList.remove(vRelated);
        return removed;
    } //-- boolean removeRelated(java.lang.String) 

    /**
     * Method removeTeaser
     * 
     * 
     * 
     * @param vTeaser
     * @return boolean
     */
    public boolean removeTeaser(java.lang.String vTeaser)
    {
        boolean removed = _teaserList.remove(vTeaser);
        return removed;
    } //-- boolean removeTeaser(java.lang.String) 

    /**
     * Sets the value of field 'byline'.
     * 
     * @param byline the value of field 'byline'.
     */
    public void setByline(java.lang.String byline)
    {
        this._byline = byline;
    } //-- void setByline(java.lang.String) 

    /**
     * Method setCaption
     * 
     * 
     * 
     * @param index
     * @param vCaption
     */
    public void setCaption(int index, java.lang.String vCaption)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _captionList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _captionList.set(index, vCaption);
    } //-- void setCaption(int, java.lang.String) 

    /**
     * Method setCaption
     * 
     * 
     * 
     * @param captionArray
     */
    public void setCaption(java.lang.String[] captionArray)
    {
        //-- copy array
        _captionList.clear();
        for (int i = 0; i < captionArray.length; i++) {
            _captionList.add(captionArray[i]);
        }
    } //-- void setCaption(java.lang.String) 

    /**
     * Sets the value of field 'created'.
     * 
     * @param created the value of field 'created'.
     */
    public void setCreated(java.util.Date created)
    {
        this._created = created;
    } //-- void setCreated(java.util.Date) 

    /**
     * Sets the value of field 'deck'.
     * 
     * @param deck the value of field 'deck'.
     */
    public void setDeck(java.lang.String deck)
    {
        this._deck = deck;
    } //-- void setDeck(java.lang.String) 

    /**
     * Sets the value of field 'editor'.
     * 
     * @param editor the value of field 'editor'.
     */
    public void setEditor(java.util.Date editor)
    {
        this._editor = editor;
    } //-- void setEditor(java.util.Date) 

    /**
     * Sets the value of field 'expire'.
     * 
     * @param expire the value of field 'expire'.
     */
    public void setExpire(java.util.Date expire)
    {
        this._expire = expire;
    } //-- void setExpire(java.util.Date) 

    /**
     * Sets the value of field 'fulltext'.
     * 
     * @param fulltext the value of field 'fulltext'.
     */
    public void setFulltext(java.lang.String fulltext)
    {
        this._fulltext = fulltext;
    } //-- void setFulltext(java.lang.String) 

    /**
     * Sets the value of field 'id'.
     * 
     * @param id the value of field 'id'.
     */
    public void setId(java.lang.Integer id)
    {
        this._id = id;
    } //-- void setId(java.lang.Integer) 

    /**
     * Method setImage
     * 
     * 
     * 
     * @param index
     * @param vImage
     */
    public void setImage(int index, java.lang.String vImage)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _imageList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _imageList.set(index, vImage);
    } //-- void setImage(int, java.lang.String) 

    /**
     * Method setImage
     * 
     * 
     * 
     * @param imageArray
     */
    public void setImage(java.lang.String[] imageArray)
    {
        //-- copy array
        _imageList.clear();
        for (int i = 0; i < imageArray.length; i++) {
            _imageList.add(imageArray[i]);
        }
    } //-- void setImage(java.lang.String) 

    /**
     * Sets the value of field 'live'.
     * 
     * @param live the value of field 'live'.
     */
    public void setLive(java.util.Date live)
    {
        this._live = live;
    } //-- void setLive(java.util.Date) 

    /**
     * Sets the value of field 'modified'.
     * 
     * @param modified the value of field 'modified'.
     */
    public void setModified(java.util.Date modified)
    {
        this._modified = modified;
    } //-- void setModified(java.util.Date) 

    /**
     * Sets the value of field 'order'.
     * 
     * @param order the value of field 'order'.
     */
    public void setOrder(java.lang.Integer order)
    {
        this._order = order;
    } //-- void setOrder(java.lang.Integer) 

    /**
     * Method setRelated
     * 
     * 
     * 
     * @param index
     * @param vRelated
     */
    public void setRelated(int index, java.lang.String vRelated)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _relatedList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _relatedList.set(index, vRelated);
    } //-- void setRelated(int, java.lang.String) 

    /**
     * Method setRelated
     * 
     * 
     * 
     * @param relatedArray
     */
    public void setRelated(java.lang.String[] relatedArray)
    {
        //-- copy array
        _relatedList.clear();
        for (int i = 0; i < relatedArray.length; i++) {
            _relatedList.add(relatedArray[i]);
        }
    } //-- void setRelated(java.lang.String) 

    /**
     * Sets the value of field 'summary'.
     * 
     * @param summary the value of field 'summary'.
     */
    public void setSummary(java.lang.String summary)
    {
        this._summary = summary;
    } //-- void setSummary(java.lang.String) 

    /**
     * Method setTeaser
     * 
     * 
     * 
     * @param index
     * @param vTeaser
     */
    public void setTeaser(int index, java.lang.String vTeaser)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _teaserList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _teaserList.set(index, vTeaser);
    } //-- void setTeaser(int, java.lang.String) 

    /**
     * Method setTeaser
     * 
     * 
     * 
     * @param teaserArray
     */
    public void setTeaser(java.lang.String[] teaserArray)
    {
        //-- copy array
        _teaserList.clear();
        for (int i = 0; i < teaserArray.length; i++) {
            _teaserList.add(teaserArray[i]);
        }
    } //-- void setTeaser(java.lang.String) 

    /**
     * Sets the value of field 'text'.
     * 
     * @param text the value of field 'text'.
     */
    public void setText(java.lang.String text)
    {
        this._text = text;
    } //-- void setText(java.lang.String) 

    /**
     * Sets the value of field 'type'.
     * 
     * @param type the value of field 'type'.
     */
    public void setType(java.lang.String type)
    {
        this._type = type;
    } //-- void setType(java.lang.String) 

    /**
     * Sets the value of field 'URL'.
     * 
     * @param URL the value of field 'URL'.
     */
    public void setURL(java.lang.String URL)
    {
        this._URL = URL;
    } //-- void setURL(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Headline
     */
    public static com.hellomagazine.newsticker.Headline unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.newsticker.Headline) Unmarshaller.unmarshal(com.hellomagazine.newsticker.Headline.class, reader);
    } //-- com.hellomagazine.newsticker.Headline unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
