/*
 * Created on 25-may-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hellomagazine.newsticker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import com.hola.ed.util.ISOCalendar;
import com.hola.feeds.atom.Entry;
import com.hola.feeds.atom.Feed;
import com.hola.feeds.atom.Link;
import com.hola.feeds.atom.types.LinkTypeRelType;
import com.hola.util.text.Filter;

/**
 * @author armando
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Ticker2Atom {
    public static final String BASE= "http://www.hellomagazine.com";
    public static final int BASE_LENGTH= BASE.length();
    
	public static Feed convert(Ticker ticker, String wrapper) throws IOException{
		List file= null;
		Feed f= new Feed();
		wrapper= wrapper == null ? "" : wrapper;

		f.setTitle("hellomagazine.com");
		Link l= new Link();
        
		l.setHref(BASE);
		l.setRel(LinkTypeRelType.valueOf("alternate"));
		l.setType("text/html");
		f.setModified((new Date()).toString());
		f.setVersion("0.3");
		
//		System.out.println("Converting "+ticker.getHeadlineCount()+" headlines");
		for (int i=0; i < ticker.getHeadlineCount(); i++){
			Headline h= ticker.getHeadline(i);
			if (h == null) 
			    continue;
			String url= h.getURL();
			if (url.equals("")){
				url= BASE+"/?"+Math.random();
			}
			if (url.startsWith(BASE)){
			    url = url.substring(BASE_LENGTH);
			}
			url= BASE+ wrapper + url;
			Entry news= new Entry();
			Date d= new Date();
			ISOCalendar ic= new ISOCalendar();
			ic.setTime(h.getCreated() != null ? h.getCreated() : d);
			news.setIssued(ic.toString());
			ic.setTime(h.getModified() != null ? h.getModified() : d);
			news.setModified(ic.toString());
			news.setTitle(Filter.toUnicode(h.getText()));
			news.setId(url);
			Link li= new Link();
			li.setHref(url);
			li.setRel(LinkTypeRelType.valueOf("alternate"));
//			System.out.println(h.getType());
			li.setType(h.getType());
			news.addLink(li);
			f.addEntry(news);
		}
		
		try {
            f.marshal(new StringWriter());
        }
        catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		return (f.isValid()) ? f : null; 
	} 

	public static byte[] aString(Ticker t, String wrapper) throws IOException{
	    Feed f= convert(t, wrapper);
	    ByteArrayOutputStream bo= new ByteArrayOutputStream();
	    try{
            f.marshal(new OutputStreamWriter(bo, "UTF8"));
        } catch (MarshalException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	    return bo.toByteArray();	    
	}
}
