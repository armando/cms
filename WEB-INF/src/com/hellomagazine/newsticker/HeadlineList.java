/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.4.3</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.newsticker;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class HeadlineList.
 * 
 * @version $Revision$ $Date$
 */
public class HeadlineList extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _idList
     */
    private java.util.ArrayList _idList;


      //----------------/
     //- Constructors -/
    //----------------/

    public HeadlineList() {
        super();
        _idList = new ArrayList();
    } //-- com.hellomagazine.newsticker.HeadlineList()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addId
     * 
     * @param vId
     */
    public void addId(java.lang.Integer vId)
        throws java.lang.IndexOutOfBoundsException
    {
        _idList.add(vId);
    } //-- void addId(java.lang.Integer) 

    /**
     * Method addId
     * 
     * @param index
     * @param vId
     */
    public void addId(int index, java.lang.Integer vId)
        throws java.lang.IndexOutOfBoundsException
    {
        _idList.add(index, vId);
    } //-- void addId(int, java.lang.Integer) 

    /**
     * Method clearId
     */
    public void clearId()
    {
        _idList.clear();
    } //-- void clearId() 

    /**
     * Method enumerateId
     */
    public java.util.Enumeration enumerateId()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_idList.iterator());
    } //-- java.util.Enumeration enumerateId() 

    /**
     * Method getId
     * 
     * @param index
     */
    public java.lang.Integer getId(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _idList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return ((Integer)_idList.get(index));
    } //-- java.lang.Integer getId(int) 

    /**
     * Method getId
     */
    public java.lang.Integer[] getId()
    {
        int size = _idList.size();
        java.lang.Integer[] mArray = new java.lang.Integer[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = ((Integer)_idList.get(index));
        }
        return mArray;
    } //-- java.lang.Integer[] getId() 

    /**
     * Method getIdCount
     */
    public int getIdCount()
    {
        return _idList.size();
    } //-- int getIdCount() 

    /**
     * Method isValid
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeId
     * 
     * @param vId
     */
    public boolean removeId(java.lang.Integer vId)
    {
        boolean removed = _idList.remove(vId);
        return removed;
    } //-- boolean removeId(java.lang.Integer) 

    /**
     * Method setId
     * 
     * @param index
     * @param vId
     */
    public void setId(int index, java.lang.Integer vId)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _idList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _idList.set(index, vId);
    } //-- void setId(int, java.lang.Integer) 

    /**
     * Method setId
     * 
     * @param idArray
     */
    public void setId(java.lang.Integer[] idArray)
    {
        //-- copy array
        _idList.clear();
        for (int i = 0; i < idArray.length; i++) {
            _idList.add(idArray[i]);
        }
    } //-- void setId(java.lang.Integer) 

    /**
     * Method unmarshal
     * 
     * @param reader
     */
    public static com.hellomagazine.newsticker.HeadlineList unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.newsticker.HeadlineList) Unmarshaller.unmarshal(com.hellomagazine.newsticker.HeadlineList.class, reader);
    } //-- com.hellomagazine.newsticker.HeadlineList unmarshal(java.io.Reader) 

    /**
     * Method validate
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
