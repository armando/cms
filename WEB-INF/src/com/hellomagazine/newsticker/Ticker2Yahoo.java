/*
 * Created on 25-may-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hellomagazine.newsticker;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import org.apache.xml.serialize.Method;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.DocumentHandler;

import com.hellomagazine.yahoo.Article;
import com.hellomagazine.yahoo.Articles;
import com.hellomagazine.yahoo.Images;
import com.hola.ed.util.ISOCalendar;
import com.hola.util.text.Filter;

/**
 * @author armando
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Ticker2Yahoo {
    public static final String BASE = "http://www.hellomagazine.com";

    public static final int BASE_LENGTH = BASE.length();

    public static Articles convert(Ticker ticker, String wrapper)
            throws IOException {
        List file = null;
        Articles f = new Articles();
        wrapper = wrapper == null ? "" : wrapper;

        //		System.out.println("Converting "+ticker.getHeadlineCount()+"
        // headlines");
        long time = System.currentTimeMillis();
        for (int i = 0, n = ticker.getHeadlineCount(); i < n; i++) {
            Headline h = ticker.getHeadline(i);
            if (h == null)
                continue;
            String url = h.getURL();
            if (url.equals("")) {
                url = BASE + "/?" + Math.random();
            }
            if (url.startsWith(BASE)) {
                url = url.substring(BASE_LENGTH);
            }
            url = BASE + wrapper + url;
            Article news = new Article();
            Date d = new Date();
            ISOCalendar ic = new ISOCalendar();
            //            System.out.println(h.getLive()!= null ?
            // h.getLive().toLocaleString(): "null");
            ic.setTime(h.getLive() != null ? h.getLive() : d);
            ISOCalendar order = new ISOCalendar();
            order.setTimeInMillis(time - 300000 * i);
            news.setDate(ic.toString());
            news.setHeadline(Filter.toUnicode(h.getText()));
            news.setUrl(url);
            news.setAuthor("hellomagazine.com");
            news.setSummary(h.getSummary());
            news.setStory(h.getFulltext());
            news.setRelated(h.getRelated());
            if (i == 0) { // Limitacion actual: 1 foto
                if (h.getCaptionCount() > 0) {
                    news.setCaption_photo_1(h.getCaption(0));
                }
                if (h.getCaptionCount() > 1) {
                    news.setCaption_photo_2(h.getCaption(1));
                }
                if (h.getImageCount() > 0) {
                    String nombre = h.getText();
                    int space = nombre.indexOf(' ');
                    nombre = (space > 0) ? nombre.substring(0, space) : nombre;
                    System.out.println(ic.toString());
                    String nom = Filter.filename(nombre+"_"+ic.toString());
                    System.out.println(nom);
                    Images ims = new Images();
                    // Atencion al absurdo: Maarten Beerge de Yahoo necesita las
                    // imagenes en orden inverso...
                    for (int j = h.getImageCount() - 1, m = 0; j >= m; j--) {
                        if (j < 2) {
                            ims.addImage(nom + "_" +  i
                                    + "_" + j + ".jpg");
                        }
                    }
                    news.setImages(ims);
                }
            }
            f.addArticle(news);
        }

        try {
            f.marshal(new StringWriter());
        } catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return (f.isValid()) ? f : null;
    }

    public static byte[] aString(Ticker t, String wrapper) throws IOException {
        Articles f = convert(t, wrapper);
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        try {
            OutputStreamWriter out = new OutputStreamWriter(bo, "UTF-8");
            OutputFormat format = new OutputFormat(Method.XML, "UTF-8", true);
            String[] cdata = { "provider", "summary", "headline", "story" };
            format.setCDataElements(cdata);
            //format.setNonEscapingElements(cdata);
            //format.setPreserveSpace(true);

            XMLSerializer serializer = new XMLSerializer(out, format);
            DocumentHandler handler = serializer.asDocumentHandler();

            Marshaller marshaller = new Marshaller(handler);

            marshaller.marshal(f);
        } catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bo.toByteArray();
    }

    public static void main(String[] args) {
        try {
            Ticker t = (Ticker) Ticker.unmarshal(new FileReader(
                    "/www/data/hello/newsticker/yahoo.xml"));
            System.out.println(new String(Ticker2Yahoo.aString(t, null)));
        } catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
