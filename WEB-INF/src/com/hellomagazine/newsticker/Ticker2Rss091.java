/*
 * Created on 25-may-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hellomagazine.newsticker;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import com.hola.feeds.rss091.Channel;
import com.hola.feeds.rss091.Image;
import com.hola.feeds.rss091.Item;
import com.hola.feeds.rss091.Rss;

/**
 * @author armando
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Ticker2Rss091 {
    private static Locale locale= new Locale("en", "GB");
    private static SimpleDateFormat df=
        new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", locale);
    private static Map context= new HashMap();    
              
	public static Rss convert(Ticker ticker) throws IOException{
		List file= null;
        setContext();
		Rss f= new Rss();

		f.setVersion("0.91");
        Channel c= new Channel();
        
        c.setTitle("Hello Magazine");
		c.setLink("http://www.hellomagazine.com");
        c.setDescription("Celebrity news");
        c.setLastBuildDate(df.format(new Date()));
        c.setCopyright("Hello Magazine 2004");
        c.setLanguage("en-gb");
        Image im= new Image();
        im.setTitle("Hello");
        im.setUrl("http://www.hellomagazine.com/graphics/logo_helloNEW.gif");
        im.setLink("http://www.hellomagazine.com");
        c.setImage(im);
             
        int items= ticker.getHeadlineCount();
        items= Math.min(items, 15);
		for (int i=0; i < items; i++){
			Headline h= complete(ticker.getHeadline(i));
			String url= h.getURL();
			if (url.equals("")){
				continue;
			}
			Item news= new Item();

			news.setTitle(h.getText());
            String description= (h.getSummary() != null && h.getSummary().length()>1) 
                ? h.getSummary() 
                : " ";
            news.setDescription(description);
			news.setLink(url);
			c.addItem(news);
//            System.out.println("Nuevo item :"+url+"-"+description+"-");
            }
		
        f.setChannel(c);
        
		try {
            f.marshal(new StringWriter());
        }
        catch (MarshalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (ValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		if (f.isValid()){
			return f;        
		}
		else{
			System.out.println("Feed no valido");
			return null;
		}

	} 
    
    private static void setContext(){
        Ticker ticker= null;
        try {
            ticker= (Ticker) Ticker.unmarshal(new FileReader("/www/data/hello/newsticker/headlines.xml"));
        }
        catch (MarshalException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        catch (ValidationException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        for (int i=0; i < ticker.getHeadlineCount(); i++){
            Headline h= ticker.getHeadline(i);
            Integer id= h.getId();
            context.put(id, h);
        }
    }
    
    private static Headline complete(Headline h){
        Headline hnew= new Headline();
        Headline hcon= (Headline) context.get(h.getId());
        hnew.setId(h.getId());
//        hnew.setModified(h.getModified()!= null ? h.getModified(): hcon.getModified());
        hnew.setSummary(h.getSummary()!= null ? h.getSummary(): hcon.getSummary());
        hnew.setText(h.getText()!= null ? h.getText(): hcon.getText());
        hnew.setURL(h.getURL()!= null ? h.getURL(): hcon.getURL());
        hnew.setCreated(h.getCreated()!= null ? h.getCreated(): hcon.getCreated());        
        return hnew;
    }
    
}
