/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 0.9.9</a>, using an XML
 * Schema.
 * $Id$
 */

package com.hellomagazine.newsticker;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ticker.
 * 
 * @version $Revision$ $Date$
 */
public class Ticker extends com.hola.Documento 
implements java.io.Serializable
{


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _id
     */
    private java.lang.String _id;

    /**
     * Field _headlineList
     */
    private java.util.ArrayList _headlineList;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ticker() 
     {
        super();
        _headlineList = new ArrayList();
    } //-- com.hellomagazine.newsticker.Ticker()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addHeadline
     * 
     * 
     * 
     * @param vHeadline
     */
    public void addHeadline(com.hellomagazine.newsticker.Headline vHeadline)
        throws java.lang.IndexOutOfBoundsException
    {
        _headlineList.add(vHeadline);
    } //-- void addHeadline(com.hellomagazine.newsticker.Headline) 

    /**
     * Method addHeadline
     * 
     * 
     * 
     * @param index
     * @param vHeadline
     */
    public void addHeadline(int index, com.hellomagazine.newsticker.Headline vHeadline)
        throws java.lang.IndexOutOfBoundsException
    {
        _headlineList.add(index, vHeadline);
    } //-- void addHeadline(int, com.hellomagazine.newsticker.Headline) 

    /**
     * Method clearHeadline
     * 
     */
    public void clearHeadline()
    {
        _headlineList.clear();
    } //-- void clearHeadline() 

    /**
     * Method enumerateHeadline
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateHeadline()
    {
        return new org.exolab.castor.util.IteratorEnumeration(_headlineList.iterator());
    } //-- java.util.Enumeration enumerateHeadline() 

    /**
     * Method getHeadline
     * 
     * 
     * 
     * @param index
     * @return Headline
     */
    public com.hellomagazine.newsticker.Headline getHeadline(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _headlineList.size())) {
            throw new IndexOutOfBoundsException();
        }
        
        return (com.hellomagazine.newsticker.Headline) _headlineList.get(index);
    } //-- com.hellomagazine.newsticker.Headline getHeadline(int) 

    /**
     * Method getHeadline
     * 
     * 
     * 
     * @return Headline
     */
    public com.hellomagazine.newsticker.Headline[] getHeadline()
    {
        int size = _headlineList.size();
        com.hellomagazine.newsticker.Headline[] mArray = new com.hellomagazine.newsticker.Headline[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (com.hellomagazine.newsticker.Headline) _headlineList.get(index);
        }
        return mArray;
    } //-- com.hellomagazine.newsticker.Headline[] getHeadline() 

    /**
     * Method getHeadlineCount
     * 
     * 
     * 
     * @return int
     */
    public int getHeadlineCount()
    {
        return _headlineList.size();
    } //-- int getHeadlineCount() 

    /**
     * Returns the value of field 'id'.
     * 
     * @return String
     * @return the value of field 'id'.
     */
    public java.lang.String getId()
    {
        return this._id;
    } //-- java.lang.String getId() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeHeadline
     * 
     * 
     * 
     * @param vHeadline
     * @return boolean
     */
    public boolean removeHeadline(com.hellomagazine.newsticker.Headline vHeadline)
    {
        boolean removed = _headlineList.remove(vHeadline);
        return removed;
    } //-- boolean removeHeadline(com.hellomagazine.newsticker.Headline) 

    /**
     * Method setHeadline
     * 
     * 
     * 
     * @param index
     * @param vHeadline
     */
    public void setHeadline(int index, com.hellomagazine.newsticker.Headline vHeadline)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > _headlineList.size())) {
            throw new IndexOutOfBoundsException();
        }
        _headlineList.set(index, vHeadline);
    } //-- void setHeadline(int, com.hellomagazine.newsticker.Headline) 

    /**
     * Method setHeadline
     * 
     * 
     * 
     * @param headlineArray
     */
    public void setHeadline(com.hellomagazine.newsticker.Headline[] headlineArray)
    {
        //-- copy array
        _headlineList.clear();
        for (int i = 0; i < headlineArray.length; i++) {
            _headlineList.add(headlineArray[i]);
        }
    } //-- void setHeadline(com.hellomagazine.newsticker.Headline) 

    /**
     * Sets the value of field 'id'.
     * 
     * @param id the value of field 'id'.
     */
    public void setId(java.lang.String id)
    {
        this._id = id;
    } //-- void setId(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ticker
     */
    public static com.hellomagazine.newsticker.Ticker unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (com.hellomagazine.newsticker.Ticker) Unmarshaller.unmarshal(com.hellomagazine.newsticker.Ticker.class, reader);
    } //-- com.hellomagazine.newsticker.Ticker unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
