/*
 * Created on 02-dic-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.hellomagazine.util.ftp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Random;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.hellomagazine.ed.ticker.TickerForm;
import com.hellomagazine.newsticker.Headline;
import com.hellomagazine.newsticker.Ticker;
import com.hellomagazine.newsticker.Ticker2Atom;
import com.hellomagazine.newsticker.Ticker2Yahoo;
import com.hola.ed.util.ISOCalendar;
import com.hola.util.text.Filter;
import com.mindprod.base64.Base64;

/**
 * @author armando
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FtpC {
    private String user = null, password = null, site = null;

    FTPClient ftp = null;

    public FtpC(String s, String u, String p, boolean open) {
        user = u;
        password = p;
        site = s;
        if (open) {
            ftp = new FTPClient();
            int reply;
            try {
                ftp.connect(site);
                ftp.enterLocalPassiveMode();
                System.out.print(ftp.getReplyString());
    
                ftp.login(user, password);
                ftp.setFileType(FTP.BINARY_FILE_TYPE);
                System.out.println("Connected to " + site + ".");
                System.out.print(ftp.getReplyString());
                System.out.println("Port: "+ftp.getPassivePort());

                // After connection attempt, you should check the reply code to
                // verify
                // success.
                reply = ftp.getReplyCode();

                if (!FTPReply.isPositiveCompletion(reply)) {
                    ftp.disconnect();
                    System.err.println("FTP server refused connection.");
                    System.exit(1);
                }
            } catch (IOException e) {
                if (ftp.isConnected()) {
                    try {
                        ftp.disconnect();
                    } catch (IOException f) {
                        // do nothing
                    }
                }
                System.err.println("Could not connect to server.");
                e.printStackTrace();
            }
        }
    }

    public String send(InputStream is, String directory, String filename,
            boolean opened) {
        boolean sent = false;
        if (!opened) {
            try {
                ftp = new FTPClient();
                int reply;
                ftp.connect(site);
            
                ftp.login(user, password);
                ftp.setFileType(FTP.BINARY_FILE_TYPE);
                System.out.println("Connected from send to " + site + ".");
                System.out.print(ftp.getReplyString());

                // After connection attempt, you should check the reply code to
                // verify
                // success.
                reply = ftp.getReplyCode();

                if (!FTPReply.isPositiveCompletion(reply)) {
                    ftp.disconnect();
                    System.err.println("FTP server refused connection.");
                    System.exit(1);
                }
            } catch (IOException e) {
                if (ftp.isConnected()) {
                    try {
                        ftp.disconnect();
                    } catch (IOException f) {
                        // do nothing
                    }
                }
                System.err.println("Could not connect to server.");
                e.printStackTrace();
            }
        }
        
        
        try {
            if (directory != null) {
                ftp.changeWorkingDirectory(directory);
                System.out.println("Cambiado directorio a "+directory);
            }
            
            try {
                sent = ftp.storeFile(filename, is);
                System.out.print(ftp.getReplyString());
                System.out.println("Enviado " + sent);
                System.out.println("Port: "+ftp.getPassivePort());
            } catch (Exception e) {
                System.out.println(e);
            }
        } catch (IOException e) {
            if (ftp.isConnected()) {
                try {
                    ftp.disconnect();
                } catch (IOException f) {
                    // do nothing
                }
            }
            System.err.println("Could not connect to server.");
            e.printStackTrace();
        }
        if (!opened){
            try {
                ftp.disconnect();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        return (sent ? "ok" : "error");
    }

    public static String sendNTL() {
        String result = sendNTLtest();
        FtpC fc = new FtpC("staging.ntlworld.com", "hello", "bash4u", false);
        //        URL url= new URL("http://app.hellomagazine.com/feeds/atom/cover/");
        //       FtpC fc= new FtpC("ftp.hola.com", "inouttv", "d3f4j9k0");

        try {
            //            InputStream is= url.openStream();
            Ticker tick = TickerForm.loadTicker("ntl");
            int total = tick.getHeadlineCount();
            Headline[] hs = new Headline[6];
            for (int i = 0; i < 6 && i < total; i++) {
                hs[i] = tick.getHeadline(i);

            }
            tick.setHeadline(hs); // Una forma pintoresca de dejar solo 6 (AR)
            byte[] b = Ticker2Atom.aString(tick, "/feeds/ntl");
            InputStream is = new ByteArrayInputStream(b);
            result = fc.send(is, "docs", "cover.xml", false);
            is.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Sent NTL to staging");
        return result;
    }

    public static String sendNTLtest() {
        String result = "error";

        try {
            FtpC fc = new FtpC("partner.ntlworld.com", "hello", "J2KkRky2",
                    false);
            //            InputStream is= url.openStream();
            Ticker tick = TickerForm.loadTicker("ntl");
            int total = tick.getHeadlineCount();
            Headline[] hs = new Headline[6];
            for (int i = 0; i < 6 && i < total; i++) {
                hs[i] = tick.getHeadline(i);

            }
            tick.setHeadline(hs);
            byte[] b = Ticker2Atom.aString(tick, "/feeds/ntl");
            InputStream is = new ByteArrayInputStream(b);
            result = fc.send(is, "docs", "cover.xml", false);
            System.out.println(new String(b));
            is.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Sent NTL to partners");
        return result;
    }

public static String sendYahoo() {
        String result = "error";
        FtpC fc= null;

        try {
            fc = new FtpC("anon.europe.yahoo.com", "hello", "lSao0YPHZ", true);
          
            //            InputStream is= url.openStream();
            Ticker tick = TickerForm.loadTicker("yahoo");
            int total = tick.getHeadlineCount();
            Headline[] hs = new Headline[total];

            for (int i = 0; i < total; i++) {
                hs[i] = tick.getHeadline(i);
                if (i == 0) { // Una sola imagen
                    ISOCalendar ic = new ISOCalendar();
                    ic.setTime(hs[i].getLive() != null ? hs[i].getLive()
                            : new Date());
                    String nombre = hs[i].getText();
                    int space = nombre.indexOf(' ');
                    nombre = (space > 0) ? nombre.substring(0, space) : nombre;
                    nombre = Filter.filename(nombre + "_" + ic.toString());
                    for (int j = 0, n = hs[i].getImageCount(); j < n; j++) {
                        Base64 encoder = new Base64();
                        byte[] image = encoder.decode(hs[i].getImage(j));
                        InputStream im = new ByteArrayInputStream(image);

                        System.out.println("Sending " + image.length
                                + " bytes from " + nombre + "_" + i + "_" + j);
                        String res = fc.send(im, null, nombre + "_" + i + "_"
                                + j + ".jpg", true);
                        im.close();
                    }
                }
            }
            tick.setHeadline(hs);
            byte[] b = Ticker2Yahoo.aString(tick, "");
            InputStream is = new ByteArrayInputStream(b);
            result = fc.send(is, null, "hello.xml", true);
            //System.out.println(new String(b));
            is.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Sent Yahoo to partners");
        try {
            fc.ftp.disconnect();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        return result;
    }    

public static String sendTesco() {
        String result = null;
        FtpC fc = new FtpC("62.253.166.194", "hello", "H3110_4ga1n", false);
        //        URL url= new URL("http://app.hellomagazine.com/feeds/atom/cover/");
        try {
            //            InputStream is= url.openStream();
            Ticker tick = TickerForm.loadTicker("tesco");
            int total = tick.getHeadlineCount();
            Headline[] hs = new Headline[6];
            for (int i = 0; i < 6 && i < total; i++) {
                hs[i] = tick.getHeadline(i);

            }
            tick.setHeadline(hs);
            byte[] b = Ticker2Atom.aString(tick, null);
            InputStream is = new ByteArrayInputStream(b);
            result = fc.send(is, "docs", "cover.xml", false);
            is.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) {
        sendYahoo();
    }

}