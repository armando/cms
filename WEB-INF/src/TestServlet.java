
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xmlrpc.XmlRpcServer;

/**
 * 
 * @author Armando Ramos <armando@hola.com>
 *  
 */

public class TestServlet extends HttpServlet {
    public void init(){
    }
    
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        System.out.println("message="+req.getParameter("message"));
        System.out.println("otros="+req.getParameter("otros"));
        for (Enumeration i= req.getHeaderNames(); i.hasMoreElements();){
            String name= (String) i.nextElement();
            System.out.println(name+":"+req.getHeader(name));
        }
        System.out.println(req.getCharacterEncoding());
        System.out.println(req.getContentType());
        
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
/*        byte[] result = xmlrpc.execute (req.getInputStream ());
        res.setContentType ("text/xml");
        res.setContentLength (result.length);
        OutputStream out = res.getOutputStream();
        out.write (result);
        out.flush ();
*/
                doGet(req, res);
    }
}