
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hellomagazine.ed.ticker.TickerForm;
import com.hellomagazine.newsticker.Ticker;
import com.mindprod.base64.Base64;

/**
 * 
 * @author Armando Ramos <armando@hola.com>
 *  
 */

public class ImageTickerServlet extends HttpServlet {
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        String imagenFile= req.getPathInfo();

        Pattern p1= Pattern
                .compile("/([^/]+)/([0-9]+)/([0-9x]+)\\.(jpg|gif|png|html)");
        Pattern p2= Pattern.compile("([0-9]+)(x([0-9]+))?");
        String tick= null, type= null, name= null, number= null;
        int num= 0;

        Matcher m1= p1.matcher(imagenFile);
        if (m1.matches()){
            tick= m1.group(1);
            number= m1.group(2);
            name= m1.group(3);
        } else{
            System.out.println("No match en "+imagenFile);
            return;
        }

        System.out.println(tick+":"+number+":"+name);
        
        Ticker ticker= TickerForm.loadTicker(tick);

        BufferedOutputStream out= null;
        BufferedInputStream in= null;

        try{
            num= Integer.parseInt(number);
            int img = Integer.parseInt(name);
            System.out.println(num);
            Base64 encoder= new Base64();
            String image= ticker.getHeadline(num).getImage(img);
            byte[] ba= encoder.decode(image);

            out= new BufferedOutputStream(res.getOutputStream());
            for (int i= 0, n= ba.length; i < n; i++){
                out.write(ba[i]);
            }

            res.setContentType("image/jpeg");

            /*
             * convertDate.setTime(f.lastModified());
             * convertBuffer.setLength(0); res.setHeader("Last-Modified",
             * formatter.format(convertDate, convertBuffer,
             * convertField).toString());
             * 
             * convertDate.setTime(System.currentTimeMillis() + distance / 10L);
             * convertBuffer.setLength(0); res.setHeader("Expires",
             * formatter.format(convertDate, convertBuffer,
             * convertField).toString());
             */
            //            res.setContentLength(n);
            //            fis.close();
            out.flush();
            out.close();
        } catch (Exception e){
            System.err.println("Exception showing image: " + imagenFile + e);
        } finally{
            if (in != null){
                in.close();
            } // Always Closed
            if (out != null){
                out.close();
            } // Always Closed
        }
    }

}