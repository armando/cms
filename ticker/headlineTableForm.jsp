<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page session="false"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.sourceforge.net/tld/myfaces_ext_0_9.tld" prefix="x"%>
<html>

<%@include file="inc/head.inc" %>

<body>

<f:view>

    <f:loadBundle basename="com.hola.ed.resources.ed_messages" var="ed_messages"/>

    <x:panelLayout id="page" layout="#{globalOptions.pageLayout}"
            styleClass="pageLayout"
            headerClass="pageHeader"
            navigationClass="pageNavigation"
            bodyClass="pageBody"
            footerClass="pageFooter" >

        <%@include file="inc/page_header.jsp" %>
        <f:facet name="navigation">
            <f:subview id="menu" >
                <jsp:include page="inc/navegacion.jsp" />
            </f:subview>
        </f:facet>

        <f:facet name="body">
            <h:panelGroup id="body">

               <h:messages errorClass="error" showSummary="true" showDetail="true" />

                <h:form id="form" style="display:inline" >
                <h:dataTable id="data"
                        styleClass="standardTable"
                        headerClass="standardTable_Header"
                        rowClasses="standardTable_Row1,standardTable_Row2"
                        columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                        var="headline"
                        value="#{tickerForm.headlines}"
                   >
                   
                   <h:column>
                        <f:facet name="header">
                            <h:outputText value="#{ed_messages['label_order']}"/>
                       </f:facet>
                        <h:selectOneListbox id="order" size="1" value="#{headline.order}">
                            <f:selectItems id="orders" value="#{tickerForm.orders}" />
                        </h:selectOneListbox>
                   </h:column>

                   <h:column>
                       <f:facet name="header">
                          <h:outputText value="#{ed_messages['label_text']}" />
                       </f:facet>
                       <h:inputText id="htext" value="#{headline.text}" required="true" />
                   </h:column>

                   <h:column>
                       <f:facet name="header">
                          <h:outputText value="#{ed_messages['label_URL']}" />
                       </f:facet>
                       <h:inputText id="hurl" value="#{headline.URL}"/>

                   </h:column>

                   <h:column>
                       <f:facet name="header">
                          <h:outputText value="#{ed_messages['label_type']}" />
                       </f:facet>
                        <h:selectOneListbox id="selone" size="1" value="#{headline.type}">
                            <f:selectItems id="selone_types" value="#{headlineForm.types}" />
                        </h:selectOneListbox>
                   </h:column>


                   <f:facet name="footer">
                        <h:panelGroup>
                            <h:commandButton action="#{tickerForm.save}" value="#{ed_messages['button_save']}" />
                            <f:verbatim>&nbsp;</f:verbatim>
                            <h:commandButton action="go_back" immediate="true" value="#{ed_messages['button_cancel']}" />
                        </h:panelGroup>
                   </f:facet>

                </h:dataTable>
                </h:form>

            </h:panelGroup>
        </f:facet>

        <%@include file="inc/page_footer.jsp" %>

    </x:panelLayout>

</f:view>

</body>
</html>
