
<%@ page session="false"
%><%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"
%><%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"
%>
<html>

<f:view>

<f:verbatim>
<head>
  <meta HTTP-EQUIV="Content-Type" CONTENT="text/html;CHARSET=UTF-8">
  <title>
</f:verbatim>
  <h:outputText value="#{tickerForm.workName} - editor"/>
<f:verbatim>
</title>
  <Link rel="stylesheet" type="text/css" href="css/basic.css">
</head>
<body>
</f:verbatim>

    <f:loadBundle basename="com.hola.ed.resources.ed_messages" var="ed_messages"/>

    <h:panelGrid>

        <%@include file="inc/page_header.jsp" %>
        <f:facet name="navigation">
            <f:subview id="menu" >
                <jsp:include page="inc/navegacion.jsp" />
            </f:subview>
        </f:facet>

        <f:facet name="body">
            <h:panelGroup id="body">

               <h:panelGrid columns="2">
				<h:form id="lists">
				 <h:selectOneListbox id="list" value="#{tickerForm.workName}" onchange="submit()" size="1">
                            <f:selectItem id="l0" itemValue="headlines" itemLabel="Headlines"/>                            
                            <f:selectItem id="l1" itemValue="hb" itemLabel="Health & Beauty"/>
                            <f:selectItem id="l2" itemValue="lg" itemLabel="Looking Good"/>
                            <f:selectItem id="l3" itemValue="sd" itemLabel="Skin Deep"/>
                            <f:selectItem id="l4" itemValue="fg" itemLabel="Feeling Great"/>
                            <f:selectItem id="l5" itemValue="ht" itemLabel="Head Turner"/>
                 </h:selectOneListbox>
				</h:form>
			   <h:form id="actionstop">
                   <h:commandButton action="#{headlineForm.add}" value="#{ed_messages['new_headline']}" styleClass="standard" rendered="#{tickerForm.isContext}"/>
			   </h:form>
               </h:panelGrid>
               <f:verbatim><br/><br/></f:verbatim>
                <h:dataTable id="data"
                        styleClass="standardTable"
                        headerClass="standardTable_Header"
                        footerClass="standardTable_Header"
                        rowClasses="standardTable_Row1,standardTable_Row2"
                        columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                        var="headline"
                        value="#{tickerForm.headlines}"
                   >
                   
                   <h:column>
                       <f:facet name="header">
                          <h:outputText value="#{ed_messages['label_order']}" />
                       </f:facet>
                       <h:outputText value="#{tickerForm.headlines.rowIndex + 1}" />
                   </h:column>
                                                         
                   <h:column>
                       <f:facet name="header">
                          <h:outputText value="#{ed_messages['label_headline']}" />
                       </f:facet>
                   <x:commandLink action="go_headline">
                            <h:outputText value="#{headline.text}" />
                            <x:updateActionListener property="#{headlineForm.id}" value="#{headline.id}" />
                       </x:commandLink>
                   </h:column>

                   <h:column rendered="#{tickerForm.hasTypes}">
                       <f:facet name="header">
                          <h:outputText value="#{ed_messages['label_type']}" />
                       </f:facet>
                        <h:selectOneListbox id="selone" size="1" value="#{headline.type}" disabled="true">
                            <f:selectItems id="selone_types" value="#{headlineForm.types}" />
                        </h:selectOneListbox>
                   </h:column>

                  <h:column>
                       <f:facet name="header">
                          <h:outputText value="" />
                       </f:facet>
                   <x:commandLink action="#{headlineForm.up}" >
                            <h:graphicImage id="up" url="images/up.gif" style="border-width:0"/>
                            <x:updateActionListener property="#{headlineForm.id}" value="#{headline.id}" />
                       </x:commandLink>
                   </h:column>
                    <h:column>
                       <f:facet name="header">
                          <h:outputText value="" />
                       </f:facet>
                   <x:commandLink action="#{headlineForm.down}" >
							<h:graphicImage id="down" url="images/down.gif" style="border-width:0"/>
                            <x:updateActionListener property="#{headlineForm.id}" value="#{headline.id}" />
                       </x:commandLink>
                   </h:column>

                   <h:column>
                       <f:facet name="header">
                          <h:outputText value="#{ed_messages['label_id']}" />
                       </f:facet>
                       <h:outputText value="#{headline.id}" />
                   </h:column>

                   <h:column>
                       <f:facet name="header">
                          <h:outputText value="#{ed_messages['label_date']}" />
                       </f:facet>
                       <h:outputText value="#{headline.live}" />
                   </h:column>

                    <h:column>
                       <f:facet name="header">
                          <h:outputText value="" />
                       </f:facet>
                   <x:commandLink action="#{headlineForm.delete}" >
							<h:graphicImage id="trash" url="images/trash.gif" style="border-width:0"/>
                            <x:updateActionListener property="#{headlineForm.id}" value="#{headline.id}" />
                       </x:commandLink>
                   </h:column>

                </h:dataTable>
               <f:verbatim><br/><br/></f:verbatim>
				<h:outputText value="Repository" rendered="#{!tickerForm.isContext}"/>
			   <h:form id="actions">
               <h:panelGrid columns="1">
				 <h:selectOneListbox id="headlines" value="#{headlineForm.id}" rendered="#{!tickerForm.isContext}" >
                            <f:selectItems id="heads" value="#{tickerForm.headlinesOptions}"/>
                 </h:selectOneListbox>

                   <h:commandButton action="#{headlineForm.add}" 
                   value="#{ed_messages['existing_headline']}" styleClass="standard" rendered="#{!tickerForm.isContext && !tickerForm.emptyRepository}"/>

               </h:panelGrid>
               <f:verbatim><br/></f:verbatim>		
                </h:form>
                <h:form id="sending">
             <h:outputText value="SMS message:" rendered="#{tickerForm.isSms}"/>
             <f:verbatim><br/></f:verbatim>
             <h:outputText value="#{tickerForm.smsText}" rendered="#{tickerForm.isSms}"/>
             <f:verbatim><br/></f:verbatim>
             <h:commandButton id= "smssend" action="#{tickerForm.sendSms}" 
             	 value="Send SMS" rendered="#{tickerForm.isSms}"/> 
             
             <h:commandButton id="jbagsend" action="#{tickerForm.sendJBag}"
             value="Send to JBag" rendered="#{tickerForm.isJBag}"/> 
             
             <h:commandButton id="ntlsend" action="#{tickerForm.sendNTL}"
             value="Send to NTL" rendered="#{tickerForm.isNTL}"/> 
             
             <h:commandButton id="tescosend" action="#{tickerForm.sendTesco}"
             value="Send to Tesco" rendered="#{tickerForm.isTesco}"/> 
             
             <h:commandButton id="wapsend" action="#{tickerForm.sendWap}"
             value="Send to WAP" rendered="#{tickerForm.isWap}"/> 
             
             <h:commandButton id="tickersend" action="#{tickerForm.sendTicker}"
             value="Send to Ticker" rendered="#{tickerForm.isTicker}"/> 
             
             <h:commandButton id="blueyondersend" action="#{tickerForm.sendBlueyonder}"
             value="Send to Blueyonder" rendered="#{tickerForm.isBlueyonder}"/>
             
             <h:commandButton id="ukonlinesend" action="#{tickerForm.sendUKOnline}"
             value="Send to UK Online" rendered="#{tickerForm.isUKOnline}"/> 

             <h:commandButton id="biographysend" action="#{tickerForm.sendBiography}"
             value="Send to Biographychannel" rendered="#{tickerForm.isBiography}"/> 
             
             <h:commandButton id="yahoosend" action="#{tickerForm.sendYahoo}"
             value="Send to Yahoo" rendered="#{tickerForm.isYahoo}"/> 

             	   </h:form>
                <f:verbatim><br></f:verbatim>

            </h:panelGroup>
        </f:facet>

        <%@include file="inc/page_footer.jsp" %>

    </h:panelGrid>

</f:view>

</body>

</html>