<%@ page session="false"
%><%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"
%><%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"
%><%@ taglib uri="http://myfaces.sourceforge.net/tld/myfaces_ext_0_9.tld" prefix="x"
%><html>

<%@include file="inc/head.inc" %>

<body>

<f:view>

    <f:loadBundle basename="com.hola.ed.resources.ed_messages" var="ed_messages"/>

    <x:panelLayout id="page" layout="#{globalOptions.pageLayout}"
            styleClass="pageLayout"
            headerClass="pageHeader"
            navigationClass="pageNavigation"
            bodyClass="pageBody"
            footerClass="pageFooter" >

        <%@include file="inc/page_header.jsp" %>
        <f:facet name="navigation">
            <f:subview id="menu" >
               <jsp:include page="inc/navegacion.jsp" />
            </f:subview>
        </f:facet>

        <f:facet name="body">
            <h:panelGroup>
                <h:form id="sms" name="sms" enctype="multipart/form-data">
                    <x:saveState value="#{sms.id}" />
                    <h:panelGrid columns="2" styleClass="countryFormTable"
                                 headerClass="countryFormHeader"
                                 footerClass="countryFormFooter"
                                 columnClasses="countryFormLabels, countryFormInputs" >


                        <h:outputLabel for="message" value="#{ed_messages['label_text']}"/>
                        <h:panelGroup>
                            <h:inputTextarea id="message" rows="2" cols="33" value="#{sms.message}" required="true" />
                            <h:message for="message" styleClass="error" showDetail="true" showSummary="false" />
                        </h:panelGroup>

      					<h:outputLabel for="day" value="#{ed_messages['label_day']}" />
      					<h:panelGroup>
                        <h:selectOneListbox id="day" size="1" value="#{sms.day}">
                            <f:selectItems id="day_select" value="#{sms.days}" />
                        </h:selectOneListbox>
						</h:panelGroup>

      					<h:outputLabel for="month" value="#{ed_messages['label_month']}" />
      					<h:panelGroup>
                        <h:selectOneListbox id="month" size="1" value="#{sms.month}">
                            <f:selectItems id="month_select" value="#{sms.months}" />
                        </h:selectOneListbox>
						</h:panelGroup>

      					<h:outputLabel for="year" value="#{ed_messages['label_year']}" />
      					<h:panelGroup>
                        <h:selectOneListbox id="year" size="1" value="#{sms.year}">
                            <f:selectItems id="year_select" value="#{sms.years}" />
                        </h:selectOneListbox>
						</h:panelGroup>

      					<h:outputLabel for="hour" value="#{ed_messages['label_hour']}" />
      					<h:panelGroup>
                        <h:selectOneListbox id="hour" size="1" value="#{sms.hour}">
                            <f:selectItems id="hour_select" value="#{sms.hours}" />
                        </h:selectOneListbox>
						</h:panelGroup>

      					<h:outputLabel for="minute" value="#{ed_messages['label_minute']}" />
      					<h:panelGroup>
                        <h:selectOneListbox id="minute" size="1" value="#{sms.minute}">
                            <f:selectItems id="minute_select" value="#{sms.minutes}" />
                        </h:selectOneListbox>
						</h:panelGroup>
																		
                        <h:panelGroup>
                            <h:commandButton action="#{sms.send}"  value="#{ed_messages['button_send']}" />
                        </h:panelGroup>

                    </h:panelGrid>
                </h:form>
            </h:panelGroup>
        </f:facet>

        <%@include file="inc/page_footer.jsp" %>

    </x:panelLayout>

</f:view>

</body>

</html>
