<%@ page session="false"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://www.hola.com/jsf" prefix="d"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<html>
<f:view>

<f:verbatim>
<head>
  <meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8">
  <title>
</f:verbatim>
  <h:outputText value="#{tickerForm.workName} - editor"/>
<f:verbatim>
</title>
  <Link rel="stylesheet" type="text/css" href="css/basic.css">
</head>
<body>
</f:verbatim>

<f:loadBundle basename="com.hola.ed.resources.ed_messages" var="ed_messages"/>

    <h:panelGroup>

        <%@include file="inc/page_header.jsp" %>
        <f:facet name="navigation">
            <f:subview id="menu" >
               <jsp:include page="inc/navegacion.jsp" />
            </f:subview>
        </f:facet>

        <f:facet name="body">
            <h:panelGroup>
                <h:form id="headlineForm" name="headlineForm" enctype="multipart/form-data">
					<x:saveState value="#{headlineForm.id}" />
                    <h:panelGrid columns="2" styleClass="countryFormTable"
                                 headerClass="countryFormHeader"
                                 footerClass="countryFormFooter"
                                 columnClasses="countryFormLabels, countryFormInputs"
                                 style="align: left;" >
                        <f:facet name="header">
                            <h:outputText value="#{tickerForm.workName}"/>
                        </f:facet>
                        <f:facet name="footer">
                            <h:outputText value="Main repository" rendered="#{tickerForm.isContext}"/>
                        </f:facet>
                        
                        <h:outputLabel for="order" value="#{ed_messages['label_order']}"/>
                        <h:panelGroup>
                         <h:selectOneListbox id="order" size="1" value="#{headlineForm.order}">
                            <f:selectItems id="orders" value="#{tickerForm.orders}" />
                        </h:selectOneListbox>
                        </h:panelGroup>

                        <h:outputLabel for="text" value="#{ed_messages['label_headline']}"/>
                        <h:panelGroup>
                            <h:inputText id="text" size="70" value="#{headlineForm.text}" required="true" />
                            <h:message for="text" styleClass="error" showDetail="true" showSummary="false" />
                        </h:panelGroup>

                       <h:outputLabel for="deck" value="#{ed_messages['label_deck']}"/>
                        <h:panelGroup>
                            <h:inputText id="deck" size="70" value="#{headlineForm.deck}">
                            </h:inputText>
                            <h:message for="deck" styleClass="error" showDetail="true" showSummary="false" />
                        </h:panelGroup>	

                        <h:outputLabel for="URL" value="#{ed_messages['label_URL']}"/>
                        <h:panelGroup>
                            <h:inputText id="URL" size="70" value="#{headlineForm.URL}">
                            </h:inputText>
                            <h:message for="URL" styleClass="error" showDetail="true" showSummary="false" />
                        </h:panelGroup>	

						<h:panelGroup rendered="#{tickerForm.hasTypes}">
							<h:outputLabel for="selone" value="#{ed_messages['label_type']}" />
						</h:panelGroup>

						<h:panelGroup rendered="#{tickerForm.hasTypes}">
							<h:selectOneListbox id="selone" size="1" value="#{headlineForm.type}">
    	                       	<f:selectItems id="selone_types" value="#{headlineForm.types}" />
        	               	</h:selectOneListbox>
						</h:panelGroup>
						
	
						<h:outputLabel for="summary" value="#{ed_messages['label_summary']}" />
      					<h:panelGroup>
      					    <h:inputTextarea id="summary" rows="6" cols="55" value="#{headlineForm.summary}">
      					    </h:inputTextarea>
							<h:message for="summary" styleClass="error" showDetail="true" showSummary="false" />
						</h:panelGroup>
						
						<h:outputLabel for="fulltext" value="#{ed_messages['label_fulltext']}" />
      					<h:panelGroup>
      					    <h:inputTextarea id="fulltext" rows="12" cols="55" value="#{headlineForm.fulltext}">
      					    </h:inputTextarea>
							<h:message for="fulltext" styleClass="error" showDetail="true" showSummary="false" />
						</h:panelGroup>
						
						<h:outputLabel for="datepicker" value="#{ed_messages['label_live_date']}" />	
						<h:panelGroup>
							<d:datePicker id="datepicker" value="#{headlineForm.liveDate}" startYear="2004" totalYears="3" />
						</h:panelGroup>

						<%-- first image related cells --%>
						<h:outputLabel for="fileupload" value="#{ed_messages['label_image']}" />
      					<h:panelGroup>
                        	<t:inputFileUpload id="fileupload"
                            	storage="file" accept="image/*"
                                value="#{headlineForm.image1}"
                                styleClass="fileUploadInput" />
						</h:panelGroup>
						
						<h:panelGroup rendered="#{headlineForm.image1Set}">
							<h:outputLabel for="current_image" value="#{ed_messages['label_current_image']}" />
						</h:panelGroup>
						
						<h:panelGroup rendered="#{headlineForm.image1Set}">
							<h:graphicImage id="current_image" value="/imgticker/#{headlineForm.image1Name}" />
						</h:panelGroup>

                        <h:outputLabel for="caption1" value="#{ed_messages['label_caption']}"/>
                        <h:panelGroup>
                            <h:inputText id="caption1" size="70" value="#{headlineForm.caption1}">
                            </h:inputText>
                            <h:message for="caption1" styleClass="error" showDetail="true" showSummary="false" />
                        </h:panelGroup>	

						
						<h:panelGroup rendered="#{headlineForm.image1Set}" />
						<h:panelGroup rendered="#{headlineForm.image1Set}">
							<h:commandLink action="#{headlineForm.deleteImage1}">
								<h:outputText value="#{ed_messages['label_delete_image']}" />
							</h:commandLink>
						</h:panelGroup>
						
						<%-- second image related cells --%>
						<h:outputLabel for="fileupload" value="#{ed_messages['label_image']}" rendered="#{headlineForm.image1Set}" />
      					<h:panelGroup rendered="#{headlineForm.image1Set}">
                        	<x:inputFileUpload id="fileupload2"
                            	accept="image/*"
                                value="#{headlineForm.image2}"
                                styleClass="fileUploadInput" />
						</h:panelGroup>

						<h:panelGroup rendered="#{headlineForm.image2Set}">
							<h:outputLabel for="current_image2" value="#{ed_messages['label_current_image']}" />
						</h:panelGroup>
						
						<h:panelGroup rendered="#{headlineForm.image2Set}">
							<h:graphicImage id="current_image2" value="/imgticker/#{headlineForm.image2Name}" />
						</h:panelGroup>

                        <h:outputLabel for="caption2" value="#{ed_messages['label_caption']}"/>
                        <h:panelGroup>
                            <h:inputText id="caption2" size="70" value="#{headlineForm.caption2}">
                            </h:inputText>
                            <h:message for="caption2" styleClass="error" showDetail="true" showSummary="false" />
                        </h:panelGroup>	


						<h:panelGroup rendered="#{headlineForm.image2Set}"/>
						<h:panelGroup rendered="#{headlineForm.image2Set}">
							<h:commandLink action="#{headlineForm.deleteImage2}">
								<h:outputText value="#{ed_messages['label_delete_image']}" />
							</h:commandLink>
						</h:panelGroup>
						
						<h:outputLabel for="related1" value="#{ed_messages['label_related']}"/>
                        <h:panelGroup>
                            <h:inputText id="related1" size="70" value="#{headlineForm.related1}">
                            </h:inputText>
                            <h:message for="related1" styleClass="error" showDetail="true" showSummary="false" />
                        </h:panelGroup>	
						
						<h:outputLabel for="related2" value="#{ed_messages['label_related']}"/>
                        <h:panelGroup>
                            <h:inputText id="related2" size="70" value="#{headlineForm.related2}">
                            </h:inputText>
                            <h:message for="related2" styleClass="error" showDetail="true" showSummary="false" />
                        </h:panelGroup>	
                        
						<h:outputLabel for="related3" value="#{ed_messages['label_related']}"/>
                        <h:panelGroup>
                            <h:inputText id="related3" size="70" value="#{headlineForm.related3}">
                            </h:inputText>
                            <h:message for="related3" styleClass="error" showDetail="true" showSummary="false" />
                        </h:panelGroup>	
						
						
						<h:panelGroup/>
						<h:panelGroup>
							<h:commandButton action="#{headlineForm.save}"  value="#{ed_messages['button_save']}" />
                            <f:verbatim>&nbsp;</f:verbatim>
                            <h:commandButton action="cancel" immediate="true" value="#{ed_messages['button_cancel']}" />
                            <f:verbatim>&nbsp;</f:verbatim>
                            <h:commandButton action="#{headlineForm.delete}" immediate="true" value="#{ed_messages['button_delete']}" />
                        </h:panelGroup>

                    </h:panelGrid>
				</h:form>

            </h:panelGroup>
			

			

			
		</f:facet>
		
		
        <%@include file="inc/page_footer.jsp" %>

    </h:panelGroup>

</f:view>

</body>

</html>
