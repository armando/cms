<%@ page session="false"
%><%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"
%><%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"
%><%@ taglib uri="http://myfaces.sourceforge.net/tld/myfaces_ext_0_9.tld" prefix="x"
%><html>


<%@include file="inc/head.inc" %>

<body>

<f:view>

    <f:loadBundle basename="com.hola.ed.resources.ed_messages" var="ed_messages"/>

    <x:panelLayout id="page" layout="#{globalOptions.pageLayout}"
            styleClass="pageLayout"
            headerClass="pageHeader"
            navigationClass="pageNavigation"
            bodyClass="pageBody"
            footerClass="pageFooter" >

        <%@include file="inc/page_header.jsp" %>
        <f:facet name="navigation">
            <f:subview id="menu" >
				<%@include file="tree.jsp" %>
            </f:subview>
        </f:facet>

        <f:facet name="body">
            <h:panelGroup id="body">
                <h:dataTable id="data"
                        styleClass="standardTable"
                        headerClass="standardTable_Header"
                        footerClass="standardTable_Header"
                        rowClasses="standardTable_Row1,standardTable_Row2"
                        columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                        var="image"
                        value="#{resources.list}"
                   >
                   
                    <h:column>

                          <h:outputText value="#{image.mime}" />

                   </h:column>

                    <h:column>

                          <h:outputText value="#{image.width}" />
                   </h:column>

                    <h:column>

                          <h:outputText value="#{image.height}" />


                   </h:column>

                    <h:column>
                   <x:commandLink action="go_fileupload">
						 <h:outputText value=""/>
							<f:verbatim><img src="</f:verbatim>

                          <h:outputText value="/ed/img/#{image.data}" />
							<f:verbatim>" width="200" height="</f:verbatim>
                          <h:outputText value="#{((image.height * 200) / image.width)}" />
							<f:verbatim>"></f:verbatim>
                            <!-- for convenience: MyFaces extension. sets id of current row in countryForm -->
                            <!-- you don't have to implement a custom action! -->
                            <x:updateActionListener property="#{fileUploadForm.versionNumber}" value="#{resources.list.rowIndex}" />
                       </x:commandLink>

                   </h:column>

                </h:dataTable>
                
                <f:verbatim><br></f:verbatim>

            </h:panelGroup>
        </f:facet>

        <%@include file="inc/page_footer.jsp" %>

    </x:panelLayout>

</f:view>

</body>

</html>